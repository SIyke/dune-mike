// This struct conatains the number of available threads
// and a vector for storing all executing threads


#ifndef DUNE_THREAD_CONSTRUCT_HH
#define DUNE_THREAD_CONSTRUCT_HH


#include <vector>
#include <thread>
#include <memory>
#include "threadpool_old.hh"


namespace Dune {
    
    // struct for sharing multithreading variables
    class ThreadConstruction
    {
    public:
        size_t thread_number;

        ThreadConstruction(size_t n)
            : thread_number(n)
        {
            threads.reserve(n);
        }

        template<typename T, typename... Ts>
        void addtask(T& lambda, Ts&... args)
        {
            threads.emplace_back(std::thread{lambda, args...});
        }

        template<typename T, typename... Ts>
        void addtaskid(T& lambda, size_t id, Ts&... args)
        {
            threads.emplace_back(std::thread{lambda, args...});
        }


        void waittasks()
        {
            for(auto& t: threads)
                t.join();

            threads.clear();
        }

    private:
        std::vector<std::thread> threads;
    };

    typedef std::shared_ptr<ThreadConstruction> ThreadConstructContext;
}


#endif