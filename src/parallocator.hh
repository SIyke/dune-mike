#ifndef DUNE_PAR_ALLOCATOR_HH
#define DUNE_PAR_ALLOCATOR_HH


#include <iostream>
#include <cstring>
#include <string.h>
#include <hwloc.h>
#include "parallelton.hh"

namespace Dune{

    template<class T, class ThreadBackend = ThreadPool>
    class ParAllocator
    {
    public:
        typedef T value_type;
        typedef std::size_t size_type;
        typedef ptrdiff_t difference_type;


        ParAllocator() = default;

        template<class U>
        ParAllocator (const ParAllocator<U>& other) noexcept {}

        ParAllocator& operator=(const ParAllocator&) = default;

        template<class U>
        bool operator==(const ParAllocator<U>&) const noexcept {return true;}

        template<class U>
        bool operator!=(const ParAllocator<U>&) const noexcept {return false;}


        T* allocate(size_type n) const
        {
            // T* t = new T[n];
            T* t = (T*) hwloc_alloc(Parallelton<ThreadPool>::topology, sizeof(T) * n);
            
            // std::cout << "Allocating " << (n * sizeof(T)) / (1024.0 * 1024.0) << " MB\n"; 

            typedef Parallelton<ThreadBackend> ParalleltonTB;
            auto context = ParalleltonTB::context;
            int rank, size;
            MPI_Comm_rank(MPI_COMM_WORLD, &rank);
            MPI_Comm_size(MPI_COMM_WORLD, &size);
            auto N = ParalleltonTB::N;
            // preparing of variables to determine correct memory binding
            constexpr int Dim = 3;

            std::array<int, Dim> dims;
            dims.fill(0);
            MPI_Dims_create(size, Dim, &dims[0]);

            size_t blockx = dims[0];
            size_t blocky = dims[1];
            size_t blockz = 0ul;
            
            if constexpr (Dim == 3)
                blockz = dims[2];

            size_t pbx = N / blockx;
            size_t pby = N / blocky;
            size_t pbz = 0ul;

            if constexpr (Dim == 3)
                pbz = N / blockz;
            // // 2D
            //             // between corners + 4 corners
            // auto lostelements = (N-2) * 4 + 4 * 2; 
            // 3D
                        // 6 areas + 12 edges + 8 corners
            auto lostelements = 2 * (pbx-2) * (pby-2) + 2 * (pbx-2) * (pbz-2) + 2 * (pby-2) * (pbz-2)          // 6 areas
                                + 4 * (pbx-2) * 2 + 4 * (pby-2) * 2 + 4 * (pbz-2) * 2                           // 12 edges 
                                + 8 * 3;                                                                        // 8 corners

            // elements per thread = how many elements will get allocated / over how many threads are they distributed
            auto ept = n / context->getnbthreads();

            hwloc_bitmap_t set;

            char* s;
            int err;
            set = hwloc_bitmap_alloc();
            if (!set) {
                fprintf(stderr, "failed to allocate a bitmap\n");
                hwloc_topology_destroy(ParalleltonTB::topology);
            }

            // allocating r or vectors
            // if (n == pbx*pby*pbz || n == (pbx+2) * (pby+2) * (pbz+2))
            {
                // std::cout << "Allocating " << n << " elements; ept=" << ept << "\n";
                for(size_t i = 0; i < context->getnbthreads(); i++)
                {
                    auto logid = ParalleltonTB::lambda(i, rank, context->getnbthreads());
                    auto logobj = hwloc_get_obj_by_type(ParalleltonTB::topology, HWLOC_OBJ_PU, logid);
                    hwloc_bitmap_only(set, logobj->os_index);
                    
                    err = hwloc_set_area_membind(ParalleltonTB::topology, t + i*ept, sizeof(T) * ept, set, HWLOC_MEMBIND_BIND, 
                        HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
                    if (err < 0)
                        std::cout << "Error: memory binding failed" << std::endl;
                    
                    // hwloc_bitmap_asprintf(&s, set);

                    // std::cout << "Rank=" << ParalleltonTB::rank << " Tid=" << i
                    //     << " on PU logical index=" << s << std::endl;
                }   
            }
            // // allocating a and j
            // else
            // {
            //     ept = (n - lostelements) / context->getnbthreads();
            //     // std::cout << "Allocating " << n << " elements; ept=" << ept << "\n";

            //     for(size_t i = 0; i < context->getnbthreads(); i++)
            //     {
            //         auto logid = ParalleltonTB::lambda(i, rank, context->getnbthreads());
            //         auto logobj = hwloc_get_obj_by_type(ParalleltonTB::topology, HWLOC_OBJ_PU, logid);
            //         hwloc_bitmap_only(set, logobj->os_index);
                    
            //         err = hwloc_set_area_membind(ParalleltonTB::topology, t + i*ept, sizeof(T) * ept, set, HWLOC_MEMBIND_BIND, 
            //             HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
            //         if (err < 0)
            //             std::cout << "Error: memory binding failed" << std::endl;
                    
            //         // hwloc_bitmap_asprintf(&s, set);

            //         // std::cout << "Rank=" << ParalleltonTB::rank << " Tid=" << i
            //         //     << " on PU logical index=" << s << std::endl;
            //     }
            // }



            // // first touching
            // for(size_type i = 0; i < n; i++)
            // {
            //     T tmp;
            //     t[i] = tmp;
            // }

            // // checking where memory is allocated to
            // hwloc_membind_policy_t policy;
            // hwloc_bitmap_zero(set);
            // err = hwloc_get_area_membind(ParalleltonTB::topology, t, sizeof(T)*n, set, &policy, HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
            // if(err < 0)
            //     std::cout << "Error: getting memory binding failed" << std::endl;

            // // print out data
            // hwloc_bitmap_asprintf(&s, set);
            // std::cout << "R" << rank << " actually on " << s << std::endl;

            // auto touch = [t](size_t start, size_t stride, size_t id)
            // {
            //     for(auto i = start; i < start+stride; i++)
            //     {
            //         T tmp;
            //         t[i] = tmp;
            //     }
            // };

            // for(size_t i = 0; i < context->getnbthreads(); i++)
            // {
            //     auto current = i * ept;
            //     context->addtask(touch, i, current, ept, i);
            // }
            // context->waittasks();


            // // checking where memory is allocated to
            // for(size_t i = 0; i < context->getnbthreads(); i++)
            // {
            //     hwloc_membind_policy_t policy;
            //     hwloc_bitmap_zero(set);
            //     err = hwloc_get_area_membind(ParalleltonTB::topology, t+i*ept, sizeof(T)*ept, set, &policy, HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
            //     if(err < 0)
            //         std::cout << "Error: getting memory binding failed" << std::endl;

            //     // print out data
            //     hwloc_bitmap_asprintf(&s, set);
            //     std::cout << "T" << i << " actually on " << s << std::endl; 
            // }

            hwloc_bitmap_free(set);
            return t;      
        }

        void deallocate(T* p, size_type n) const
        {
            if(p)
            {
                // delete[] p;
                hwloc_free(Parallelton<ThreadPool>::topology, p, sizeof(T) * n);
            }
        }
    };
}



#endif