#ifndef BLOCKING_PATTERN_3D_HH
#define BLOCKING_PATTERN_3D_HH


#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/scalarmatrixview.hh>
#include <algorithm>
#include "parcommunicator.hh"



std::vector<size_t> setupoverlapindices3D(size_t N, Dune::ParData pardata)
{
    std::vector<size_t> overlaps;
    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    auto blockx = pardata.getblockx();
    auto blocky = pardata.getblocky();
    auto blockz = pardata.getblockz();
    auto size = pardata.getsize();
    auto rank = pardata.getrank();
    auto start = pardata.getstart();
    // coordinates of the current rank block
    auto rx = pardata.getxcoord();
    auto ry = pardata.getycoord();
    auto rz = pardata.getzcoord();

    auto nbthreads = Parallelton<Dune::ThreadPool>::context->getnbthreads();
    std::array<int, 3> dims;
    dims.fill(0);
    MPI_Dims_create(nbthreads, 3, &dims[0]);

    auto threadblockx = dims[0];
    auto threadblocky = dims[1];
    auto threadblockz = dims[2];

    auto ptx = pbx / threadblockx;
    auto pty = pby / threadblocky;
    auto ptz = pbz / threadblockz;


    auto overlap = 1ul;
    auto index = 0ul;

    for (size_t j = 0; j < nbthreads; j++)
        for (size_t k = 0; k < ptx*pty*ptz; k++, ++index)
        {
            // offset so each block is mapped correctly
            auto block_index = index - j * ptx*pty*ptz;

            // thread local x and y coords
            size_t x = block_index%ptx;
            size_t y = (block_index/ptx) % pty;
            size_t z = (block_index/(ptx*pty)) % ptz;
            // coordinates of the current thread block
            auto tx = j%dims[0];
            auto ty = (j/dims[0]) % threadblocky;
            auto tz = (j/(dims[0] * dims[1])) % threadblockz;
            
            // overlap on the front
            if (tx == 0 && ty == 0 && tz == 0 && x == 0 && y == 0 && z == 0)
                overlap += (pbx+2) * (pby+2);
            // overlap on the left
            if (tx == 0 && x == 0)
                overlap += 1;
            // overlap on the bottom
            if (ty == 0 && index % (ptx*pty) == 0)
                overlap += ptx;
            // overlap on the top
            if (ty == threadblocky-1 && (tx > 0 || z > 0) && x == 0 && y == 0)
                overlap += ptx;
            // overlap on the right
            if (tx == threadblockx-1 && x == 0 && y > 0)
                overlap += 1;
            // overlap for completing one row of thread blocks (last overlap on the right side for the last block)
            //                         completing any row and moving up || completing a row and moving one layer behind             
            if (x == 0 && y == 0 && ((ty > 0 && tx == 0 && z == 0) || (z > 0 && tx == threadblockx-1)))
                overlap += 1;
            // overlap for completing a layer and moving one to the back (top layer + top right corner + last overlap on right side + bottom left corner)
            if (x == 0 && y == 0 && z == 0 && tx == 0 && ty == 0 && tz > 0)
                overlap += ptx + 1 + 1 + 1;
            // overlap for top right corner
            if (x == 0 && y == 0 && z > 0 && tx == threadblockx-1 && ty == threadblocky-1)
                overlap += 1;
            // overlap for bottom right corner
            if (ty == 0 && tx == threadblockx-1 && x == 0 && y == 0)
                overlap += 1;
            // overlap for top left corner (happens in the next thread block and all layers in z idrections)
            if (ty == threadblocky-1 && ((tx == 1 && z == 0) || (tx == 0 && z > 0)) && x == 0 && y == 0)
                overlap += 1;
            // overlap for the bottom left corner
            if (z > 0 && x == 0 && y == 0 && tx == 0 && ty == 0)
                overlap += 1;

            overlaps.emplace_back(index + overlap);
            // if (index + overlap >= 1624)
            // {
            //     char c;
            //     std::cin >> c;
            //     std::cout << "x:" << x << " y:" << y << " z:" << z << " tx:" << tx << " ty:" << ty << " tz: " << tz << 
            //                 /*" rx:" << rx << " ry:" << ry << " rz:" << rz <<*/ " ID:" << index << " OID: " << overlap + index << std::endl;
            // }
        }
    // exit(1);
    return overlaps;
}


template<class B, class Alloc>
void setupBlockedSparsity3D(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
    typedef typename Dune::BCRSMatrix<B,Alloc> Matrix;

    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    auto blockx = pardata.getblockx();
    auto blocky = pardata.getblocky();
    auto blockz = pardata.getblockz();
    auto size = pardata.getsize();
    auto rank = pardata.getrank();
    auto start = pardata.getstart();
    // coordinates of the current rank block
    auto rx = pardata.getxcoord();
    auto ry = pardata.getycoord();
    auto rz = pardata.getzcoord();

    
    auto nbrows = N*N*N / size;
    auto nbcols = (pbx+2) * (pby+2) * (pbz+2);

    A.setSize(nbrows, nbcols, nbrows*7ul);
    A.setBuildMode(Matrix::row_wise);

    auto nbthreads = Parallelton<Dune::ThreadPool>::context->getnbthreads();
    std::array<int, 3> dims;
    dims.fill(0);
    MPI_Dims_create(nbthreads, 3, &dims[0]);

    auto threadblockx = dims[0];
    auto threadblocky = dims[1];
    auto threadblockz = dims[2];

    auto ptx = pbx / threadblockx;
    auto pty = pby / threadblocky;
    auto ptz = pbz / threadblockz;

    // std::cout << "TBlockx: " << threadblockx << " TBlocky: " << threadblocky << " TBlockz: " << threadblockz << " ptx: " << ptx << " pty: " << pty << " ptz: " << ptz << "\n";

    typename Dune::BCRSMatrix<B,Alloc>::CreateIterator i = A.createbegin();

    auto overlapindices = setupoverlapindices3D(N, pardata);

    for (size_t j = 0; j < nbthreads; j++)
        for (size_t k = 0; k < ptx*pty*ptz; k++, ++i)
        {
            // offset so each block is mapped correctly
            auto block_index = i.index() - j * ptx*pty*ptz;

            // thread local x and y coords
            size_t x = block_index%ptx;
            size_t y = (block_index/ptx) % pty;
            size_t z = (block_index/(ptx*pty)) % ptz;
            // coordinates of the current thread block
            auto tx = j%dims[0];
            auto ty = (j/dims[0]) % threadblocky;
            auto tz = (j/(dims[0] * dims[1])) % threadblockz;
            
            auto overlap_index = overlapindices[i.index()];

            if (k == 0)
                Parallelton<Dune::ThreadPool>::threadstarts.emplace_back(overlap_index);

            // intra thread dependencies
            // center
            i.insert(overlap_index);
            // left neighbor
            if (x > 0)
            {
                i.insert(overlap_index-1);
                // std::cout << "Neighbor left: " << overlap_index-1 << "\n";
            }  
            // right neighbor
            if (x < ptx-1)
            {
                i.insert(overlap_index+1);
                // std::cout << "Neighbor right: " << overlap_index+1 << "\n";
            }
            // lower neighbor
            if (y > 0)
            {
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i.insert(overlap_index - elementsx);
                // std::cout << "Neighbor low: " << overlap_index - elementsx << "\n";
            }
            // upper neighbor
            if (y < pty-1)
            {
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i.insert(overlap_index + elementsx);
                // std::cout << "Neighbor high: " << overlap_index + elementsx << "\n";
            }
            // front neighbor
            if (z > 0)
            {
                // elements in x and y direction per thread block
                auto elementsx = ptx;
                auto elementsy = pty;

                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                i.insert(overlap_index - elementsx*elementsy);
                // std::cout << "Neighbor front: " << overlap_index - elementsx*elementsy << "\n";
            }
            // back neighbor
            if (z < ptz-1)
            {
                // elements in x and y direction per thread block
                auto elementsx = ptx;
                auto elementsy = pty;

                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                i.insert(overlap_index + elementsx*elementsy);
                // std::cout << "Neighbor back: " << overlap_index + elementsx*elementsy << "\n";
            }

            // inter thread dependencies

            // threadblock to the left exists
            if (x == 0 && tx > 0)
            {
                auto tmp = overlap_index - (ptx*(pty-1) + 1); // baseline distance (used in center where there is no overlap)
                if(tx-1 == 0)
                    tmp -= (pty-1) - y; // left overlap of first thread block
                if(tx == threadblockx-1)
                    tmp -= y;           // right overlap
                if(tx == threadblockx-1 && ty == 0)
                    tmp -= 1;           // adjust for bottom right corner
                if(tx == 1 && ty == threadblocky-1)
                    tmp -= 1;           // adjust for top left corner
                if(ty == 0 || ty == threadblocky-1)
                    tmp -= ptx;         // bottom or top overlap
                if(ty == 0 && ty == threadblocky-1)
                    tmp -= ptx;         // bottom and top overlap
                // z distance (how many layer from the current layer and the neighbor layer)
                size_t currentelementsx = ptx, currentelementsy = pty, neighborelementsx = ptx, neighborelementsy = pty; 
                if (tx == threadblockx-1)
                    currentelementsx++;
                if (ty == 0 || ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (ty == 0 && ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (tx-1 == 0)
                    neighborelementsx++;

                auto currentoffsetz = z * currentelementsx * currentelementsy;
                auto neighboroffsetz = ((ptz-1) - z) * neighborelementsx * neighborelementsy;

                i.insert(tmp - (currentoffsetz + neighboroffsetz));
                // std::cout << "Neighbor left Thread Block: " << tmp - (currentoffsetz + neighboroffsetz) << "\n";
            }

            // threadblock to the right exists
            if (x == ptx-1 && tx < threadblockx-1)
            {    
                auto tmp = overlap_index + ptx*(pty-1) + 1; // baseline distance
                if(tx == 0)
                    tmp += (pty-1) - y;
                if(tx+1 == threadblockx-1)
                    tmp += y;
                if(tx+1 == threadblockx-1 && ty == 0)
                    tmp += 1;
                if(tx == 0 && ty == threadblocky-1)
                    tmp += 1;
                if(ty == 0 || ty == threadblocky-1)
                    tmp += ptx;
                if(ty == 0 && ty == threadblocky-1)
                    tmp += ptx;
                // z distance (how many layer from the current layer and the neighbor layer)
                size_t currentelementsx = ptx, currentelementsy = pty, neighborelementsx = ptx, neighborelementsy = pty; 
                if (tx == 0)
                    currentelementsx++;
                if (ty == 0 || ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (ty == 0 && ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (tx+1 == threadblockx-1)
                    neighborelementsx++;

                auto currentoffsetz = ((ptz-1) - z) * currentelementsx * currentelementsy;
                auto neighboroffsetz = z * neighborelementsx * neighborelementsy;

                i.insert(tmp + (currentoffsetz + neighboroffsetz));
                // std::cout << "Neighbor right Thread Block: " << tmp + (currentoffsetz + neighboroffsetz) << "\n";
            }
            
            // threadblock to the top exists
            if (y == pty-1 && ty < threadblocky-1)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                auto tmp = overlap_index + elementsx + (threadblockz-1 - z) * elementsx*elementsy; // one row and the rest of the current thread block
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = ty + (tx + l)/threadblockx;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp += currentelementsx * currentelementsy * ptz;
                }
                // the threadblock above the current
                elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty+1 == 0 || ty+1 == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty+1 == 0 && ty+1 == threadblocky-1) // overlap on both sides
                    elementsy++;

                tmp += elementsx * elementsy * z;

                i.insert(tmp);
                // std::cout << "Neighbor above Thread Block: " << tmp << "\n";
            }

            // threadblock to the bottom exists
            if (y == 0 && ty > 0)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                auto tmp = overlap_index - (elementsx + z * elementsx*elementsy); // one row and the rest of the current thread block
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = ty-1 + (tx + l)/threadblockx;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp -= currentelementsx * currentelementsy * ptz;
                }
                // the threadblock below the current
                elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty-1 == 0 || ty-1 == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty-1 == 0 && ty-1 == threadblocky-1) // overlap on both sides
                    elementsy++;

                tmp -= elementsx * elementsy * (threadblockz-1 - z);

                i.insert(tmp);
                // std::cout << "Neighbor below Thread Block: " << tmp << "\n";
            }    

            // thread block to the front exists
            if (z == 0 && tz > 0)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                // current and front block
                auto tmp = overlap_index - (elementsx * elementsy); // complete rows of current element and of back neighbor
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx*threadblocky; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = ty + (tx + l)/threadblockx;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp -= currentelementsx * currentelementsy * ptz;
                }

                i.insert(tmp);
                // std::cout << "Neighbor front Thread Block: " << tmp << "\n";
            }

            // thread block to the back exists
            if (z == ptz-1 && tz < threadblockz-1)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                // current and behind block
                auto tmp = overlap_index + (elementsx * elementsy); // complete rows of current element and of back neighbor
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx*threadblocky; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = (ty + (tx + l)/threadblockx) % threadblocky;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp += currentelementsx * currentelementsy * ptz;
                }

                i.insert(tmp);
                // std::cout << "Neighbor back Thread Block: " << tmp << "\n";
            }


            // inter rank dependencies

            // rank block to the left exists
            if (x == 0 && tx == 0 && rx > 0)
            {
                i.insert(overlap_index - 1);
                // std::cout << "Neighbor left rank: " << overlap_index - 1 << "\n";
            }
            // rank block to the right exists
            if (x == ptx-1 && tx == threadblockx-1 && rx < blockx-1)
            {
                i.insert(overlap_index + 1);
                // std::cout << "Neighbor right rank: " << overlap_index + 1 << "\n";
            }
            // rank block to bottom exists
            if (y == 0 && ty == 0 && ry > 0)
            {   
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;
                
                i.insert(overlap_index - elementsx);
                // std::cout << "Neighbor bot rank: " << overlap_index - elementsx << "\n";        
            }
            // rank block to the top exists
            if (y == pty-1 && ty == threadblocky-1 && ry < blocky-1)
            {
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;
            
                i.insert(overlap_index + elementsx);
                // std::cout << "Neighbor top rank: " << overlap_index + elementsx << "\n";
            }
            // rank block to the front exists
            if (z == 0 && tz == 0 && rz > 0)
            {
                i.insert(overlap_index - (pbx+2) * (pby+2));
                // std::cout << "Neighbor front rank: " << overlap_index - (pbx+2) * (pby+2) << "\n";
            }
            // rank block to the back exists
            if (z == ptz-1 && tz == threadblockz-1 && rz < blockz-1)
            {
                i.insert(overlap_index + (pbx+2) * (pby+2));
                // std::cout << "Neighbor back rank: " << overlap_index + (pbx+2) * (pby+2) << "\n";
            }

            // if (overlap_index >= 1624)
            // {
            //     char c;
            //     std::cin >> c;
            //     std::cout << "x:" << x << " y:" << y << " z:" << z << " tx:" << tx << " ty:" << ty << " tz: " << tz << 
            //                 " rx:" << rx << " ry:" << ry << " rz:" << rz << " OID: " << overlap_index << std::endl;
            // }
        }
    
    // exit(1);
}



template<class B, class Alloc>
void setupBlockedLaplacian3D(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
    typedef typename Dune::BCRSMatrix<B,Alloc>::field_type FieldType;

    setupBlockedSparsity3D(A,N,pardata);

    B diagonal(static_cast<FieldType>(0)), bone(static_cast<FieldType>(0));

    auto setDiagonal = [](auto&& scalarOrMatrix, const auto& value) {
        auto&& matrix = Dune::Impl::asMatrix(scalarOrMatrix);
        for (auto rowIt = matrix.begin(); rowIt != matrix.end(); ++rowIt)
        (*rowIt)[rowIt.index()] = value;
    };

    setDiagonal(diagonal, 7.0);
    setDiagonal(bone, -1.0);

    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    auto blockx = pardata.getblockx();
    auto blocky = pardata.getblocky();
    auto blockz = pardata.getblockz();
    auto size = pardata.getsize();
    auto rank = pardata.getrank();
    auto start = pardata.getstart();
    // coordinates of the current rank block
    auto rx = pardata.getxcoord();
    auto ry = pardata.getycoord();
    auto rz = pardata.getzcoord();

    
    auto nbrows = N*N*N / size;
    auto nbcols = (pbx+2) * (pby+2) * (pbz+2);

    auto nbthreads = Parallelton<Dune::ThreadPool>::context->getnbthreads();
    std::array<int, 3> dims;
    dims.fill(0);
    MPI_Dims_create(nbthreads, 3, &dims[0]);

    auto threadblockx = dims[0];
    auto threadblocky = dims[1];
    auto threadblockz = dims[2];

    auto ptx = pbx / threadblockx;
    auto pty = pby / threadblocky;
    auto ptz = pbz / threadblockz;

    auto overlapindices = setupoverlapindices3D(N, pardata);

    typename Dune::BCRSMatrix<B,Alloc>::RowIterator i = A.begin();

    for (size_t j = 0; j < nbthreads; j++)
        for (size_t k = 0; k < ptx*pty*ptz; k++, ++i)
        {
            // offset so each block is mapped correctly
            auto block_index = i.index() - j * ptx*pty*ptz;

            // thread local x and y coords
            size_t x = block_index%ptx;
            size_t y = (block_index/ptx) % pty;
            size_t z = (block_index/(ptx*pty)) % ptz;
            // coordinates of the current thread block
            auto tx = j%dims[0];
            auto ty = (j/dims[0]) % threadblocky;
            auto tz = (j/(dims[0] * dims[1])) % threadblockz;
            
            auto overlap_index = overlapindices[i.index()];

            // if (overlap_index >= 405)
            // {
            //     std::cin >> c;
            //     std::cout << "x:" << x << " y:" << y << " z:" << z << " tx:" << tx << " ty:" << ty << " tz: " << tz << 
            //                 " rx:" << rx << " ry:" << ry << " rz:" << rz << " OID: " << overlap_index << std::endl;
            // }

            // intra thread dependencies
            // center
            i->operator[](overlap_index)=diagonal;
            // left neighbor
            if (x > 0)
            {
                i->operator[](overlap_index-1)=bone;
                // std::cout << "Neighbor left: " << overlap_index-1 << "\n";
            }  
            // right neighbor
            if (x < ptx-1)
            {
                i->operator[](overlap_index+1)=bone;
                // std::cout << "Neighbor right: " << overlap_index+1 << "\n";
            }
            // lower neighbor
            if (y > 0)
            {
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i->operator[](overlap_index - elementsx)=bone;
            }
            // upper neighbor
            if (y < pty-1)
            {    
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i->operator[](overlap_index + elementsx)=bone;
            }
            // front neighbor
            if (z > 0)
            {
                // elements in x and y direction per thread block
                auto elementsx = ptx;
                auto elementsy = pty;

                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                i->operator[](overlap_index - elementsx*elementsy)=bone;
            }
            // back neighbor
            if (z < ptz-1)
            {
                // elements in x and y direction per thread block
                auto elementsx = ptx;
                auto elementsy = pty;

                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                i->operator[](overlap_index + elementsx*elementsy)=bone;
            }

            // inter thread dependencies

            // threadblock to the left exists
            if (x == 0 && tx > 0)
            {
                auto tmp = overlap_index - (ptx*(pty-1) + 1); // baseline distance (used in center where there is no overlap)
                if(tx-1 == 0)
                    tmp -= (pty-1) - y; // left overlap of first thread block
                if(tx == threadblockx-1)
                    tmp -= y;           // right overlap
                if(tx == threadblockx-1 && ty == 0)
                    tmp -= 1;           // adjust for bottom right corner
                if(tx == 1 && ty == threadblocky-1)
                    tmp -= 1;           // adjust for top left corner
                if(ty == 0 || ty == threadblocky-1)
                    tmp -= ptx;         // bottom or top overlap
                if(ty == 0 && ty == threadblocky-1)
                    tmp -= ptx;         // bottom and top overlap
                // z distance (how many layer from the current layer and the neighbor layer)
                size_t currentelementsx = ptx, currentelementsy = pty, neighborelementsx = ptx, neighborelementsy = pty; 
                if (tx == threadblockx-1)
                    currentelementsx++;
                if (ty == 0 || ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (ty == 0 && ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (tx-1 == 0)
                    neighborelementsx++;

                auto currentoffsetz = z * currentelementsx * currentelementsy;
                auto neighboroffsetz = ((ptz-1) - z) * neighborelementsx * neighborelementsy;

                i->operator[](tmp - (currentoffsetz + neighboroffsetz))=bone;
            }

            // threadblock to the right exists
            if (x == ptx-1 && tx < threadblockx-1)
            {    
                auto tmp = overlap_index + ptx*(pty-1) + 1; // baseline distance
                if(tx == 0)
                    tmp += (pty-1) - y;
                if(tx+1 == threadblockx-1)
                    tmp += y;
                if(tx+1 == threadblockx-1 && ty == 0)
                    tmp += 1;
                if(tx == 0 && ty == threadblocky-1)
                    tmp += 1;
                if(ty == 0 || ty == threadblocky-1)
                    tmp += ptx;
                if(ty == 0 && ty == threadblocky-1)
                    tmp += ptx;
                // z distance (how many layer from the current layer and the neighbor layer)
                size_t currentelementsx = ptx, currentelementsy = pty, neighborelementsx = ptx, neighborelementsy = pty; 
                if (tx == 0)
                    currentelementsx++;
                if (ty == 0 || ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (ty == 0 && ty == threadblocky-1)
                {
                    currentelementsy++;
                    neighborelementsy++;
                }
                if (tx+1 == threadblockx-1)
                    neighborelementsx++;

                auto currentoffsetz = ((ptz-1) - z) * currentelementsx * currentelementsy;
                auto neighboroffsetz = z * neighborelementsx * neighborelementsy;

                i->operator[](tmp + (currentoffsetz + neighboroffsetz))=bone;
            }
            
            // threadblock to the top exists
            if (y == pty-1 && ty < threadblocky-1)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                auto tmp = overlap_index + elementsx + (threadblockz-1 - z) * elementsx*elementsy; // one row and the rest of the current thread block
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = ty + (tx + l)/threadblockx;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp += currentelementsx * currentelementsy * ptz;
                }
                // the threadblock above the current
                elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty+1 == 0 || ty+1 == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty+1 == 0 && ty+1 == threadblocky-1) // overlap on both sides
                    elementsy++;

                tmp += elementsx * elementsy * z;

                i->operator[](tmp)=bone;
            }

            // threadblock to the bottom exists
            if (y == 0 && ty > 0)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                auto tmp = overlap_index - (elementsx + z * elementsx*elementsy); // one row and the rest of the current thread block
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = ty-1 + (tx + l)/threadblockx;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp -= currentelementsx * currentelementsy * ptz;
                }
                // the threadblock below the current
                elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty-1 == 0 || ty-1 == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty-1 == 0 && ty-1 == threadblocky-1) // overlap on both sides
                    elementsy++;

                tmp -= elementsx * elementsy * (threadblockz-1 - z);

                i->operator[](tmp)=bone;
            }    

            // thread block to the front exists
            if (z == 0 && tz > 0)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                // current and front block
                auto tmp = overlap_index - (elementsx * elementsy); // complete rows of current element and of back neighbor
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx*threadblocky; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = ty + (tx + l)/threadblockx;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp -= currentelementsx * currentelementsy * ptz;
                }

                i->operator[](tmp)=bone;
            }

            // thread block to the back exists
            if (z == ptz-1 && tz < threadblockz-1)
            {
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                // current and behind block
                auto tmp = overlap_index + (elementsx * elementsy); // complete rows of current element and of back neighbor
                // looping of all thread blocks inbetween
                for (size_t l = 1; l < threadblockx*threadblocky; l++)
                {
                    auto currenttx = (tx + l) % threadblockx;
                    auto currentty = (ty + (tx + l)/threadblockx) % threadblocky;
                    auto currentelementsx = ptx, currentelementsy = pty;
                    
                    if (currenttx == 0 || currenttx == threadblockx-1) // overlap on one side
                        currentelementsx++;
                    if (currenttx == 0 && currenttx == threadblockx-1) // overlap on both sides
                        currentelementsx++;

                    if (currentty == 0 || currentty == threadblocky-1) // overlap on one side
                        currentelementsy++;
                    if (currentty == 0 && currentty == threadblocky-1) // overlap on both sides
                        currentelementsy++;

                    tmp += currentelementsx * currentelementsy * ptz;
                }

                i->operator[](tmp)=bone;
            }


            // inter rank dependencies

            // rank block to the left exists
            if (x == 0 && tx == 0 && rx > 0)
            {
                i->operator[](overlap_index - 1)=bone;
            }
            // rank block to the right exists
            if (x == ptx-1 && tx == threadblockx-1 && rx < blockx-1)
            {
                i->operator[](overlap_index + 1)=bone;
            }
            // rank block to bottom exists
            if (y == 0 && ty == 0 && ry > 0)
            {   
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i->operator[](overlap_index - elementsx)=bone;
            }
            // rank block to the top exists
            if (y == pty-1 && ty == threadblocky-1 && ry < blocky-1)
            {
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;
            
                i->operator[](overlap_index + elementsx)=bone;
            }
            // rank block to the front exists
            if (z == 0 && tz == 0 && rz > 0)
            {
                i->operator[](overlap_index - (pbx+2) * (pby+2))=bone;
            }
            // rank block to the back exists
            if (z == ptz-1 && tz == threadblockz-1 && rz < blockz-1)
            {
                i->operator[](overlap_index + (pbx+2) * (pby+2))=bone;
            }
        } 
}


#endif