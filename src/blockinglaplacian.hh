#ifndef BLOCKING_PATTERN_HH
#define BLOCKING_PATTERN_HH


#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/scalarmatrixview.hh>
#include <algorithm>
#include "parcommunicator.hh"



std::vector<size_t> setupoverlapindices2D(size_t N, Dune::ParData pardata)
{
    std::vector<size_t> overlaps;
    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto blockx = pardata.getblockx();
    auto blocky = pardata.getblocky();
    auto size = pardata.getsize();
    auto rank = pardata.getrank();
    auto start = pardata.getstart();
    // coordinates of the current rank block
    auto rx = pardata.getxcoord();
    auto ry = pardata.getycoord();

    auto nbthreads = Parallelton<Dune::ThreadPool>::context->getnbthreads();
    std::array<int, 2> dims;
    dims.fill(0);
    MPI_Dims_create(nbthreads, 2, &dims[0]);

    auto threadblockx = dims[0];
    auto threadblocky = dims[1];

    auto ptx = pbx / threadblockx;
    auto pty = pby / threadblocky;

    auto overlap = 1ul;
    auto index = 0ul;

    for (size_t j = 0; j < nbthreads; j++)
        for (size_t k = 0; k < ptx*pty; k++, ++index)
        {
           // offset so each block is mapped correctly
            auto block_index = index - j * ptx*pty;

            // thread local x and y coords
            size_t x = block_index%ptx;
            size_t y = block_index/ptx;
            // coordinates of the current thread block
            auto tx = j%dims[0];
            auto ty = j/dims[0];
            
            // overlap on the left
            if (tx == 0 && x == 0)
                overlap += 1;
            // overlap on the bottom
            if (ty == 0 && index % (ptx*pty) == 0)
                overlap += ptx;
            // overlap on the top
            if (ty == threadblocky-1 && tx > 0 && x == 0 && y == 0)
                overlap += ptx;
            // overlap on the right
            if (tx == threadblockx-1 && x == 0 && y > 0)
                overlap += 1;
            // overlap for completing one row of thread blocks (last overlap for the last block)
            if (index == ptx*pty * threadblockx * ty && index != 0)
                overlap += 1;
            // overlap for bottom right corner
            if (ty == 0 && tx == threadblockx-1 && x == 0 && y == 0)
                overlap += 1;
            // overlap for top left corner
            if (ty == threadblocky-1 && tx == 1 && x == 0 && y == 0)
                overlap += 1;

            overlaps.emplace_back(index + overlap);
        }
    return overlaps;
}




template<class B, class Alloc>
void setupBlockedSparsity2D(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
    typedef typename Dune::BCRSMatrix<B,Alloc> Matrix;

    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto blockx = pardata.getblockx();
    auto blocky = pardata.getblocky();
    auto size = pardata.getsize();
    auto rank = pardata.getrank();
    auto start = pardata.getstart();
    // coordinates of the current rank block
    auto rx = pardata.getxcoord();
    auto ry = pardata.getycoord();

    auto nbrows = N*N / size;
    auto nbcols = (pbx+2) * (pby+2);

    A.setSize(nbrows, nbcols, nbrows*5ul);
    A.setBuildMode(Matrix::row_wise);

    auto nbthreads = Parallelton<Dune::ThreadPool>::context->getnbthreads();
    std::array<int, 2> dims;
    dims.fill(0);
    MPI_Dims_create(nbthreads, 2, &dims[0]);

    auto threadblockx = dims[0];
    auto threadblocky = dims[1];

    auto ptx = pbx / threadblockx;
    auto pty = pby / threadblocky;

    // char c;
    // std::cout << threadblockx << " " << threadblocky << " " << ptx << " " << pty << "\n";

    typename Dune::BCRSMatrix<B,Alloc>::CreateIterator i = A.createbegin();

    auto overlapindices = setupoverlapindices2D(N, pardata);

    for (size_t j = 0; j < nbthreads; j++)
        for (size_t k = 0; k < ptx*pty; k++, ++i)
        {
            // offset so each block is mapped correctly
            auto block_index = i.index() - j * ptx*pty;

            // thread local x and y coords
            size_t x = block_index%ptx;
            size_t y = block_index/ptx;
            // coordinates of the current thread block
            auto tx = j%dims[0];
            auto ty = j/dims[0];
            
            auto overlap_index = overlapindices[i.index()];
            // std::cin >> c;
            // std::cout << "x:" << x << " y:" << y << " tx:" << tx << " ty:" << ty << " rx:" << rx << " ry:" << ry << " OID: " << overlap_index << std::endl;

            if (k == 0)
                Parallelton<Dune::ThreadPool>::threadstarts.emplace_back(overlap_index);

            // intra thread dependencies
            // center
            i.insert(overlap_index);
            // left neighbor
            if (x > 0)
            {
                i.insert(overlap_index-1);
                // std::cout << "Neighbor left: " << overlap_index-1 << "\n";
            }  
            // right neighbor
            if (x < ptx-1)
            {
                i.insert(overlap_index+1);
                // std::cout << "Neighbor right: " << overlap_index+1 << "\n";
            }
            // lower neighbor
            if (y > 0)
            {
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i.insert(overlap_index - elementsx);
                // std::cout << "Neighbor low: " << overlap_index - elementsx << "\n";
            }
            // upper neighbor
            if (y < pty-1)
            {    
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i.insert(overlap_index + elementsx);
                // std::cout << "Neighbor high: " << overlap_index + elementsx << "\n";
            }

            // inter thread dependencies

            // threadblock to the left exists
            if (x == 0 && tx > 0)
            {
                auto tmp = overlap_index - (ptx*(pty-1) + 1); // baseline distance (used in center where there is no overlap)
                if(tx-1 == 0)
                    tmp -= (pty-1) - y; // left overlap of first thread block
                if(tx == threadblockx-1)
                    tmp -= y;           // right overlap
                if(tx == threadblockx-1 && ty == 0)
                    tmp -= 1;           // adjust for bottom right corner
                if(tx == 1 && ty == threadblocky-1)
                    tmp -= 1;           // adjust for top left corner
                if(ty == 0 || ty == threadblocky-1)
                    tmp -= ptx;         // bottom or top overlap
                if(ty == 0 && ty == threadblocky-1)
                    tmp -= ptx;         // bottom and top overlap

                i.insert(tmp);
                // std::cout << "Neighbor left Thread Block: " << tmp << "\n";
            }

            // threadblock to the right exists
            if (x == ptx-1 && tx < threadblockx-1)
            {    
                auto tmp = overlap_index + ptx*(pty-1) + 1; // baseline distance
                if(tx == 0)
                    tmp += (pty-1) - y;
                if(tx+1 == threadblockx-1)
                    tmp += y;
                if(tx+1 == threadblockx-1 && ty == 0)
                    tmp += 1;
                if(tx == 0 && ty == threadblocky-1)
                    tmp += 1;
                if(ty == 0 || ty == threadblocky-1)
                    tmp += ptx;
                if(ty == 0 && ty == threadblocky-1)
                    tmp += ptx;
                
                i.insert(tmp);
                // std::cout << "Neighbor right Thread Block: " << tmp << "\n";
            }
            
            // threadblock to the top exists
            if (y == pty-1 && ty < threadblocky-1)
            {
                auto tmp = overlap_index + ((threadblockx-1) - tx) * ptx*pty + ptx; // how many block in same row + blocksize + one row
                
                if (tx == 0 || tx == threadblockx-1)
                    tmp += pty + 1; // side overlap for first and last block (only one element overlapped in above block or overlap already in overlapindex)
                else
                    tmp += 2*pty; // overlap on both sides full                    
                if (ty == 0 && tx < threadblockx-1)        // in the lowest row we have the bottom overlap. in the last block we already covered the corner
                    tmp += 1 + (threadblockx-1 - tx) * ptx; // bottom right corner + bottom overlap of each block
                if (ty+1 == threadblocky-1 && tx > 0)        // in the highest row we have the top overlap
                    tmp += 1 + tx * ptx; // top left corner + top overlap of each block
                // blocks in the above row
                tmp += tx * ptx*pty;    // blocks in the above row are always without overlap

                i.insert(tmp);
                // std::cout << "Neighbor above Thread Block: " << tmp << "\n";
            }

            // threadblock to the bottom exists
            if (y == 0 && ty > 0)
            {
                auto tmp = overlap_index - ((threadblockx-1 - tx) * ptx*pty + ptx); // how many block in below row + blocksize + one row

                if (tx == threadblockx-1 || tx == 0)
                    tmp -= pty + 1; // side overlap for first block (only one element overlapped in above block)
                else
                    tmp -= 2*pty; // overlap on both sides full
                if (ty-1 == 0 && tx < threadblockx-1)        // in the lowest row we have the bottom overlap
                    tmp -= 1 + (threadblockx-1 - tx) * ptx; // bottom right corner + bottom overlap of each block
                if (ty == threadblocky-1 && tx > 0)        // in the highest row we have the top overlap
                    tmp -= 1 + tx * ptx; // top left corner + top overlap of each block

                // blocks in the current row
                tmp -= tx * ptx*pty;    // blocks in the current row are always without overlap

                i.insert(tmp);
                // std::cout << "Neighbor bottom Thread Block: " << tmp << "\n";
            }    

            // inter rank dependencies

            // rank block to the left exists
            if (x == 0 && tx == 0 && rx > 0)
            {
                i.insert(overlap_index - 1);
                // std::cout << "Neighbor left rank: " << overlap_index - 1 << "\n";
            }
            // rank block to the right exists
            if (x == ptx-1 && tx == threadblockx-1 && rx < blockx-1)
            {
                i.insert(overlap_index + 1);
                // std::cout << "Neighbor right rank: " << overlap_index + 1 << "\n";
            }
            // rank block to bottom exists
            if (y == 0 && ty == 0 && ry > 0)
            {    
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;
                
                i.insert(overlap_index - elementsx);
            }
            // rank block to the top exists
            if (y == pty-1 && ty == threadblocky-1 && ry < blocky-1)
            {   
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;
            
                i.insert(overlap_index + elementsx);
            }
        }
}



template<class B, class Alloc>
void setupBlockedLaplacian2D(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
    typedef typename Dune::BCRSMatrix<B,Alloc>::field_type FieldType;

    setupBlockedSparsity2D(A,N,pardata);

    B diagonal(static_cast<FieldType>(0)), bone(static_cast<FieldType>(0));

    auto setDiagonal = [](auto&& scalarOrMatrix, const auto& value) {
        auto&& matrix = Dune::Impl::asMatrix(scalarOrMatrix);
        for (auto rowIt = matrix.begin(); rowIt != matrix.end(); ++rowIt)
        (*rowIt)[rowIt.index()] = value;
    };

    setDiagonal(diagonal, 4.0);
    setDiagonal(bone, -1.0);

    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto blockx = pardata.getblockx();
    auto blocky = pardata.getblocky();
    auto size = pardata.getsize();
    auto rank = pardata.getrank();
    auto start = pardata.getstart();
    // coordinates of the current rank block
    auto rx = pardata.getxcoord();
    auto ry = pardata.getycoord();


    
    auto nbrows = N*N / size;
    auto nbcols = (pbx+2) * (pby+2);

    auto nbthreads = Parallelton<Dune::ThreadPool>::context->getnbthreads();
    std::array<int, 2> dims;
    dims.fill(0);
    MPI_Dims_create(nbthreads, 2, &dims[0]);

    auto threadblockx = dims[0];
    auto threadblocky = dims[1];

    auto ptx = pbx / threadblockx;
    auto pty = pby / threadblocky;

    auto overlapindices = setupoverlapindices2D(N, pardata);

    typename Dune::BCRSMatrix<B,Alloc>::RowIterator i = A.begin();

    for (size_t j = 0; j < nbthreads; j++)
        for (size_t k = 0; k < ptx*pty; k++, ++i)
       {
            // offset so each block is mapped correctly
            auto block_index = i.index() - j * ptx*pty;

            // thread local x and y coords
            size_t x = block_index%ptx;
            size_t y = block_index/ptx;
            // coordinates of the current thread block
            auto tx = j%dims[0];
            auto ty = j/dims[0];
            
            auto overlap_index = overlapindices[i.index()];
            
            // intra thread dependencies
            // center
            i->operator[](overlap_index)=diagonal;
            // left neighbor
            if (x > 0)
                i->operator[](overlap_index-1)=bone;
            // right neighbor
            if (x < ptx-1)
                i->operator[](overlap_index+1)=bone;
            // lower neighbor
            if (y > 0)
            {
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i->operator[](overlap_index - elementsx)=bone;
            }
            // upper neighbor
            if (y < pty-1)
            {    
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                i->operator[](overlap_index + elementsx)=bone;
            }

            // inter thread dependencies

            // threadblock to the left exists
            if (x == 0 && tx > 0)
            {
                auto tmp = overlap_index - (ptx*(pty-1) + 1); // baseline distance (used in center where there is no overlap)
                if(tx-1 == 0)
                    tmp -= (pty-1) - y; // left overlap of first thread block
                if(tx == threadblockx-1)
                    tmp -= y;           // right overlap
                if(tx == threadblockx-1 && ty == 0)
                    tmp -= 1;           // adjust for bottom right corner
                if(tx == 1 && ty == threadblocky-1)
                    tmp -= 1;           // adjust for top left corner
               if(ty == 0 || ty == threadblocky-1)
                    tmp -= ptx;         // bottom or top overlap
                if(ty == 0 && ty == threadblocky-1)
                    tmp -= ptx;         // bottom and top overlap

                i->operator[](tmp)=bone;
                // std::cout << "Neighbor in left Thread Block: " << tmp << "\n";
            }

            // threadblock to the right exists
            if (x == ptx-1 && tx < threadblockx-1)
            {    
                auto tmp = overlap_index + ptx*(pty-1) + 1; // baseline distance
                if(tx == 0)
                    tmp += (pty-1) - y;
                if(tx+1 == threadblockx-1)
                    tmp += y;
                if(tx+1 == threadblockx-1 && ty == 0)
                    tmp += 1;
                if(tx == 0 && ty == threadblocky-1)
                    tmp += 1;
                if(ty == 0 || ty == threadblocky-1)
                    tmp += ptx;
                if(ty == 0 && ty == threadblocky-1)
                    tmp += ptx;
                
                i->operator[](tmp)=bone;
                // std::cout << "Neighbor in right Thread Block: " << tmp << "\n";
            }
            
            // threadblock to the top exists
            if (y == pty-1 && ty < threadblocky-1)
            {
                auto tmp = overlap_index + ((threadblockx-1) - tx) * ptx*pty + ptx; // how many block in same row + blocksize + one row
                
                if (tx == 0 || tx == threadblockx-1)
                    tmp += pty + 1; // side overlap for first and last block (only one element overlapped in above block or overlap already in overlapindex)
                else
                    tmp += 2*pty; // overlap on both sides full                    
                if (ty == 0 && tx < threadblockx-1)        // in the lowest row we have the bottom overlap. in the last block we already covered the corner
                    tmp += 1 + (threadblockx-1 - tx) * ptx; // bottom right corner + bottom overlap of each block
                if (ty+1 == threadblocky-1 && tx > 0)        // in the highest row we have the top overlap
                    tmp += 1 + tx * ptx; // top left corner + top overlap of each block
                // blocks in the above row
                tmp += tx * ptx*pty;    // blocks in the above row are always without overlap

                i->operator[](tmp)=bone;
                // std::cout << "Neighbor in above Thread Block: " << tmp << "\n";
            }

            // threadblock to the bottom exists
            if (y == 0 && ty > 0)
            {
                auto tmp = overlap_index - ((threadblockx-1 - tx) * ptx*pty + ptx); // how many block in below row + blocksize + one row

                if (tx == threadblockx-1 || tx == 0)
                    tmp -= pty + 1; // side overlap for first block (only one element overlapped in above block)
                else
                    tmp -= 2*pty; // overlap on both sides full
                if (ty-1 == 0 && tx < threadblockx-1)        // in the lowest row we have the bottom overlap
                    tmp -= 1 + (threadblockx-1 - tx) * ptx; // bottom right corner + bottom overlap of each block
                if (ty == threadblocky-1 && tx > 0)        // in the highest row we have the top overlap
                    tmp -= 1 + tx * ptx; // top left corner + top overlap of each block

                // blocks in the current row
                tmp -= tx * ptx*pty;    // blocks in the current row are always without overlap

                i->operator[](tmp)=bone;
                // std::cout << "Neighbor in bottom Thread Block: " << tmp << "\n";
            }    

            // inter rank dependencies

            // rank block to the left exists
            if (x == 0 && tx == 0 && rx > 0)
                i->operator[](overlap_index - 1)=bone;
            // rank block to the right exists
            if (x == ptx-1 && tx == threadblockx-1 && rx < blockx-1)
                i->operator[](overlap_index + 1)=bone;
            // rank block to bottom exists
            if (y == 0 && ty == 0 && ry > 0)
            {    
                 auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;
                
                i->operator[](overlap_index - elementsx)=bone;
            }
            // rank block to the top exists
            if (y == pty-1 && ty == threadblocky-1 && ry < blocky-1)
            {   
                auto elementsx = ptx;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;
            
                i->operator[](overlap_index + elementsx)=bone;
            }
        } 
}


#endif