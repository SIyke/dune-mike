#include "threadpool.hh"

namespace Dune
{
    Direction reverse(Direction d)
    {
        switch(d)
        {
            case Direction::right:
                return Direction::left;
            case Direction::left:
                return Direction::right;
            case Direction::up:
                return Direction::down;
            case Direction::down:
                return Direction::up;
            case Direction::back:
                return Direction::front;
            case Direction::front:
                return Direction::back;
        }
        return Direction::right;
    }


    ThreadPool::ThreadPool(size_t n)
    : thread_number(n), threads(n-1), thread_based_tasks(n), barrier(n), queue_locks(n), taskcompleted(n)
    {
        tasks_available = new (std::align_val_t {64}) padded_aint [n];
        // tasks_available = new padded_aint [n];
        if(sizeof(padded_aint) != 64)
            std::cout << "Problem: padded_int struct is of size " << sizeof(padded_aint) << std::endl;

        createthreads();

        tasks_available[0].set(0ul);
        // tasks_available[0].clear_neighbors();
        tasks_available[0].intset_constructor();

        barrier.wait(0);
    }

    ThreadPool::~ThreadPool()
    {
        // waittasks();
        auto start = std::chrono::high_resolution_clock::now();
        while (!notasksleft())
        {
            auto end = std::chrono::high_resolution_clock::now();
            if (std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count() > 5.0)
            {
                std::cout << "Tasks:" << std::endl;
                for(size_t i = 0; i < thread_number; i++)
                    std::cout << tasks_available[i].get();
                std::cout << std::endl;

                exit(1);
            }
        }
        // std::cout << "T0 had " << worktime << " work time\n";
        running = false;
        destroythreads();
        delete[] tasks_available;
    }

    bool ThreadPool::notasksleft()
    {
        for(size_t i = 0; i < thread_number; i++)
            if(tasks_available[i].get() != 0)
                return false;
        return true;
    }

    // void ThreadPool::waittasks()
    // {
    //     std::function<void()> task;

    //     while (true)
    //     {
    //         // if(task_number.load(std::memory_order_acquire) == 0)
    //         // if(notasksleft())
    //         if (tasks_available[0].get() == 0)
    //         {
    //             break;
    //         }
    //         else if (poptask(task, 0))
    //         {
    //             // workstart = std::chrono::high_resolution_clock::now();
    //             task();
    //             // workend = std::chrono::high_resolution_clock::now();
    //             // worktime += std::chrono::duration_cast<std::chrono::duration<double>> (workend - workstart).count();

    //             // taskcompleted[0] = std::chrono::high_resolution_clock::now();
                
    //             // task_number.fetch_sub(1, std::memory_order_acq_rel);
    //             tasks_available[0] -= 1;                
    //         }
    //     }
    // }

    void ThreadPool::waittasks()
    {
        while (!notasksleft())
        {}
    }

    void ThreadPool::thread0task()
    {
        std::function<void()> task;
        if (poptask(task, 0))
        {
            // workstart = std::chrono::high_resolution_clock::now();
            task();
            // workend = std::chrono::high_resolution_clock::now();
            // worktime += std::chrono::duration_cast<std::chrono::duration<double>> (workend - workstart).count();

            // taskcompleted[0] = std::chrono::high_resolution_clock::now();
            
            // task_number.fetch_sub(1, std::memory_order_acq_rel);
            tasks_available[0] -= 1;
            thread0localsnyc = !thread0localsnyc;
        }

        if (thread_number > 1)
        {
            if (thread0localsnyc)
            {
                // give signal to other thread
                
                for (auto const& p : neighbors)
                {
                    if (0ul + p.second > thread_number-1)
                        tasks_available[0].increase_neighbor(reverse(p.first));
                    else
                        tasks_available[0 + p.second].increase_neighbor(p.first);
                }

                // auto start = std::chrono::high_resolution_clock::now();
                {
                    using namespace std::chrono_literals;
                    auto sleep = 1us;
                    while (!tasks_available[0].neighbors_ready_and_clear(neighbors.size()))
                    {
                        // auto end = std::chrono::high_resolution_clock::now();
                        // if (std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count() > 5.0)
                        // {
                        //     std::cout << "stuck at " << 0 << std::endl;
                        //     exit(1);
                        // }

                        // std::this_thread::sleep_for(sleep);
                        // sleep *= 3.0;
                    }
                }
            }
        }   
    }

    void ThreadPool::startprogressthread(MPI_Request& request)
    {
        auto progress = [&request]()
        {
            int doneflag = false;
            while (!doneflag)
            {
                MPI_Test(&request, &doneflag, MPI_STATUS_IGNORE);
            }
        };

        progressthread = std::thread(progress);
    }

    void ThreadPool::endprogressthread()
    {
        progressthread.join();
    }


    void ThreadPool::createthreads()
    {
        for (size_t i = 1; i < thread_number; i++)
            threads[i-1] = std::thread([this, i]{workloop(i);});
    }

    void ThreadPool::destroythreads()
    {
        for (size_t i = 1; i < thread_number; i++)
            threads[i-1].join();
    }

    void ThreadPool::workloop(size_t id)
    {
        // wait flags touched first
        tasks_available[id].set(0ul);
        // tasks_available[id].clear_neighbors();
        tasks_available[id].intset_constructor();

        barrier.wait(id);
        
        // auto time = 0.0;
        auto worktime = 0.0;
        // auto start = std::chrono::high_resolution_clock::now();
        auto workstart = std::chrono::high_resolution_clock::now();
        auto workend = std::chrono::high_resolution_clock::now();

        // do local synchronization variable
        // row-wise data decomp only needs to wait for thread -1 and +1

        // every two tasks a local sync is needed
        // 1. task: apply 2. task: richardson then local sync
        bool localsync = false;

        while (running)
        {
            std::function<void()> task;
            if(poptask(task, id))
            {
                // workstart = std::chrono::high_resolution_clock::now();
                task();
                // workend = std::chrono::high_resolution_clock::now();
                // worktime += std::chrono::duration_cast<std::chrono::duration<double>> (workend - workstart).count();
                // end of task
                // taskcompleted[id] = std::chrono::high_resolution_clock::now();
                
                // task_number.fetch_sub(1, std::memory_order_acq_rel);
                tasks_available[id] -= 1;
                localsync = !localsync;

                // wait for local sync before continuing execution
                if (localsync)
                {
                    // signal neighbors that we are done
                    for (auto const& p : neighbors)
                    {
                        if (id + p.second > thread_number-1)
                            tasks_available[id].increase_neighbor(reverse(p.first));
                        else
                            tasks_available[id + p.second].increase_neighbor(p.first);
                    }
                 
                    
                    // wait until neighbors signaled they are ready
                    // if they have reset our neighbor counter
                    // auto start = std::chrono::high_resolution_clock::now();
                    {
                        using namespace std::chrono_literals;
                        auto sleep = 1us;
                        while (!tasks_available[id].neighbors_ready_and_clear(neighbors.size()))
                        {
                            // auto end = std::chrono::high_resolution_clock::now();
                            // if (std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count() > 5.0)
                            // {
                            //     std::cout << "stuck at " << id << std::endl;
                            //     exit(1);
                            // }

                            // std::this_thread::sleep_for(sleep);
                            // sleep *= 3.0;
                        }
                    }
                }
            }
            else
            {
                //sleepyield();
            }
        }
        // auto end = std::chrono::high_resolution_clock::now();
        // time = std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();


        // for (auto i = 0; i < thread_number; i++)
        // {
        //     if(i == id)
        //     {
        //         std::lock_guard<std::mutex> lock(mtx);
        //         // std::cout << "T" << id << " had " << worktime << " work time" << std::endl;
        //     }
        // }
    }

    bool ThreadPool::poptask(std::function<void()>& task, size_t id)
    {
        // std::lock_guard<std::mutex> lock(mtx);
        // look for individually assigned tasks
        if (tasks_available[id].get() == 0)                            
        {
            return false;
        }
        else
        {
            std::lock_guard<std::mutex> lock(queue_locks[id]);
            task = std::move(thread_based_tasks[id].front());
            thread_based_tasks[id].pop();
            return true;
        }
    }

    void ThreadPool::sleepyield()
    {
        if(sleepduration)
            std::this_thread::sleep_for(std::chrono::microseconds(sleepduration));
        else
            std::this_thread::yield();
    }

    bool ThreadPool::bindthreads(std::vector<size_t>& logindices)
    {
        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        auto bind = [this](size_t logid)
        {
            // bind this thread to the correct core
            hwloc_bitmap_t set = hwloc_bitmap_alloc();
            if (!set) {
                fprintf(stderr, "failed to allocate a bitmap\n");
                hwloc_topology_destroy(Parallelton<ThreadPool>::topology);
            }

            auto logobj = hwloc_get_obj_by_type(Parallelton<ThreadPool>::topology, HWLOC_OBJ_PU, logid);
            hwloc_bitmap_only(set, logobj->os_index);

            auto err = hwloc_set_cpubind(Parallelton<ThreadPool>::topology, set, HWLOC_CPUBIND_THREAD);
            if (err < 0) {
                fprintf(stderr, "failed to set thread binding\n");
                hwloc_bitmap_free(set);
                hwloc_topology_destroy(Parallelton<ThreadPool>::topology);
            }

            // auto i = hwloc_bitmap_first(set);
            // auto obj = hwloc_get_pu_obj_by_os_index(Parallelton<ThreadPool>::topology, i);
            // std::cout << "Rank=" << Parallelton<ThreadPool>::rank << " logical index=" 
            //         << obj->logical_index << " (OS/physical index " << i << ")" << std::endl;
            hwloc_bitmap_free(set);
        };

        for(size_t id = 0; id < thread_number; id++)
        {
            auto logid = logindices[id + rank * thread_number];
            addtask(bind, id, logid);
        }

        thread0task();
        while(!notasksleft())
        {}
        return true;
    }

    bool ThreadPool::bindthreads(std::function<size_t (size_t, size_t, size_t)> lambda)
    {
        auto bind = [this](size_t logid)
        {
            // bind this thread to the correct core
            hwloc_bitmap_t set = hwloc_bitmap_alloc();
            if (!set) {
                fprintf(stderr, "failed to allocate a bitmap\n");
                hwloc_topology_destroy(Parallelton<ThreadPool>::topology);
            }

            auto logobj = hwloc_get_obj_by_type(Parallelton<ThreadPool>::topology, HWLOC_OBJ_PU, logid);
            hwloc_bitmap_only(set, logobj->os_index);

            auto err = hwloc_set_cpubind(Parallelton<ThreadPool>::topology, set, HWLOC_CPUBIND_THREAD);
            if (err < 0) {
                fprintf(stderr, "failed to set thread binding\n");
                hwloc_bitmap_free(set);
                hwloc_topology_destroy(Parallelton<ThreadPool>::topology);
            }

            hwloc_bitmap_free(set);
        };

        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        for(size_t id = 0; id < thread_number; id++)
        {
            auto logid = lambda(id, rank, thread_number);
            addtask(bind, id, logid);
        }

        thread0task();
        while(!notasksleft())
        {}
        return true;
    }

    bool ThreadPool::bindthread(size_t id)
    {
        // /* get the number of cores */
        // unsigned nbcores = hwloc_get_nbobjs_by_type(Parallelton<ThreadPool>::topology, HWLOC_OBJ_CORE);
        // // if(id == 0)
        // //     std::cout << "nbcores=" << nbcores << std::endl;
        // /* get the number of different L3 Caches */
        // unsigned nbl3 = hwloc_get_nbobjs_by_type(Parallelton<ThreadPool>::topology, HWLOC_OBJ_L3CACHE);
        // unsigned nbpackages = hwloc_get_nbobjs_by_type(Parallelton<ThreadPool>::topology, HWLOC_OBJ_PACKAGE);

        // auto coresperl3 = nbcores / nbl3;
        // auto coresperpackage = nbcores / nbpackages;
        
        // if(thread_number * Parallelton<ThreadPool>::size > nbcores)
        //     std::cout << "Error: too many threads started" << std::endl;

        // // if(id == 0)
        // //     std::cout << "nbl3=" << nbl3 << std::endl;
        // hwloc_obj_t package, core;
        // // get package according to thread count
        // // if(id == 0)
        // //     std::cout << "rank=" << Parallelton<ThreadPool>::rank << std::endl;
        
        // if(thread_number * Parallelton<ThreadPool>::rank < 64 && id < 64)
        //     package = hwloc_get_obj_by_type(Parallelton<ThreadPool>::topology, HWLOC_OBJ_PACKAGE, 0);
        // else
        //     package = hwloc_get_obj_by_type(Parallelton<ThreadPool>::topology, HWLOC_OBJ_PACKAGE, 1);

        // // get core with thread id and rank
        // auto coreid = id + Parallelton<ThreadPool>::rank * thread_number;
        // coreid = coreid % coresperpackage;

        // // if(id == 0)
        // //     std::cout << "coreid=" << coreid << std::endl;

        // core = hwloc_get_obj_inside_cpuset_by_type(Parallelton<ThreadPool>::topology, package->cpuset, HWLOC_OBJ_CORE, coreid);

        // // bind this thread to the selected core (singlify to make sure no migration of the thread takes place)
        // hwloc_cpuset_t set = hwloc_bitmap_dup(core->cpuset);
        // hwloc_bitmap_singlify(set);
        // hwloc_set_cpubind(Parallelton<ThreadPool>::topology, set, HWLOC_CPUBIND_THREAD);

        // // auto i = hwloc_bitmap_first(set);
        // // auto obj = hwloc_get_pu_obj_by_os_index(Parallelton<ThreadPool>::topology, i);

        // // std::cout << "Rank=" << Parallelton<ThreadPool>::rank << " Tid=" << id
        // //         << " is now running on PU logical index=" << obj->logical_index << " (OS/physical index " << i << ")" << std::endl;
        // // // std::cout << "Rank=" << Parallelton<ThreadPool>::rank << " logical index=" 
        // // //         << obj->logical_index << " (OS/physical index " << i << ")" << std::endl;

        // hwloc_bitmap_free(set);

        return false;



        // // bind this thread to the correct core
        // hwloc_bitmap_t set = hwloc_bitmap_alloc();
        // if (!set) {
        //     fprintf(stderr, "failed to allocate a bitmap\n");
        //     hwloc_topology_destroy(Parallelton<ThreadPool>::topology);
        //     return EXIT_FAILURE;
        // }
        
        // auto err = hwloc_set_cpubind(Parallelton<ThreadPool>::topology, set, HWLOC_CPUBIND_THREAD);
        // if (err < 0) {
        //     fprintf(stderr, "failed to set thread binding\n");
        //     hwloc_bitmap_free(set);
        //     hwloc_topology_destroy(Parallelton<ThreadPool>::topology);
        //     return EXIT_FAILURE;
        // }

        // auto i = hwloc_bitmap_first(set);
        // auto obj = hwloc_get_pu_obj_by_os_index(Parallelton<ThreadPool>::topology, i);

        // // std::cout << "Rank=" << Parallelton<ThreadPool>::rank << " Tid=" << id
        // //         << " is now running on PU logical index=" << obj->logical_index << " (OS/physical index " << i << ")" << std::endl;
        // // std::cout << "Rank=" << Parallelton<ThreadPool>::rank << " logical index=" 
        // //         << obj->logical_index << " (OS/physical index " << i << ")" << std::endl;

        // hwloc_bitmap_free(set);
        // return true;
    }
}
