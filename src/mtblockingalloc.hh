#ifndef DUNE_BLOCK_ALLOCATOR_HH
#define DUNE_BLOCK_ALLOCATOR_HH


#include <iostream>
#include <cstring>
#include <string.h>
#include <hwloc.h>
#include "parallelton.hh"

namespace Dune{
template<class T, class ThreadBackend = ThreadPool>
class BlockAllocator2D
{
public:
    typedef T value_type;
    typedef std::size_t size_type;
    typedef ptrdiff_t difference_type;

    typedef T* pointer;
    typedef const T* const_pointer;
    typedef T& reference;
    typedef const T& const_reference;

    BlockAllocator2D() = default;

    template<class U>
    BlockAllocator2D (const BlockAllocator2D<U>& other) noexcept {}

    BlockAllocator2D& operator=(const BlockAllocator2D&) = default;

    template<class U>
    bool operator==(const BlockAllocator2D<U>&) const noexcept {return true;}

    template<class U>
    bool operator!=(const BlockAllocator2D<U>&) const noexcept {return false;}


    T* allocate(size_type n) const
    {
        // T* t = new T[n];
        T* t = (T*) hwloc_alloc(Parallelton<ThreadPool>::topology, sizeof(T) * n);
        
        typedef Parallelton<ThreadBackend> ParalleltonTB;
        auto context = ParalleltonTB::context;

        int rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        auto ptx = ParalleltonTB::ptx;
        auto pty = ParalleltonTB::pty;

        std::array<int, 2> dims;
        dims.fill(0);
        MPI_Dims_create(context->getnbthreads(), 2, &dims[0]);

        auto pbx = ptx * dims[0];

        // elements per thread = how many elements will get allocated / over how many threads are they distributed
        auto ept = n / context->getnbthreads();
        // elements per row = how many elements per thread / over how many rows distributed
        auto epr = ept / pty;

        hwloc_bitmap_t set;

        char* s;
        int err;
        set = hwloc_bitmap_alloc();
        if (!set) {
            fprintf(stderr, "failed to allocate a bitmap\n");
            hwloc_topology_destroy(ParalleltonTB::topology);
        }

        for(size_t i = 0; i < context->getnbthreads(); i++)
        {
            auto logid = ParalleltonTB::lambda(i, rank, context->getnbthreads());
            auto logobj = hwloc_get_obj_by_type(ParalleltonTB::topology, HWLOC_OBJ_PU, logid);
            hwloc_bitmap_only(set, logobj->os_index);

            for(size_t j = 0; j < pty; j++)
            {
                err = hwloc_set_area_membind(ParalleltonTB::topology, t + (ParalleltonTB::threadstarts[i] + j*pbx), sizeof(T)*epr, set, HWLOC_MEMBIND_BIND,
                    HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
                if (err < 0)
                    std::cout << "Error: memory binding failed" << std::endl;

                // hwloc_bitmap_asprintf(&s, set);

                // std::cout << "Rank=" << ParalleltonTB::rank << " Tid=" << i
                //     << " on PU logical index=" << s << std::endl;
            }
        }



        // // first touching
        // auto touch = [t](size_t start, size_t stride, size_t id)
        // {
        //     for(auto i = start; i < start+stride; i++)
        //     {
        //         T tmp;
        //         t[i] = tmp;
        //     }
        // };

        // for(size_t i = 0; i < context->getnbthreads(); i++)
        // {
        //     auto current = i * ept;
        //     context->addtask(touch, i, current, ept, i);
        // }
        // context->waittasks();


        // // checking where memory is allocated to
        // for(size_t i = 0; i < context->getnbthreads(); i++)
        // {
        //     hwloc_membind_policy_t policy;
        //     hwloc_bitmap_zero(set);
        //     err = hwloc_get_area_membind(ParalleltonTB::topology, t+i*ept, sizeof(T)*ept, set, &policy, HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
        //     if(err < 0)
        //         std::cout << "Error: getting memory binding failed" << std::endl;

        //     // print out data
        //     hwloc_bitmap_asprintf(&s, set);
        //     std::cout << "T" << i << " actually on " << s << std::endl; 
        // }

        hwloc_bitmap_free(set);
        return t;      
    }

    void deallocate(T* p, size_type n) const
    {
        if(p)
        {
            // delete[] p;
            hwloc_free(Parallelton<ThreadPool>::topology, p, sizeof(T) * n);
        }
    }
};
}



#endif