#ifndef DUNE_PAR_CALCULATIONS
#define DUNE_PAR_CALCULATIONS

#include <dune/common/exceptions.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvercategory.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <thread>
#include "parallelton.hh"
#include "threadpool.hh"


namespace Dune {

    template <class Vector>
    class ParCalculationAdapter
    {
    public:
        explicit ParCalculationAdapter(Vector& x, Vector& b, const ParData& p)
            : _X_(stackobject_to_shared_ptr(x)), _B_(stackobject_to_shared_ptr(b)), pardata(stackobject_to_shared_ptr(p))
        {
            context = Parallelton<ThreadPool>::context;
            nbthreads = context->getnbthreads();
            pbx = pardata->getpbx();
            pby = pardata->getpby();
            pbz = pardata->getpbz();
            stride2D = (pbx * pby) / nbthreads;
            stride3D = (pbx * pby * pbz) / nbthreads;
            // fullstride = ((pbx+2) * (pby+2)) / nbthreads;
            fullstride = x.size() / nbthreads;

            std::array<int, 2> dims2D;
            dims2D.fill(0);
            MPI_Dims_create(nbthreads, 2, &dims2D[0]);

            threadblockx2D = dims2D[0];
            threadblocky2D = dims2D[1];

            ptx2D = pbx / dims2D[0];
            pty2D = pby / dims2D[1];

            std::array<int, 3> dims3D;
            dims3D.fill(0);
            MPI_Dims_create(nbthreads, 3, &dims3D[0]);

            threadblockx3D = dims3D[0];
            threadblocky3D = dims3D[1];
            threadblockz3D = dims3D[2];

            ptx3D = pbx / dims3D[0];
            pty3D = pby / dims3D[1];
            ptz3D = pbz / dims3D[2];
        }

        explicit ParCalculationAdapter(std::shared_ptr<Vector> X, std::shared_ptr<Vector> B, std::shared_ptr<const ParData> p)
            : _X_(X), _B_(B), pardata(p)
        {}

        ~ParCalculationAdapter()
        {
            // // print wait times of threads
            // for(auto i = 0; i < Parallelton<ThreadPool>::context->getnbthreads(); i++)
            // {
            //     std::cout << "ParCalcAdap Thread " << i << " waited " << waittimes[i] << "s\n";
            // }
        }

        void richardson(Vector& b_0, double omega);

        void richardsonalt2D(Vector& d, Vector& Ad, double omega);
        void subalt2D(Vector& b, Vector& b_0);

        void richardsonalt3D(Vector& d, Vector& Ad, double omega);
        void subalt3D(Vector& b, Vector& b_0);

        void richardsonalt2Dblocked(Vector& d, Vector& Ad, double omega);
        void subalt2Dblocked(Vector& b, Vector& b_0);

        void richardsonalt3Dblocked(Vector& d, Vector& Ad, double omega);
        void subalt3Dblocked(Vector& b, Vector& b_0);


        void richardsoninner2D(Vector& b_0, double omega);
        void richardsoninner3D(Vector& b_0, double omega);
        void richardsonouter2D(Vector& b_0, double omega);
        void richardsonouter3D(Vector& b_0, double omega);

        void setupborderindices2D();
        void setupborderindices3D();


        void richarddefectout2D(Vector&, Vector&, double);
        void richarddefectin2D(Vector&, Vector&, double);
        
        void richarddefectout3D(Vector&, Vector&, double);
        void richarddefectin3D(Vector&, Vector&, double);
        
    private:
        const std::shared_ptr<Vector> _X_;
        const std::shared_ptr<Vector> _B_;
        const std::shared_ptr<const ParData> pardata;
        std::vector<size_t> borderindices;
        // variables frequently used
        std::shared_ptr<ThreadPool> context;
        size_t nbthreads;
        size_t stride2D;
        size_t stride3D;
        size_t fullstride;
        size_t pbx;
        size_t pby;
        size_t pbz;
        size_t ptx2D;
        size_t pty2D;
        size_t ptx3D;
        size_t pty3D;
        size_t ptz3D;
        size_t threadblockx2D;
        size_t threadblocky2D;
        size_t threadblockx3D;
        size_t threadblocky3D;
        size_t threadblockz3D;
    };

    template <class Vector>
    void ParCalculationAdapter<Vector>::richarddefectout2D(Vector& d, Vector& Ad, double omega)
    {
        auto f = [this, &d, &Ad, omega](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                auto borderindex = borderindices[i];

                Ad[borderindex] = d[borderindex] - omega * Ad[borderindex];
                (*_X_)[borderindex] = (*_X_)[borderindex] + omega * Ad[borderindex];
            }
        };

        // start threads
        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbrows = borderindices.size();
        auto stride = nbrows / nbthreads;
        auto rest = nbrows % nbthreads;

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;
            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }

    template <class Vector>
    void ParCalculationAdapter<Vector>::richarddefectin2D(Vector& d, Vector& Ad, double omega)
    {
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();

        auto f = [this, &d, &Ad, omega, pbx, pby](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                auto innerindex = i + pbx+3 + pbx+3 + i/(pbx-2) * 4;

                Ad[innerindex] = d[innerindex] - omega * Ad[innerindex];
                (*_X_)[innerindex] = (*_X_)[innerindex] + omega * Ad[innerindex];
            }
        };

        // start threads
        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbrows = (pbx-2) * (pby-2);
        auto stride = nbrows / nbthreads;
        auto rest = nbrows % nbthreads;

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;
            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }



    template <class Vector>
    void ParCalculationAdapter<Vector>::richarddefectout3D(Vector& d, Vector& Ad, double omega)
    {
        auto f = [this, &d, &Ad, omega](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                auto borderindex = borderindices[i];

                Ad[borderindex] = d[borderindex] - omega * Ad[borderindex];
                (*_X_)[borderindex] = (*_X_)[borderindex] + omega * Ad[borderindex];
            }
        };

        // start threads
        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbrows = borderindices.size();
        auto stride = nbrows / nbthreads;
        auto rest = nbrows % nbthreads;

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;
            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }

    template <class Vector>
    void ParCalculationAdapter<Vector>::richarddefectin3D(Vector& d, Vector& Ad, double omega)
    {
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();
        auto pbz = pardata->getpbz();

        auto f = [this, &d, &Ad, omega, pbx, pby](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                auto innerindex = i + 2*(pbx+2)*(pby+2) + 2*(pbx+3) + 
                //              once we complete one row we skip the left and right border to get to the next row
                                    i/(pbx-2) * 4 +
                //              one we complete one layer we skip to the next (skip top layer, bottom layer)
                                    i/((pbx-2)*(pby-2)) * (4 * (pbx+2));

                Ad[innerindex] = d[innerindex] - omega * Ad[innerindex];
                (*_X_)[innerindex] = (*_X_)[innerindex] + omega * Ad[innerindex];
            }
        };

        // start threads
        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbinnerrows = (pbx-2) * (pby-2) * (pbz-2);
        auto stride = nbinnerrows / nbthreads;
        auto rest = nbinnerrows % nbthreads;

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;
            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }


    // x = x + omega * (b_0 - b);
    template <class Vector>
    void ParCalculationAdapter<Vector>::richardson(Vector& b_0, double omega)
    {
        auto f = [this, &b_0, omega](size_t start, size_t stride)
        {
            auto i = _X_->begin() + start;
            auto iend = i + stride;

            auto j = _B_->begin() + start;
            auto k = b_0.begin() + start;

            for(; i != iend; ++i, ++j, ++k)
            {
                *i = *i + omega * (*k - *j);
            }
        };

        // start threads
        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto stride = _X_->size() / nbthreads;
        auto rest = _X_->size() % nbthreads;

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;
            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }

    // x = x + omega * (b_0 - b);
    template <class Vector>
    void ParCalculationAdapter<Vector>::richardsonalt2D(Vector& d, Vector& Ad, double omega)
    {
        auto f = [this, &d, &Ad, omega](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                // // update d
                // Ad[i] = d[i] - omega * Ad[i];
                // // update x
                // (*_X_)[i] = (*_X_)[i] + omega * Ad[i];

                auto overlap_index = i + i/pbx*2 + (pbx+2)+1;

                // update d
                Ad[overlap_index] = d[overlap_index] - omega * Ad[overlap_index];
                // update x
                (*_X_)[overlap_index] = (*_X_)[overlap_index] + omega * Ad[overlap_index];
            }
        };

        // start threads
        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride2D;

            context->addtask(f, i, current, stride2D);
        }
        // wait for threads
        context->thread0task();
    }


    template <class Vector>
    void ParCalculationAdapter<Vector>::subalt2D(Vector& b, Vector& b_0)
    {
        auto f = [this, &b, &b_0](size_t start, size_t stride, size_t id)
        {
            // for(auto i = 0ul; i < stride; i++)
            for(auto i = start; i < start + stride; i++)
            {
                // auto new_index = Parallelton<ThreadPool>::threadstarts[id] + i%Parallelton<ThreadPool>::locpbx + i/Parallelton<ThreadPool>::locpbx * Parallelton<ThreadPool>::Nx; 
                // auto new_index = start + i%locpbx + i/locpbx * Nx; 

                auto overlap_index = i + i/pbx*2 + (pbx+2)+1;
                b[overlap_index] = b_0[overlap_index] - b[overlap_index];
            }
        };

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride2D;
 
            context->addtask(f, i, current, stride2D, i);
        }
        // wait for threads
        context->thread0task();
    }


    // x = x + omega * (b_0 - b);
    template <class Vector>
    void ParCalculationAdapter<Vector>::richardsonalt3D(Vector& d, Vector& Ad, double omega)
    {
        auto f = [this, &d, &Ad, omega](size_t start, size_t stride)
        {
            size_t frontoverlap = (pbx+2) * (pby+2);

            for(auto i = start; i < start + stride; i++)
            {
                // // update d
                // Ad[i] = d[i] - omega * Ad[i];
                // // update x
                // (*_X_)[i] = (*_X_)[i] + omega * Ad[i];

                auto overlap_index = i + i/pbx * 2
                                    // if we clear one slide we go one layer into back direction (pos z)
                                    + i/(pbx*pby) * (2*(pbx+2)) 
                                    // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                                    + frontoverlap+(pbx+2)+1;

                // update d
                Ad[overlap_index] = d[overlap_index] - omega * Ad[overlap_index];
                // update x
                (*_X_)[overlap_index] = (*_X_)[overlap_index] + omega * Ad[overlap_index];
            }
        };

        // start threads
        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride3D;

            context->addtask(f, i, current, stride3D);
        }
        // wait for threads
        context->thread0task();
    }


    template <class Vector>
    void ParCalculationAdapter<Vector>::subalt3D(Vector& b, Vector& b_0)
    {
        auto f = [this, &b, &b_0](size_t start, size_t stride, size_t id)
        {
            size_t frontoverlap = (pbx+2) * (pby+2);

            // for(auto i = 0ul; i < stride; i++)
            for(auto i = start; i < start + stride; i++)
            {
                // auto new_index = Parallelton<ThreadPool>::threadstarts[id] + i%Parallelton<ThreadPool>::locpbx + i/Parallelton<ThreadPool>::locpbx * Parallelton<ThreadPool>::Nx; 
                // auto new_index = start + i%locpbx + i/locpbx * Nx; 

                auto overlap_index = i + i/pbx * 2
                                    // if we clear one slide we go one layer into back direction (pos z)
                                    + i/(pbx*pby) * (2*(pbx+2)) 
                                    // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                                    + frontoverlap+(pbx+2)+1;

                b[overlap_index] = b_0[overlap_index] - b[overlap_index];
            }
        };

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride3D;
 
            context->addtask(f, i, current, stride3D, i);
        }
        // wait for threads
        context->thread0task();
    }



    // x = x + omega * (b_0 - b);
    template <class Vector>
    void ParCalculationAdapter<Vector>::richardsonalt2Dblocked(Vector& d, Vector& Ad, double omega)
    {
        auto f = [this, &d, &Ad, omega](auto id)
        {
            auto tx = id%threadblockx2D;
            auto ty = (id/threadblockx2D) % threadblocky2D;

            auto iindex = Parallelton<ThreadPool>::threadstarts[id]; // thread block start
            auto overlapx = 0ul;
            // overlap on the sides
            if (tx == 0 || tx == threadblockx2D-1)
                overlapx++;
            if (tx == 0 && tx == threadblockx2D-1)
                overlapx++;
                
            for (size_t k = 0; k < pty2D; k++)
            {                     
                for (size_t l = 0; l < ptx2D; l++)
                {            
                    // update d
                    Ad[iindex] = d[iindex] - omega * Ad[iindex];
                    // update x
                    (*_X_)[iindex] = (*_X_)[iindex] + omega * Ad[iindex];
                    iindex++;
                }
                // one row completed   
                iindex += overlapx;
            }
        };

        // start threads
        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * ptx2D*pty2D;

            context->addtask(f, i, i);
        }
        // wait for threads
        context->thread0task();
    }


    template <class Vector>
    void ParCalculationAdapter<Vector>::subalt2Dblocked(Vector& b, Vector& b_0)
    {
        auto f = [this, &b, &b_0](size_t start, size_t stride, size_t id)
        {
            // for(auto i = 0ul; i < stride; i++)
            for(auto i = start; i < start + stride; i++)
            {
                b[i] = b_0[i] - b[i];
            }
        };

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * fullstride;
 
            context->addtask(f, i, current, fullstride, i);
        }
        // wait for threads
        context->thread0task();
    }




    // x = x + omega * (b_0 - b);
    template <class Vector>
    void ParCalculationAdapter<Vector>::richardsonalt3Dblocked(Vector& d, Vector& Ad, double omega)
    {
        auto f = [this, &d, &Ad, omega](size_t id)
        {
            auto tx = id%threadblockx3D;
            auto ty = (id/threadblockx3D) % threadblocky3D;
            auto tz = (id/(threadblockx3D * threadblocky3D)) % threadblockz3D;

            auto iindex = Parallelton<ThreadPool>::threadstarts[id]; // thread block start
            auto overlapx = 0ul, overlapy = 0ul, overlapcorner = 0ul;
            // overlap on the sides
            if (tx == 0 || tx == threadblockx3D-1)
                overlapx++;
            if (tx == 0 && tx == threadblockx3D-1)
                overlapx++;
            // overlap on the top and bottom
            if (ty == 0 || ty == threadblocky3D-1)
                overlapy += ptx3D;
            if (ty == 0 && ty == threadblocky3D-1)
                overlapy += ptx3D;
            // corner overlap
            if (tx == 0 && ty == 0) // bottom left corner
                overlapcorner++;
            if (tx == 0 && ty == threadblocky3D-1) // bottom right corner
                overlapcorner++;
            if (tx == threadblockx3D-1 && ty == 0) // top left corner
                overlapcorner++;
            if (tx == threadblockx3D-1 && ty == threadblocky3D-1) // top right corner
                overlapcorner++;

            for (size_t k = 0; k < ptz3D; k++)
            {
                for (size_t l = 0; l < pty3D; l++)
                {                     
                    for (size_t m = 0; m < ptx3D; m++)
                    {            
                        // update d
                        Ad[iindex] = d[iindex] - omega * Ad[iindex];
                        // update x
                        (*_X_)[iindex] = (*_X_)[iindex] + omega * Ad[iindex];
                        iindex++;
                    }
                    // one row completed   
                    iindex += overlapx;
                }
                // one layer completed
                iindex += overlapy + overlapcorner;
            }
        };

        // start threads
        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * ptx3D*pty3D*ptz3D;

            context->addtask(f, i, i);
        }
        // wait for threads
        context->thread0task();
    }


    template <class Vector>
    void ParCalculationAdapter<Vector>::subalt3Dblocked(Vector& b, Vector& b_0)
    {
        auto f = [this, &b, &b_0](size_t start, size_t stride, size_t id)
        {
            // for(auto i = 0ul; i < stride; i++)
            for(auto i = start; i < start + stride; i++)
            {
                b[i] = b_0[i] - b[i];
            }
        };

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * fullstride;
 
            context->addtask(f, i, current, fullstride, i);
        }
        // wait for threads
        context->thread0task();
    }





    template <class Vector>
    void ParCalculationAdapter<Vector>::richardsoninner2D(Vector& b_0, double omega)
    {
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();

        auto f = [this, &b_0, omega, pbx](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                auto innerindex = i + pbx+3 + pbx+3 + i/(pbx-2) * 4;

                (*_X_)[innerindex] = (*_X_)[innerindex] + omega * (b_0[innerindex] - (*_B_)[innerindex]);
            }
        };

        // start threads
        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbrows = pardata->getN() * pardata->getN() / pardata->getsize();
        auto nbinnerrows = nbrows - 2*pbx - 2*pby + 4;
        auto stride = nbinnerrows / nbthreads;
        auto rest = nbinnerrows % nbthreads;

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;

            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }
    
    template <class Vector>
    void ParCalculationAdapter<Vector>::richardsoninner3D(Vector& b_0, double omega)
    {
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();
        auto pbz = pardata->getpbz();

        auto f = [this, &b_0, omega, pbx, pby](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                //                 we skip one layer twice and must also skip the bottom tow +1 twice
                auto innerindex = i + 2*((pbx+2)*(pby+2)) + 2*((pbx+2)+1) + 
                //              once we complete one row we skip the left and right border to get to the next row
                                    i/(pbx-2) * 4 +
                //              one we complete one layer we skip to the next (skip top layer, bottom layer)
                                    i/((pbx-2)*(pby-2)) * (3*(pbx+2) + 2 + pbx);
                                    
                (*_X_)[innerindex] = (*_X_)[innerindex] + omega * (b_0[innerindex] - (*_B_)[innerindex]);
            }
        };

        // start threads
        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbrows = pardata->getN() * pardata->getN() * pardata->getN() / pardata->getsize();
        auto nbinnerrows = nbrows - 2*pbx*pby - (2*pby*pbz - 4*pby) - (2*pbx*pbz - 4*pbx - 4*(pbz-2));
        auto stride = nbinnerrows / nbthreads;
        auto rest = nbinnerrows % nbthreads;

        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;

            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }


    // write result into B
    template <class Vector>    
    void ParCalculationAdapter<Vector>::richardsonouter2D(Vector& b_0, double omega)
    {
        auto f = [this, &b_0, omega](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                auto borderindex = borderindices[i];

                (*_B_)[borderindex] = (*_X_)[borderindex] + omega * (b_0[borderindex] - (*_B_)[borderindex]);
            }
        };
        
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();

        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbouterrows = 2*pbx + 2*pby - 4;
        auto stride = nbouterrows / nbthreads;
        auto rest = nbouterrows % nbthreads;

        // start threads
        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;

            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }


    template <class Vector>    
    void ParCalculationAdapter<Vector>::richardsonouter3D(Vector& b_0, double omega)
    {
        auto f = [this, &b_0, omega](size_t start, size_t stride)
        {
            for(auto i = start; i < start + stride; i++)
            {
                auto borderindex = borderindices[i];

                (*_B_)[borderindex] = (*_X_)[borderindex] + omega * (b_0[borderindex] - (*_B_)[borderindex]);
            }
        };

        
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();
        auto pbz = pardata->getpbz();

        auto context = Parallelton<ThreadPool>::context;
        auto nbthreads = context->getnbthreads();
        auto nbouterrows = 2*pbx*pby + (2*pby*pbz - 4*pby) + (2*pbx*pbz - 4*pbx - 4*(pbz-2));
        auto stride = nbouterrows / nbthreads;
        auto rest = nbouterrows % nbthreads;

        // start threads
        for(size_t i = 0; i < nbthreads; i++)
        {
            auto current = i * stride;
            if(i == nbthreads - 1)
                stride += rest;

            context->addtask(f, i, current, stride);
        }
        // wait for threads
        context->waittasks();
    }





    template <class Vector>
    void ParCalculationAdapter<Vector>::setupborderindices2D()
    {
        borderindices.clear();
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();
        auto rank = pardata->getrank();   
        auto nbrows = 2*pbx + 2*pby - 4;
        borderindices.reserve(nbrows);

        for(size_t i = 0; i < nbrows; i++)
        {
            // map i to borderindex
            bool lowrow = i / pbx;
            lowrow = 1 - lowrow;
            bool highrow = i / (pbx + 2*(pby-2));
            bool column = !lowrow && !highrow;

            auto lowrowcontrib = lowrow * (i + pbx+3); // take first row directly after overlap offset
            auto highrowcontrib = highrow * (i + pbx+5 + (pbx) * (pby-2)); // add all inner values to index
            auto columncontrib = column * (i + pbx+5 + ((i - (pbx-1))/2 * (pbx-2)) // if we are at the right border add the inner values of that row
                                            + ((i - pbx)/2 * 2)); // if we are at the left border add the overlap values

            auto borderindex = lowrowcontrib + highrowcontrib + columncontrib;
            borderindices.emplace_back(borderindex);
        }
    }

    template <class Vector>
    void ParCalculationAdapter<Vector>::setupborderindices3D()
    {
        borderindices.clear();
        auto N = pardata->getN();
        auto pbx = pardata->getpbx();
        auto pby = pardata->getpby();
        auto pbz = pardata->getpbz();
        auto rank = pardata->getrank();
        auto size = pardata->getsize();
        auto nbinnerrows = (pbx-2) * (pby-2) * (pbz-2);
        // size_t nbrows = 2*pbx*pby + (2*pby*pbz - 4*pby) + (2*pbx*pbz - 4*pbx - 4*(pbz-2));
        size_t nbrows = N*N*N/size - nbinnerrows;
        borderindices.reserve(nbrows);

        for(size_t i = 0; i < nbrows; i++)
        {
            auto overlaplayersize = (pbx+2)*(pby+2); // size of a layer + overlap
            auto overlaplayeroffset = pbx + 3; // offset size to skip bottom overlap
            auto overlaplayeroffsethigh = pbx+2 + pby-1 + pby; // offset size to skip to the top layer
            auto overlapperlayer = 2*(pbx+2) + 2*pby; // how much overlap is on each layer
            auto layersize = pbx*pby; // size of a layer without overlap
            auto inbtwlsize = 2*pbx + 2*(pby-2); // size of an inbetween layer, meaning only the borders count
            auto whichlayer = (i-layersize)/inbtwlsize; // which layer is the current, starting from the first that is not only overlap
            auto nbinnervals = (pbz-2) * (pbx-2) * (pby-2); // how many values are on the inside
            auto lastlayerid = layersize + (pbz-2) * inbtwlsize; // index of the first value in the last layer
            auto colnorm = i-layersize-(whichlayer*inbtwlsize);
            
            bool firstlayer = i / (layersize);
            firstlayer = 1-firstlayer;
            bool lastlayer = i/(layersize + (pbz-2) * (inbtwlsize));
            bool betweenlayer = !firstlayer && !lastlayer;
            bool lowerrow = (1-firstlayer)*(1-lastlayer) * ((i-layersize)%inbtwlsize) / pbx;
            lowerrow = 1-lowerrow;
            bool highrow = (1-firstlayer)*(1-lastlayer) * ((i-layersize)%inbtwlsize) / (pbx + 2*(pby-2));
            bool col = !lowerrow && !highrow;  

            auto firstlayercontrib = firstlayer * (i + i/pbx*2 + overlaplayersize + overlaplayeroffset); // first layer
            auto lastlayercontrib = lastlayer * (i + (i-lastlayerid)/pbx*2 + nbinnervals + overlaplayersize + (pbz-1)*overlapperlayer + overlaplayeroffset); // lastlayer 
            auto lowestrowcontrib = betweenlayer * lowerrow * (i + whichlayer*(pbx-2)*(pby-2) + overlaplayersize + (whichlayer+1)*overlapperlayer + overlaplayeroffset); // lowest row of inbetween layer
            auto highestrowcontrib = betweenlayer * highrow * (i + (whichlayer+1)*(pbx-2)*(pby-2) + overlaplayersize + (whichlayer+1)*overlapperlayer + overlaplayeroffsethigh);
            auto columncontrib = betweenlayer * col * (i + (colnorm - (pbx-1))/2 * (pbx-2) + (colnorm - (pbx-2))/2 * 2 + whichlayer*(pbx-2)*(pby-2) + overlaplayersize + (whichlayer+1)*overlapperlayer + overlaplayeroffset);
            
            
            auto borderindex = firstlayercontrib + lastlayercontrib + lowestrowcontrib + highestrowcontrib + columncontrib; 

            // if (rank == 0) std::cout << "i=" << i << " bid=" << borderindex << " firstl=" << firstlayer
            // << " betwl=" << betweenlayer << " lastl=" << lastlayer << " which=" << whichlayer << "\n" <<
            // "1:" << firstlayercontrib << " 2:" << lastlayercontrib
            // << " 3:" << lowestrowcontrib << " 4:" << highestrowcontrib << " 5:" << columncontrib << std::endl;
            borderindices.emplace_back(borderindex);
        }
    }
}






#endif