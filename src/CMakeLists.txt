set(CMAKE_CXX_FLAGS "-lhwloc")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lhwloc -fsanitize=thread")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lhwloc -pg")
# set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pg")
# set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pg")
# set(--MPICH_ASYNC_PROGRESS=1 --ASAN_OPTIONS=new_delete_type_mismatch=0)

# add_compile_options(-O3 -DNDEBUG -ffast-math)
add_compile_options(-O3 -DNDEBUG)

add_executable("dune-mike" dune-mike.cc 
                            threadpool.cc)

target_link_dune_default_libraries("dune-mike")