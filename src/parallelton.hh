#ifndef DUNE_PARALLELTON_HH
#define DUNE_PARALLELTON_HH


#include <hwloc.h>
#include <memory>
#include <vector>
#include "threadpool.hh"

namespace Dune{
    class ThreadPool;
    class ThreadPoolOld;
};


// basically a singleton wrapper around the threadpool, so that it is accessible from everywhere within the rank
template<class ThreadBackend = Dune::ThreadPool>
class Parallelton
{
public:
    // the threadpool
    inline static std::shared_ptr<ThreadBackend> context;
    // lambda for cpu binding
    inline static std::function<size_t (size_t, size_t, size_t)> lambda;

    Parallelton(Parallelton const&) = delete;
    void operator=(Parallelton const&) = delete;

    // // data for blocking MT approach
    inline static std::vector<size_t> threadstarts;
    // inline static size_t ptx, pty, ptz;

    // // data
    inline static size_t N = 0;
    // inline static size_t rank = 0;
    // inline static size_t size = 0;
    // // number of blocks in x and y direction
    // inline static size_t blockx = 0;
    // inline static size_t blocky = 0;
    // inline static size_t blockz = 0;
    // // number of elements in a block in x and y direction
    // inline static size_t pbx = 0;
    // inline static size_t pby = 0;
    // inline static size_t pbz = 0;
    // // where is the first value of our rank located
    // inline static size_t start = 0;
    // hwloc data
    inline static hwloc_topology_t topology;

    // // dirty variables needed for first touch in the allocator
    // inline static size_t Nx, locpbx;
    // inline static std::vector<size_t> threadstarts;

private:
    Parallelton() = default;
};

// typedef Parallelton<ThreadBackend> ParalleltonTP;


#endif