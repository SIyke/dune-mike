#ifndef DUNE_THREAD_POOL_ID_HH
#define DUNE_THREAD_POOL_ID_HH


#include "config.h"
#include <mpi.h>
#include <vector>
#include <thread>
#include <atomic>
#include <functional>
#include <mutex>
#include <queue>
#include <chrono>
#include <cstdlib>
#include <memory>
#include <iostream>
#include <dune/common/timer.hh>
#include "barrier.hh"
#include "parallelton.hh"
#include <bitset>
#include <cmath>
#include <utility>

namespace Dune
{
    enum class Direction {right, left, back, front, up, down};

    Direction reverse(Direction d);

    struct padded_aint
    {
    public:
        padded_aint() = default;

        // padded_aint(size_t i)
        //   : task_counter(i)
        // {
        //     // for (auto j = 0; j < 64 - (sizeof(std::atomic<size_t>) + sizeof(short int[6]) + sizeof(std::mutex)); j++)
        //     //     padding[j] = 0;
        // }

        // void operator=(int i) { task_counter = i; }
        // void operator+=(int i) { task_counter += i; }
        // void operator-=(int i) { task_counter -= i; }
        // int get() const { return task_counter; }
        // void set(int i) { task_counter = i; }

        void operator=(size_t i) { task_counter.store(i, std::memory_order_relaxed);}
        void operator+=(size_t i) { task_counter.fetch_add(i, std::memory_order_relaxed);}
        void operator-=(size_t i) { task_counter.fetch_sub(i, std::memory_order_relaxed);}
        size_t get() const { return task_counter.load(std::memory_order_relaxed);}
        size_t acquire() const { return task_counter.load(std::memory_order_acquire);}
        void set(size_t i) { task_counter.store(i, std::memory_order_relaxed);}

        // void operator=(size_t i) { task_counter.store(i, std::memory_order_release); }
        // void operator+=(size_t i) { task_counter.fetch_add(i, std::memory_order_acq_rel); }
        // void operator-=(size_t i) { task_counter.fetch_sub(i, std::memory_order_acq_rel); }
        // size_t get() const { return task_counter.load(std::memory_order_acquire); }
        // size_t acquire() const { return task_counter.load(std::memory_order_acquire); }
        // void set(size_t i) { task_counter.store(i, std::memory_order_release); }

        // // atomic neighbormanagement
        // void set_neighbor(int n) { bitset.fetch_or(1 << n, std::memory_order_relaxed); }
        // bool get_neighbor(int n) const { return bitset.load(std::memory_order_relaxed) & (1 << n); }
        // // void clear_neighbors() { bitset.fetch_and(0, std::memory_order_acq_rel); }
        // void clear_neighbors() { bitset.store(0, std::memory_order_relaxed); }
        // bool check_neighbors(int& expected, int desired) { return bitset.compare_exchange_weak(expected, desired, std::memory_order_relaxed); }
        // void print_bitset() const { std::cout << std::bitset<8>(bitset) << std::endl; }

        // lock neighbormanagement
        void increase_neighbor(Direction d) 
        { 
            std::lock_guard<std::mutex> lock(mtx); 
            intset[static_cast<int>(d)]++;
        }
        void clear_neighbors() 
        {
            std::lock_guard<std::mutex> lock(mtx);

            for (auto& i : intset)
                i = 0;
        }
        bool neighbors_ready_and_clear(int nbneighbors)
        { 
            std::lock_guard<std::mutex> lock(mtx);
            // ready check
            int counter = 0;
            for (auto const i : intset)
                if (i > 0)
                    counter++;
            // clear
            if (counter == nbneighbors)
            {
                for (auto& i : intset)
                    if (i != 0)
                        i--;
                return true;
            }
            return false;
        }
        void intset_constructor()
        {
            std::lock_guard<std::mutex> lock(mtx);
            for (auto& i : intset)
                i = 0;
        }

    private:
        std::atomic<size_t> task_counter;
        // bitset for all neighbors (0:r 1:l)
        // std::atomic<int> bitset;
        // neighbors representing ints (0:r 1:l)
        short int intset[6];
        std::mutex mtx;
        char padding[0];
        // char padding[64 - (sizeof(std::atomic<size_t>) + sizeof(short int[6]) + sizeof(std::mutex))];
    };

    class ThreadPool
    {
    public:
        ThreadPool(size_t n);
        ~ThreadPool();

        template<typename T>
        void addtask(const T& task, size_t id)
        {
            {
                std::lock_guard<std::mutex> lock(queue_locks[id]);
                thread_based_tasks[id].emplace(std::function<void()>(task));
            }
            // task_number.fetch_add(1, std::memory_order_acq_rel);
            tasks_available[id] += 1;
        }

        template <typename T, typename... Ts>
        void addtask(const T& task, size_t id, Ts&... args)
        {
            addtask([task, args...]
                    { task(args...); }, id);
        }

        template <int Dim>
        void setupneighbors(int N, bool rowwise)
        {
            size_t nbelements = std::pow(N, Dim);
            size_t elementsperthread = nbelements / thread_number;
            size_t elementsperlayer = N*N;

            if (rowwise)
            {
                // in the row-wise scheme we can always be certain that the right and above neighbor are the same
                neighbors.emplace_back(std::pair<Direction, int>(Direction::left, 1));
                // same with the below and left neighbor
                neighbors.emplace_back(std::pair<Direction, int>(Direction::right, -1));
                // we are done with the 2D case
                if (Dim == 3)
                {
                    // we need to check the offset to the back and front neighbors
                    auto aux = elementsperlayer / elementsperthread;
                    auto offset = std::max(1ul, aux);

                    if (offset != 1)
                    {
                        neighbors.emplace_back(std::pair<Direction, int>(Direction::front, offset));
                        neighbors.emplace_back(std::pair<Direction, int>(Direction::back, -offset));
                    }
                }
            }
            else
            {
                // block wise setup
                std::array<int, Dim> dims;
                dims.fill(0);
                MPI_Dims_create(thread_number, Dim, &dims[0]);
                // first right and left neighbors
                neighbors.emplace_back(std::pair<Direction, int>(Direction::right, -1));
                neighbors.emplace_back(std::pair<Direction, int>(Direction::left, 1));

                if (Dim == 3)
                {
                    // front and back
                    neighbors.emplace_back(std::pair<Direction, int>(Direction::back, -dims[0]*dims[1]));
                    neighbors.emplace_back(std::pair<Direction, int>(Direction::front, dims[0]*dims[1]));
                }
                // last up and down neighbors
                neighbors.emplace_back(std::pair<Direction, int>(Direction::up, -dims[0]));
                neighbors.emplace_back(std::pair<Direction, int>(Direction::down, dims[0]));
            }
        }

        bool notasksleft();
        void waittasks();
        void thread0task();
        // bind cpus with lambda that generates indizes
        bool bindthreads(std::function<size_t (size_t, size_t, size_t)>);
        // bind cpus with a vector of indices
        bool bindthreads(std::vector<size_t>&);

        auto getnbthreads() { return thread_number; }
        // start progress thread
        void startprogressthread(MPI_Request& request);
        void endprogressthread();

        std::vector<std::chrono::high_resolution_clock::time_point> taskcompleted;
    private:

        void createthreads();
        void destroythreads();
        void workloop(size_t id);
        bool poptask(std::function<void()>& task, size_t id);
        void sleepyield();
        bool bindthread(size_t logindex);

        size_t thread_number;
        bool thread0localsnyc = false;
        std::vector<std::pair<Direction, int>> neighbors;

        std::vector<std::thread> threads;
        std::atomic<bool> running = true;
        
        std::vector<std::queue<std::function<void()>>> thread_based_tasks;

        padded_aint* tasks_available;

        std::atomic<size_t> task_number = 0;
        size_t sleepduration = 1000;

        Barrier barrier;
        std::vector<std::mutex> queue_locks;
        std::mutex mtx;

        std::thread progressthread;

        double worktime = 0.0;
        std::chrono::time_point<std::chrono::high_resolution_clock> workstart;
        std::chrono::time_point<std::chrono::high_resolution_clock> workend;

    };

    typedef std::shared_ptr<ThreadPool> ThreadPoolContext;
}


#endif