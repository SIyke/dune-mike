#ifndef DUNE_PAR_COMMUNICATORS_HH
#define DUNE_PAR_COMMUNICATORS_HH

#include "parcommunicator.hh"

namespace Dune{


    template <class Vector, int Dim>
    class ParCommunicator5Star : public ParCommunicator<Vector, Dim>
    {
    public:
        ParCommunicator5Star(size_t N, ParData& pardata);
        ~ParCommunicator5Star() = default;

        void setupsendbuffer (Vector& x) override;
        void setuprecvbuffer (Vector& x) override;
    protected:
        typedef ParCommunicator<Vector,Dim> Base;

        using Base::GRID_COMM;
        using Base::request;

        using Base::size;
        using Base::sendbuf; using Base::recvbuf;
        using Base::root; using Base::rank;
        using Base::pbx; using Base::pby; using Base::pbz; using Base::start; using Base::N;
        using Base::blockx; using Base::blocky; using Base::blockz;
        // index of where the first element for a direction is located
        using Base::startcore;
        using Base::counts_send;
        using Base::displ_send;
        using Base::counts_recv;
        using Base::displ_recv;
        
        // size of communication in every direction
        size_t commx, commy, commz;
        // presence of neighbor
        bool neighbxp = false, neighbxn = false, neighbyp = false, 
            neighbyn = false, neighbzp = false, neighbzn = false; 
        size_t startx, starty, startz;
        std::array<int, Dim> coords;

        double setupsendwatch = 0; 
        double setuprecvwatch = 0;
    };


    template <class Vector, int Dim>
    ParCommunicator5Star<Vector,Dim>::ParCommunicator5Star(size_t N, ParData& pardata)
        : ParCommunicator<Vector, Dim>::ParCommunicator(N)
    {
        std::array<int, Dim> dims;
        dims.fill(0);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Dims_create(size, Dim, &dims[0]);
        std::array<int, Dim> periods;
        periods.fill(false);
        int reorder = false;
        MPI_Cart_create(MPI_COMM_WORLD, Dim, &dims[0], &periods[0], reorder, &GRID_COMM);

        MPI_Comm_rank(GRID_COMM, &rank);
        MPI_Comm_size(GRID_COMM, &size);

        // number of blocks in x,y and z direction
        blockx = dims[0];
        blocky = dims[1];
        blockz = 0;
        // if(rank == 0) std::cout << "size=" << size << "\n";
        // if(rank == 0) std::cout << "blockx=" << blockx << "\n" << "blocky=" << blocky << std::endl;
        
        if constexpr (Dim == 3)
        {    
            blockz = dims[2];
            // if(rank == 0) std::cout << "blockz=" << blockz  << "\n";
        }
        // number of elements in a block in x and y direction
        pbx = N / blockx;
        pby = N / blocky;
        pbz = 0;

        // if(rank == 0) std::cout << "pbx=" << pbx  << "\n" << "pby=" << pby  << "\n";

        if constexpr (Dim == 3)
        {    
            pbz = N / blockz;
            // if(rank == 0) std::cout << "pbz=" << pbz  << "\n";
        }

        MPI_Cart_coords(GRID_COMM, rank, Dim, &coords[0]);
        auto xcoord = coords[0];
        auto ycoord = coords[1];
        auto zcoord = 0;
        if constexpr (Dim == 3) 
            zcoord = coords[2];

        // start "points" to the first row in the matrix where the local rank would start
        // offset in x direction (x)
        start = xcoord * pbx;
        // offset in y direction (y)
        start += ycoord * N * pby;
        // offset in z direction
        start += zcoord * pbz * (N*N);

        pardata.set(rank, size, pbx, pby, pbz, blockx, blocky, blockz, start, N, xcoord, ycoord, zcoord);

        // std::cout << "rank=" << rank << " located at x=" << coords[0] << ", y=" << coords[1] << ", z=" << coords[2] << " starts at " << start << std::endl;
        // if(rank == 0) std::cout  << "\n";

        // how many variables are communicated in each direction
        // 2D
        commx = pby;
        commy = pbx;
        commz = 0;
        // 3D
        if constexpr (Dim == 3)
        {
            commx = pby*pbz;
            commy = pbx*pbz;
            commz = pbx*pby;
        }

        // check if we even have a neighbor in a given direction (if no, we dont have to prepare the sendbuffer for this direction)
        
        // check for neighbors in x direction
        if(coords[0] > 0)
            neighbxn = true;
        if(coords[0] < blockx-1)
            neighbxp = true;

        // check for neighbors in y direction
        if(coords[1] > 0)
            neighbyn = true;
        if(coords[1] < blocky-1)
            neighbyp = true;

        if constexpr (Dim == 3)
        {
            // check for neighbors in z direction
            if(coords[2] > 0)
                neighbzn = true;
            if(coords[2] < blockz-1)
                neighbzp = true;
        }


        // where to start looking for the values that need to be communicated (considering local overlap)
        size_t zoverlap = Dim/3 * (pbx+2) * (pby+2);
        startx = (pbx+2) + zoverlap;
        starty = 1 + zoverlap;
        startz = pbx+2 + 1;
        startcore = (pbx+2) + 1 + zoverlap;

        // init variables
        counts_send.resize(2*Dim);
        counts_recv.resize(2*Dim);
        displ_send.resize(2*Dim, 0);
        displ_recv.resize(2*Dim, 0);
        // 2D
        // how many variables are sent and received in every direction
        // keep in mind the communication directions are in this order: x -> y -> z
        counts_send[0] = commx, counts_send[1] = commx, counts_send[2] = commy, counts_send[3] = commy;
        counts_recv[0] = commx, counts_recv[1] = commx, counts_recv[2] = commy, counts_recv[3] = commy;

        // 3D
        if constexpr (Dim == 3)
        {
            // how many variables are sent and received in every direction
            counts_send[4] = commz, counts_send[5] = commz;
            counts_recv[4] = commz, counts_recv[5] = commz;
        }

        // calc displacements for send and receive
        for(auto i = 1; i < 2*Dim; i++)
            displ_send[i] = displ_send[i - 1] + counts_send[i - 1];

        for(auto i = 1; i < 2*Dim; i++)
            displ_recv[i] = displ_recv[i - 1] + counts_recv[i - 1];

        // in these buffers the data that will be sent and
        // received with MPI_Neighbor_Alltoallv will be saved
        sendbuf.resize(2*commx + 2*commy + 2*commz);
        recvbuf.resize(2*commx + 2*commy + 2*commz);
    }

    // template <class Vector, int Dim>
    // ParCommunicator5Star<Vector,Dim>::~ParCommunicator5Star()
    // {
    //     // std::cout << "Rank=" << rank << " Time for setupsendbuffer=" << setupsendwatch << std::endl;
    //     // std::cout << "Rank=" << rank << " Time for setuprcvbuffer=" << setuprecvwatch << std::endl;
    // }


    template<class Vector, int Dim>
    void ParCommunicator5Star<Vector,Dim>::setupsendbuffer(Vector& x)
    {
        // the order in the sendbuffer is according to:
        // first send left, then right, then down, then up, then (if existing) front and then back
        // auto start = std::chrono::high_resolution_clock::now();
        
        auto iter = sendbuf.begin();

        if(neighbxn)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                // iterating the left col/level and then skipping to next layer
                auto nextlayer = i/pby * (3*(pbx+2) -(pbx+2));
                *iter = x[startcore + i*(pbx+2) + nextlayer];
                // std::cout << "Send left: " << startcore + i*(pbx+2) + nextlayer << std::endl;
            }
        else
            iter += commx;

        if(neighbxp)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                // iterating the right col/level and then skipping to next layer
                auto nextlayer = i/pby * (3*(pbx+2) -(pbx+2));
                *iter = x[startcore+pbx-1 + i*(pbx+2) + nextlayer];
                // std::cout << "Send right: " << startcore+pbx-1 + i*(pbx+2) + nextlayer << std::endl;
            }
        else
            iter += commx;

        if(neighbyn)
            // here we send down and up
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                // iterating the lower col/level and then skipping to next layer
                // nextlayer = in which layer are we * distance to skip between layers
                auto nextlayer = i/pbx * ((pbx+2)*(pby+1) + 2);
                *iter = x[startcore + i + nextlayer];
                // std::cout << "Send down: " << startcore + i + nextlayer << std::endl;
            }
        else
            iter += commy;
        
        if(neighbyp)
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                // iterating the upper col/level and then skipping to next layer
                auto nextlayer = i/pbx * ((pbx+2)*(pby+1) + 2);
                *iter = x[startcore+(pbx+2)*(pby-1) + i + nextlayer];
                // std::cout << "Send up: " << startcore+(pbx+2)*(pby-1) + i + nextlayer << std::endl;
            }
        else
            iter += commy;

        if(neighbzn)
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                // iterating the front level and then skipping to next layer
                *iter = x[startcore + i + i/pbx*2];
                // std::cout << "Send front: " << startcore + i + i/pbx*2 << std::endl;
            }
        else
            iter += commz;

        if(neighbzp)
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                // iterating the back level and then skipping to next layer
                *iter = x[startcore + (pbx+2)*(pby+2)*(pbz-1) + i + i/pbx*2];
                // std::cout << "Send back: " << startcore + (pbx+2)*(pby+2)*(pbz-1) + i + i/pbx*2 << std::endl;
            }
        else
            iter += commz;

        // if(rank == 0)
        // {
        //     std::cout << "Sendbuf: ";
        //     for(auto i : sendbuf)
        //         std::cout << i << " ";
        //     std::cout << std::endl;
        // }
        // auto end = std::chrono::high_resolution_clock::now();
        // setupsendwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
    }

    template<class Vector, int Dim>
    void ParCommunicator5Star<Vector,Dim>::setuprecvbuffer(Vector& x)
    {
        // auto start = std::chrono::high_resolution_clock::now();

        auto iter = recvbuf.begin();
    
        if(neighbxn)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                // iterating the left col/level and then skipping to next layer
                auto nextlayer = i/pby * (3*(pbx+2) -(pbx+2));
                x[startx + i*(pbx+2) + nextlayer] = *iter;
                // std::cout << "Recv left: " << startx + i*(pbx+2) + nextlayer << std::endl;
            }
        else
            iter += commx;

        if(neighbxp)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                // iterating the right col/level and then skipping to next layer
                auto nextlayer = i/(pby) * (3*(pbx+2) -(pbx+2));
                x[startx+pbx+1 + i*(pbx+2) + nextlayer] = *iter;
                // std::cout << "Recv right: " << startx+pbx+1 + i*(pbx+2) + nextlayer << std::endl;
            }
        else
            iter += commx;

        if(neighbyn)
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                // iterating the lower col/level and then skipping to next layer
                auto nextlayer = i/pbx * ((pbx+2)*(pby+1) + 2); 
                x[starty + i + nextlayer] = *iter;
                // std::cout << "Recv down: " << starty + i + nextlayer << std::endl;
            }
        else
            iter += commy;

        if(neighbyp)
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                // iterating the upper col/level and then skipping to next layer
                auto nextlayer = i/pbx * ((pbx+2)*(pby+1) + 2);
                x[starty+(pbx+2)*(pby+1) + i + nextlayer] = *iter;
                // std::cout << "Recv up: " << starty+(pbx+2)*(pby+1) + i + nextlayer << std::endl;
            }
        else
            iter += commy;

        if(neighbzn)
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                // iterating the front level
                x[startz + i + i/pbx * 2] = *iter;
                // std::cout << "Recv front: " << startz + i << std::endl;
            }
        else
            iter += commz;

        if(neighbzp)
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                // iterating the back level
                x[startz + (pbx+2)*(pby+2)*(pbz+1) + i + i/pbx * 2] = *iter;
                // std::cout << "Recv back: " << startz + 2*commx+2*commy+commz + pbx*pby*pbz + i << std::endl;
            }
        else
            iter += commz;


        // if(rank == 0)
        // {
        //     std::cout << "Recvbuf: ";
        //     for(auto i : recvbuf)
        //         std::cout << i << " ";
        //     std::cout << std::endl;
        // }
        // auto end = std::chrono::high_resolution_clock::now();
        // setuprecvwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
    }







    template<class Vector, int Dim>
    class ParCommunicator9Star : public ParCommunicator<Vector,Dim>
    {
    public:

        ParCommunicator9Star(size_t N, ParData& pardata);

        void setupsendbuffer(Vector& b) override;
        void setuprecvbuffer(Vector& x) override;

    private:
        typedef ParCommunicator<Vector,Dim> Base;

        using Base::GRID_COMM;
        using Base::request;

        using Base::size;
        using Base::sendbuf; using Base::recvbuf;
        using Base::root; using Base::rank;
        using Base::pbx; using Base::pby; using Base::pbz; using Base::start; using Base::N;
        // index of where the first element for a direction is located
        using Base::startcore;
        using Base::counts_send;
        using Base::displ_send;
        using Base::counts_recv;
        using Base::displ_recv;
        
        size_t blockx, blocky, blockz;
        // size of communication in every direction
        size_t commx, commy, commz, commv, commw;
        size_t startx, starty, startz, startv, startw;
        std::array<int, Dim> coords;
        std::vector<size_t> commdirections;
    };


    template<class Vector, int Dim>
    ParCommunicator9Star<Vector,Dim>::ParCommunicator9Star(size_t N, ParData& pardata)
        : ParCommunicator<Vector,Dim>::ParCommunicator(N)
    {
        std::array<int, Dim> dims;
        dims.fill(0);
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Dims_create(size, Dim, &dims[0]);

        std::vector<int> neighbors;

        auto x = rank % dims[0];
        auto y = rank / dims[0];
        auto z = rank / (dims[0]*dims[1]);

        int upperx = dims[0]-1;
        int uppery = dims[1]-1;
        int upperz = dims[2]-1;


        // directions from front down left to back up right
        if constexpr (Dim == 3)
        {
            // one rank front and down and left
            if(z > 0 && x > 0 && y > 0)
                neighbors.emplace_back(rank - 1 - dims[0]*dims[1] - dims[0]);
            // one rank front and down
            if(z > 0 && y > 0)
                neighbors.emplace_back(rank - dims[0]*dims[1] - dims[0]);
            // one rank front and down and right
            if(z > 0 && x < upperx && y > 0)
                neighbors.emplace_back(rank + 1 - dims[0]*dims[1] - dims[0]);
            // one rank front and left
            if(z > 0 && x > 0)
                neighbors.emplace_back(rank - 1 - dims[0]*dims[1]);
            // one rank front
            if(z > 0)
                neighbors.emplace_back(rank-dims[0]*dims[1]);
            // one rank front and right
            if(z > 0 && x < upperx)
                neighbors.emplace_back(rank + 1 - dims[0]*dims[1]);
            // one rank front and up and left
            if(z > 0 && y < uppery && x > 0)
                neighbors.emplace_back(rank - 1 + dims[0] - dims[0]*dims[1]);
            // one rank front and up
            if(z > 0 && y < uppery)
                neighbors.emplace_back(rank - dims[0]*dims[1] + dims[0]);
            // one rank front and up and right
            if(z > 0 && y < uppery && x < upperx)
                neighbors.emplace_back(rank + 1 + dims[0] - dims[0]*dims[1]);

        }

        // one rank down and left
        if(x > 0 && y > 0)
            neighbors.emplace_back(rank-1-dims[0]);
        // one rank down
        if(y > 0)
            neighbors.emplace_back(rank-dims[0]);
        // one rank down and right
        if(x < upperx && y > 0)
            neighbors.emplace_back(rank+1-dims[0]);
        // one rank left
        if(x > 0)
            neighbors.emplace_back(rank-1);
        // center
        // one rank right
        if(x < upperx)
            neighbors.emplace_back(rank+1);
        // one rank up and left
        if(x > 0 && y < uppery)
            neighbors.emplace_back(rank-1+dims[0]);
        // one rank up
        if(y < uppery)
            neighbors.emplace_back(rank+dims[0]);
        // one rank up and left
        if(x < upperx && y < uppery)
            neighbors.emplace_back(rank+1+dims[0]);

        if constexpr (Dim == 3)
        {   

            // one rank back and down and left
            if(z < upperz && y > 0 && x > 0)
                neighbors.emplace_back(rank - 1 - dims[0] + dims[0]*dims[1]);
            // one rank back and down
            if(z < upperz && y > 0)
                neighbors.emplace_back(rank + dims[0]*dims[1] - dims[0]);
            // one rank back and down and right
            if(z < upperz && x < upperx && y > 0)
                neighbors.emplace_back(rank + 1 + dims[0]*dims[1] - dims[0]);
            // one rank back and left
            if(z < upperz && x > 0)
                neighbors.emplace_back(rank - 1 + dims[0]*dims[1]);
            // one rank back
            if(z < upperz)
                neighbors.emplace_back(rank + dims[0]*dims[1]);
            // one rank back and right
            if(z < upperz && x < upperx)
                neighbors.emplace_back(rank + 1 + dims[0]*dims[1]);
            // one rank back and up and left
            if(z < upperz && y < uppery && x > 0)
                neighbors.emplace_back(rank - 1 + dims[0] + dims[0]*dims[1]);
            // one rank back and up
            if(z < upperz && y < uppery)
                neighbors.emplace_back(rank + dims[0]*dims[1] + dims[0]);
            // one rank back and up and right
            if(z < upperz && y < uppery && x < upperx)
                neighbors.emplace_back(rank + 1 + dims[0] + dims[0]*dims[1]);
        }

        int reorder = false;
        // std::vector<int> weights(neighbors.size(), 1);
        // MPI_Graph_create(MPI_COMM_WORLD, nbnodes, &index[0], &edges[0], reorder, &GRID_COMM);
        MPI_Dist_graph_create_adjacent(MPI_COMM_WORLD, neighbors.size(), &neighbors[0], MPI_UNWEIGHTED, 
                                                        neighbors.size(), &neighbors[0], MPI_UNWEIGHTED, MPI_INFO_NULL, reorder, &GRID_COMM);


        // MPI_Comm_rank(GRID_COMM, &rank);
        // MPI_Comm_size(GRID_COMM, &size);

        // number of blocks in x,y and z direction
        blockx = dims[0];
        blocky = dims[1];
        blockz = 0;
        // if(rank == 0) std::cout << "size=" << size << "\n";
        // if(rank == 0) std::cout << "blockx=" << blockx << "\n" << "blocky=" << blocky << std::endl;
        
        if constexpr (Dim == 3)
        {    
            blockz = dims[2];
            // if(rank == 0) std::cout << "blockz=" << blockz  << "\n";
        }
        // number of elements in a block in x and y direction
        pbx = N / blockx;
        pby = N / blocky;
        pbz = 0;
    
        // if(rank == 0) std::cout << "pbx=" << pbx  << "\n" << "pby=" << pby  << "\n";

        if constexpr (Dim == 3)
        {    
            pbz = N / blockz;
            // if(rank == 0) std::cout << "pbz=" << pbz  << "\n";
        }

        MPI_Cart_coords(GRID_COMM, rank, Dim, &coords[0]);
        // start "points" to the first row in the matrix where the local rank would start
        // offset in z direction
        start = coords[2] * pbz * (N*N);
        // offset in y direction (y)
        start += coords[1] * N * pby;
        // offset in x direction (x)
        start += coords[0] * pbx;

        pardata.set(rank, size, pbx, pby, pbz, blockx, blocky, blockz, start, N);

        // std::cout << "rank=" << rank << " located at x=" << coords[0] << ", y=" << coords[1] << ", z=" << coords[2] << " starts at " << start << std::endl;
        // if(rank == 0) std::cout  << "\n";

        // how many variables are communicated in each direction
        // 2D
        // again going from the lower, left, front neighbor
        // commdirections.emplace_back();
        commx = pby;
        commy = pbx;
        commz = pbz;
        commv = 1;
        commw = 1;        
        // 3D
        if constexpr (Dim == 3)
        {
            commx = pby*pbz;
            commy = pbx*pbz;
            commz = pbx*pby;
            commv = pbz;
            commw = pbz;
        }

        // where to start looking for the values that need to be communicated (considering local overlap)
        size_t zoverlap = Dim/3 * (pbx+2) * (pby+2);
        startx = (pbx+2) + zoverlap;
        starty = 1 + zoverlap;
        startz = pbx+2 + 1;
        startv;
        startw;
        startcore = (pbx+2) + 1 + zoverlap;

        // init variables
        counts_send.resize(8 + Dim/3 * 18);
        counts_recv.resize(8 + Dim/3 * 18);
        displ_send.resize(8 + Dim/3 * 18, 0);
        displ_recv.resize(8 + Dim/3 * 18, 0);
        // 2D
        // how many variables are sent and received in every direction
        // keep in mind the communication directions are in this order: x -> y -> z
        counts_send[0] = commx, counts_send[1] = commx, counts_send[2] = commy, counts_send[3] = commy;
        counts_recv[0] = commx, counts_recv[1] = commx, counts_recv[2] = commy, counts_recv[3] = commy;

        // 3D
        if constexpr (Dim == 3)
        {
            // how many variables are sent and received in every direction
            counts_send[4] = commz, counts_send[5] = commz;
            counts_recv[4] = commz, counts_recv[5] = commz;
        }

        // calc displacements for send and receive
        for(auto i = 1; i < 2*Dim; i++)
            displ_send[i] = displ_send[i - 1] + counts_send[i - 1];

        for(auto i = 1; i < 2*Dim; i++)
            displ_recv[i] = displ_recv[i - 1] + counts_recv[i - 1];

        // in these buffers the data that will be sent and
        // received with MPI_Neighbor_Alltoallv will be saved
        sendbuf.resize(2*commx + 2*commy + 2*commz);
        recvbuf.resize(2*commx + 2*commy + 2*commz);
    }


    template<class Vector, int Dim>
    void ParCommunicator9Star<Vector,Dim>::setupsendbuffer(Vector& b)
    {
        // the order in the sendbuffer is according to:
        // first send left, then right, then down, then up, then (if existing) front and then back
        auto iter = sendbuf.begin();

        for(size_t i = 0; i < commx; i++, ++iter)
        {
            // iterating the left col/level and then skipping to next layer
            auto nextlayer = i/(pby) * (3*pbx+2 -(pbx+2));
            *iter = b[startcore + i*(pbx+2) + nextlayer];
            // std::cout << "Send left: " << startcore + i*(pbx+2) + nextlayer << std::endl;
        }
        for(size_t i = 0; i < commx; i++, ++iter)
        {
            // iterating the right col/level and then skipping to next layer
            auto nextlayer = i/(pby)*(3*pbx+2 -(pbx+2));
            *iter = b[startcore+pbx-1 + i*(pbx+2) + nextlayer];
            // std::cout << "Send right: " << startcore+pbx-1 + i*(pbx+2) + nextlayer << std::endl;
        }

        // here we send down and up
        for(size_t i = 0; i < commy; i++, ++iter)
        {
            // iterating the lower col/level and then skipping to next layer
            // nextlayer = in which layer are we * distance to skip between layers
            auto nextlayer = i/pbx * ((pbx+2)*pby + pbx);
            *iter = b[startcore + i + nextlayer];
            // std::cout << "Send down: " << startcore + i + nextlayer << std::endl;
        }
        for(size_t i = 0; i < commy; i++, ++iter)
        {
            // iterating the upper col/level and then skipping to next layer
            auto nextlayer = i/pbx * (pbx+(pbx+2)*pby);
            *iter = b[startcore+(pbx+2)*(pby-1) + i + nextlayer];
            // std::cout << "Send up: " << startcore+(pbx+2)*(pby-1) + i + nextlayer << std::endl;
        }

        for(size_t i = 0; i < commz; i++, ++iter)
        {
            // iterating the front level and then skipping to next layer
            *iter = b[startcore + i + i/pbx*2];
            // std::cout << "Send front: " << startcore + i + i/pbx*2 << std::endl;
        }
        for(size_t i = 0; i < commz; i++, ++iter)
        {
            // iterating the back level and then skipping to next layer
            *iter = b[startcore + 2*commy - (2*pbx) + (pbx+2)*pby*(pbz-1) + i + i/pbx*2];
            // std::cout << "Send back: " << startcore + 2*commy - (2*pbx) + (pbx+2)*pby*(pbz-1) + i + i/pbx*2 << std::endl;
        }

        // if(rank == 0)
        // {
        //     std::cout << "Sendbuf: ";
        //     for(auto i : sendbuf)
        //         std::cout << i << " ";
        //     std::cout << std::endl;
        // }
    }

    template<class Vector, int Dim>
    void ParCommunicator9Star<Vector,Dim>::setuprecvbuffer(Vector& x)
    {
        auto iter = recvbuf.begin();
    
        for(size_t i = 0; i < commx; i++, ++iter)
        {
            // iterating the left col/level and then skipping to next layer
            auto nextlayer = i/pby * (3*pbx+2 -(pbx+2));
            x[startx + i*(pbx+2) + nextlayer] = *iter;
            // std::cout << "Recv left: " << startx + i*(pbx+2) + nextlayer << std::endl;
        }
        for(size_t i = 0; i < commx; i++, ++iter)
        {
            // iterating the right col/level and then skipping to next layer
            auto nextlayer = i/(pby) * (3*pbx+2 -(pbx+2));
            x[startx+pbx+1 + i*(pbx+2) + nextlayer] = *iter;
            // std::cout << "Recv right: " << startx+pbx+1 + i*(pbx+2) + nextlayer << std::endl;
        }

        for(size_t i = 0; i < commy; i++, ++iter)
        {
            // iterating the lower col/level and then skipping to next layer
            auto nextlayer = i/pbx * ((pbx+2)*pby + pbx); 
            x[starty + i + nextlayer] = *iter;
            // std::cout << "Recv down: " << starty + i + nextlayer << std::endl;
        }
        for(size_t i = 0; i < commy; i++, ++iter)
        {
            // iterating the upper col/level and then skipping to next layer
            auto nextlayer = i/pbx * (pbx+(pbx+2)*pby);
            x[starty+pbx+(pbx+2)*pby + i + nextlayer] = *iter;
            // std::cout << "Recv up: " << starty+pbx+(pbx+2)*pby + i + nextlayer << std::endl;
        }

        for(size_t i = 0; i < commz; i++, ++iter)
        {
            // iterating the front level
            x[startz + i] = *iter;
            // std::cout << "Recv front: " << startz + i << std::endl;
        }
        for(size_t i = 0; i < commz; i++, ++iter)
        {
            // iterating the back level
            x[startz + 2*commx+2*commy+commz + pbx*pby*pbz + i] = *iter;
            // std::cout << "Recv back: " << startz + 2*commx+2*commy+commz + pbx*pby*pbz + i << std::endl;
        }

        // if(rank == 0)
        // {
        //     std::cout << "Recvbuf: ";
        //     for(auto i : recvbuf)
        //         std::cout << i << " ";
        //     std::cout << std::endl;
        // }
    }
}




#endif