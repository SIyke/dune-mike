#ifndef DUNE_BLOCK_DECOMP3D_HH
#define DUNE_BLOCK_DECOMP3D_HH


#include <iostream>
#include <cstring>
#include <string.h>
#include <hwloc.h>
#include "parallelton.hh"
#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/scalarmatrixview.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvercategory.hh>
#include <algorithm>
#include "parcommunicator.hh"




namespace Dune{


    template<class M, class X, class Y, class ThreadBackend = ThreadPool>
    class BlockMatrixAdapter3D : public AssembledLinearOperator<M,X,Y>
    {
    public:
        //! export types
        typedef M matrix_type;
        typedef X domain_type;
        typedef Y range_type;
        typedef typename X::field_type field_type; 

        //! constructor: just store a reference to a matrix
        explicit BlockMatrixAdapter3D (const M& A, const ParData& p)
            : _A_(stackobject_to_shared_ptr(A)), pardata(stackobject_to_shared_ptr(p))
        {
            // setup variables used in every iteration
            context = Parallelton<ThreadBackend>::context;
            N = pardata->getN();
            size = pardata->getsize();
            nbrows = N*N*N / size;
            nbthreads = context->getnbthreads();
            stride = nbrows / nbthreads;

            pbx = pardata->getpbx();
            pby = pardata->getpby();
            pbz = pardata->getpbz();
    
            std::array<int, 3> dims;
            dims.fill(0);
            MPI_Dims_create(nbthreads, 3, &dims[0]);

            threadblockx = dims[0];
            threadblocky = dims[1];
            threadblockz = dims[2];
            
            // per thread in x and y direction
            ptx = pbx / threadblockx;
            pty = pby / threadblocky;
            ptz = pbz / threadblockz;

            // indices = setupoverlapindices3D(N, *pardata);
        }

        //! constructor: store an std::shared_ptr to a matrix
        explicit BlockMatrixAdapter3D (std::shared_ptr<const M> A, std::shared_ptr<const ParData> p)
            : _A_(A), pardata(p)
        {}

        ~BlockMatrixAdapter3D ()
        {}

        //! apply operator to x:  \f$ y = A(x) \f$
        void apply (const X& x, Y& y) const
        {
    // #ifdef DUNE_ISTL_WITH_CHECKING
    //       if (ready != built)
    //         DUNE_THROW(BCRSMatrixError,"You can only call arithmetic operations on fully built BCRSMatrix instances");
    //       if (x.N()!=M()) DUNE_THROW(BCRSMatrixError,"index out of range");
    //       if (y.N()!=N()) DUNE_THROW(BCRSMatrixError,"index out of range");
    // #endif
            // lambda for every thread
            std::mutex mtx;
            auto f = [this, &x, &y, &mtx](auto start, auto stride, size_t id)
            {
                // int rank;
                // MPI_Comm_rank(MPI_COMM_WORLD, &rank);
                // ConstRowIterators
                auto i = _A_->begin() + start;
                auto iend = i + stride;            
                
                auto tx = id%threadblockx;
                auto ty = (id/threadblockx) % threadblocky;
                auto tz = (id/(threadblockx * threadblocky)) % threadblockz;

                auto iindex = Parallelton<ThreadBackend>::threadstarts[id]; // thread block start

                auto overlapx = 0ul, overlapy = 0ul, overlapcorner = 0ul;
                // overlap on the sides
                if (tx == 0 || tx == threadblockx-1)
                    overlapx++;
                if (tx == 0 && tx == threadblockx-1)
                    overlapx++;
                // overlap on the top and bottom
                if (ty == 0 || ty == threadblocky-1)
                    overlapy += ptx;
                if (ty == 0 && ty == threadblocky-1)
                    overlapy += ptx;
                // corner overlap
                if (tx == 0 && ty == 0) // bottom left corner
                    overlapcorner++;
                if (tx == 0 && ty == threadblocky-1) // bottom right corner
                    overlapcorner++;
                if (tx == threadblockx-1 && ty == 0) // top left corner
                    overlapcorner++;
                if (tx == threadblockx-1 && ty == threadblocky-1) // top right corner
                    overlapcorner++;
                

                for (size_t k = 0; k < ptz; k++)
                {
                    for (size_t l = 0; l < pty; l++)
                    {                     
                        for (size_t m = 0; m < ptx; m++)
                        {
                            // index inside thread block
                            y[iindex] = 0;
                            // y[i.index()] = 0;
                            // ConstColIterators
                            auto j = (*i).begin();
                            auto endj = (*i).end();
                            for(; j != endj; ++j)
                            {
                                // if (j.index() >= (pbx+2) * (pby+2) * (pbz+2) || iindex >= (pbx+2) * (pby+2) * (pbz+2))
                                // {
                                //     std::lock_guard<std::mutex> lock(mtx);
                                //     std::cout << "exited T" << id << " tx:" << tx << " ty:" << ty << " tz:" << tz << " iid:" << iindex << " jid:" << j.index() << "\n";
                                //     exit(1);
                                // }
                                auto&& xj = Impl::asVector(x[j.index()]);
                                // auto&& yi = Impl::asVector(y[i.index()]);
                                auto&& yi = Impl::asVector(y[iindex]);
                                Impl::asMatrix(*j).umv(xj, yi);

                            }
                            // char c;
                            // std::cin >> c;
                            // if (id == 17)
                            //     std::cout << iindex << " ";

                            // one element completed
                            ++i; iindex++;
                        }
                        // one row completed   
                        iindex += overlapx;
                    }
                    // one layer completed
                    iindex += overlapy + overlapcorner;
                    // if (id == 17)
                    //     std::cout << "\n";
                }
                // if (id == 17)
                // {
                //     std::cout << "exited\n";
                //     exit(1);
                // }
                    


                // // old approach
                // for(; i != iend; ++i)
                // {
                //     // index inside thread block
                //     auto iindex = indices[i.index()];
                //     y[iindex] = 0;
                //     // y[i.index()] = 0;
                //     // ConstColIterators
                //     auto j = (*i).begin();
                //     auto endj = (*i).end();
                //     for(; j != endj; ++j)
                //     {
                //         auto&& xj = Impl::asVector(x[j.index()]);
                //         // auto&& yi = Impl::asVector(y[i.index()]);
                //         auto&& yi = Impl::asVector(y[iindex]);
                //         Impl::asMatrix(*j).umv(xj, yi);                 
                //     }
                //     // char c;
                //     // std::cin >> c;
                //     // if (id == 5)
                //     //     std::cout << iindex << " ";
                // }
                // // if (id == 5)
                // //     std::cout << "\n";
                // // if (id == 5)
                // //     exit(1);
            };

            // auto rest = nbrows % nbthreads;
            // start threads
            for(size_t i = 0; i < nbthreads; i++)
            {
                auto current = i * stride;

                context->addtask(f, i, current, stride, i);
            }
            // wait for threads
            context->thread0task();
        }

        //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
        void applyscaleadd (field_type alpha, const X& x, Y& y) const override
        {}

        //! get matrix via *
        const M& getmat () const override
        {
            return *_A_;
        }

        //! Category of the solver (see SolverCategory::Category)
        SolverCategory::Category category() const override
        {
            return SolverCategory::sequential;
        }


    private:
        const std::shared_ptr<const M> _A_;
        const std::shared_ptr<const ParData> pardata;
        double innerwatch = 0;
        double outerwatch = 0;
        // variables in each iteration
        size_t N;
        size_t size;
        size_t nbrows;
        size_t nbthreads;
        size_t stride;
        size_t pbx, pby, pbz;
        size_t threadblockx, threadblocky, threadblockz;
        size_t ptx, pty, ptz;
        std::shared_ptr<ThreadBackend> context;
    };

}


#endif