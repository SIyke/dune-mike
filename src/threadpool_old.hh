#ifndef DUNE_THREAD_POOL_HH
#define DUNE_THREAD_POOL_HH


#include <vector>
#include <thread>
#include <atomic>
#include <functional>
#include <mutex>
#include <queue>
#include <chrono>
#include "barrier"
#include "parallelton.hh"
#include <hwloc.h>


namespace Dune
{    
    class ThreadPoolOld
    {
    public:

        size_t thread_number;

        ThreadPoolOld(size_t n)
        : thread_number(n), threads(n-1), barrier(n)
        {
            createthreads();

            // barrier.wait(0);
        }


        ~ThreadPoolOld()
        {
            waittasks();
            running = false;
            destroythreads();
        }
        
        size_t getqueuedtasks()
        {
            return tasks.size();
        }

        template<typename T>
        void addtask(const T& task)
        {
            task_number++;
            {
                std::lock_guard<std::mutex> lock(mtx);
                tasks.emplace(std::function<void()>(task));
            }
        }

        template <typename T, typename... Ts>
        void addtask(const T& task, const Ts&... args)
        {
            addtask([task, args...]
                    { task(args...); });
        }

        template <typename T, typename... Ts>
        void addtask(const T& task, size_t id, const Ts&... args)
        {
            addtask([task, args...]
                    { task(args...); });
        }

        void waittasks()
        {
            std::function<void()> task;

            while (true)
            {
                if(task_number == 0)
                {
                    break;
                }
                else if(poptask(task))
                {
                    task();
                    task_number--;
                }
            }
        }

        void destroypool()
        {
            waittasks();
            running = false;
            destroythreads();
        }

        bool bindthreads(std::vector<size_t>& logindices)
        {
            int rank;
            MPI_Comm_rank(MPI_COMM_WORLD, &rank);

            auto bind = [this](size_t logid)
            {
                // bind this thread to the correct core
                hwloc_bitmap_t set = hwloc_bitmap_alloc();
                if (!set) {
                    fprintf(stderr, "failed to allocate a bitmap\n");
                    hwloc_topology_destroy(Parallelton<ThreadPoolOld>::topology);
                }

                auto logobj = hwloc_get_obj_by_type(Parallelton<ThreadPoolOld>::topology, HWLOC_OBJ_PU, logid);
                hwloc_bitmap_only(set, logobj->os_index);

                auto err = hwloc_set_cpubind(Parallelton<ThreadPoolOld>::topology, set, HWLOC_CPUBIND_THREAD);
                if (err < 0) {
                    fprintf(stderr, "failed to set thread binding\n");
                    hwloc_bitmap_free(set);
                    hwloc_topology_destroy(Parallelton<ThreadPoolOld>::topology);
                }

                // auto i = hwloc_bitmap_first(set);
                // auto obj = hwloc_get_pu_obj_by_os_index(Parallelton<ThreadPool>::topology, i);
                // std::cout << "Rank=" << Parallelton<ThreadPool>::rank << " logical index=" 
                //         << obj->logical_index << " (OS/physical index " << i << ")" << std::endl;
                hwloc_bitmap_free(set);
            };

            for(size_t id = 0; id < thread_number; id++)
            {
                auto logid = logindices[id + rank * thread_number];
                addtask(bind, id, logid);
            }

            waittasks();
            return true;
        }


        template<typename Lambda>
        bool bindthreads(Lambda lambda)
        {
            auto bind = [this](size_t logid)
            {
                // bind this thread to the correct core
                hwloc_bitmap_t set = hwloc_bitmap_alloc();
                if (!set) {
                    fprintf(stderr, "failed to allocate a bitmap\n");
                    hwloc_topology_destroy(Parallelton<ThreadPoolOld>::topology);
                }

                auto logobj = hwloc_get_obj_by_type(Parallelton<ThreadPoolOld>::topology, HWLOC_OBJ_PU, logid);
                hwloc_bitmap_only(set, logobj->os_index);

                auto err = hwloc_set_cpubind(Parallelton<ThreadPoolOld>::topology, set, HWLOC_CPUBIND_THREAD);
                if (err < 0) {
                    fprintf(stderr, "failed to set thread binding\n");
                    hwloc_bitmap_free(set);
                    hwloc_topology_destroy(Parallelton<ThreadPoolOld>::topology);
                }

                hwloc_bitmap_free(set);
            };

            int rank;
            MPI_Comm_rank(MPI_COMM_WORLD, &rank);

            for(size_t id = 0; id < thread_number; id++)
            {
                auto logid = lambda(id, rank);
                addtask(bind, id, logid);
            }

            waittasks();
            return true;
        }

    private:

        void createthreads()
        {
            for (size_t i = 1; i < thread_number; i++)
                threads[i-1] = std::thread(&ThreadPoolOld::workloop, this);
        }

        void destroythreads()
        {
            for (size_t i = 1; i < thread_number; i++)
                threads[i-1].join();
        }

        void workloop()
        {
            while (running)
            {
                std::function<void()> task;
                if(poptask(task))
                {
                    task();
                    task_number--;
                }
                else
                {
                    //sleepyield();
                }
            }
        }

        bool poptask(std::function<void()>& task)
        {
            std::lock_guard<std::mutex> lock(mtx);
            if(tasks.empty())
            {
                return false;
            }
            else
            {
                task = std::move(tasks.front());
                tasks.pop();
                return true;
            }
        }

        void sleepyield()
        {
            if(sleepduration)
                std::this_thread::sleep_for(std::chrono::microseconds(sleepduration));
            else
                std::this_thread::yield();
        }


        std::vector<std::thread> threads;
        std::atomic<bool> running = true;
        
        std::queue<std::function<void()>> tasks;

        // std::vector<std::queue<std::function<void()>>> thread_based_tasks;

        std::atomic<size_t> task_number = 0;
        std::mutex mtx;
        size_t sleepduration = 1000;

        Barrier barrier;
    };

    typedef std::shared_ptr<ThreadPoolOld> ThreadPoolOldContext;
}


#endif