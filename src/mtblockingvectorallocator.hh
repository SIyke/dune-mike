#ifndef DUNE_MTBLOCKV_ALLOCATOR_HH
#define DUNE_MTBLOCKV_ALLOCATOR_HH


#include <iostream>
#include <cstring>
#include <string.h>
#include <hwloc.h>
#include "parallelton.hh"

namespace Dune{

    template<class T, class ThreadBackend = ThreadPool>
    class MtBlockVectorAllocator
    {
    public:
        typedef T value_type;
        typedef std::size_t size_type;
        typedef ptrdiff_t difference_type;


        MtBlockVectorAllocator() = default;

        template<class U>
        MtBlockVectorAllocator (const MtBlockVectorAllocator<U>& other) noexcept {}

        MtBlockVectorAllocator& operator=(const MtBlockVectorAllocator&) = default;

        template<class U>
        bool operator==(const MtBlockVectorAllocator<U>&) const noexcept {return true;}

        template<class U>
        bool operator!=(const MtBlockVectorAllocator<U>&) const noexcept {return false;}


        T* allocate(size_type n) const
        {
            // T* t = new T[n];
            T* t = (T*) hwloc_alloc(Parallelton<ThreadPool>::topology, sizeof(T) * n);
            
            // std::cout << "Allocating " << (n * sizeof(T)) / (1024.0 * 1024.0) << " MB\n"; 

            typedef Parallelton<ThreadBackend> ParalleltonTB;
            auto context = ParalleltonTB::context;

            int rank, size;
            MPI_Comm_rank(MPI_COMM_WORLD, &rank);
            MPI_Comm_size(MPI_COMM_WORLD, &size);
            auto N = ParalleltonTB::N;

            // preparing of variables to determine correct memory binding
            constexpr int Dim = 2;

            std::array<int, Dim> dims;
            dims.fill(0);
            MPI_Dims_create(size, Dim, &dims[0]);

            size_t blockx = dims[0];
            size_t blocky = dims[1];
            size_t blockz = 0ul;
            
            if constexpr (Dim == 3)
                blockz = dims[2];

            size_t pbx = N / blockx;
            size_t pby = N / blocky;
            size_t pbz = 0ul;

            if constexpr (Dim == 3)
                pbz = N / blockz;

                    
            dims.fill(0);
            MPI_Dims_create(context->getnbthreads(), Dim, &dims[0]);

            size_t threadblockx = dims[0];
            size_t threadblocky = dims[1];
            size_t threadblockz = 0ul;

            if constexpr (Dim == 3)
                threadblockz = dims[2];

            auto ptx = pbx / threadblockx;
            auto pty = pby / threadblocky;
            auto ptz = 1ul;

            if constexpr (Dim == 3)
                ptz = pbz / threadblockz;


            // elements per thread = how many elements will get allocated / over how many threads are they distributed
            // auto ept = n / context->getnbthreads();


            hwloc_bitmap_t set;

            char* s;
            int err;
            set = hwloc_bitmap_alloc();
            if (!set) {
                fprintf(stderr, "failed to allocate a bitmap\n");
                hwloc_topology_destroy(ParalleltonTB::topology);
            }

            // initial offset
            size_t offset = 0;

            if constexpr (Dim == 3)
                offset = (pbx+2) * (pby+2);

            // std::cout << "TBlockx: " << threadblockx << " TBlocky: " << threadblocky << " TBlockz: " << threadblockz << 
            //                 " ptx: " << ptx << " pty: " << pty << " ptz: " << ptz << "\n";

            for(size_t i = 0; i < context->getnbthreads(); i++)
            {
                // coordinates of the current thread block
                size_t tx = i%dims[0];
                size_t ty = (i/dims[0]) % threadblocky;

                // elements per thread = elements in x, y and z direction of a thread block
                auto elementsx = ptx, elementsy = pty;
                if (tx == 0 || tx == threadblockx-1) // overlap on one side
                    elementsx++;
                if (tx == 0 && tx == threadblockx-1) // overlap on both sides
                    elementsx++;

                if (ty == 0 || ty == threadblocky-1) // overlap on one side
                    elementsy++;
                if (ty == 0 && ty == threadblocky-1) // overlap on both sides
                    elementsy++;

                size_t ept = elementsx * elementsy * ptz;

                // char c;
                // std::cin >> c;
                // std::cout << "tx:" << tx << " ty:" << ty << " ept: " << ept << " offset:" << offset << std::endl;

                auto logid = ParalleltonTB::lambda(i, rank, context->getnbthreads());
                auto logobj = hwloc_get_obj_by_type(ParalleltonTB::topology, HWLOC_OBJ_PU, logid);
                hwloc_bitmap_only(set, logobj->os_index);
                
                err = hwloc_set_area_membind(ParalleltonTB::topology, t + offset, sizeof(T) * ept, set, HWLOC_MEMBIND_BIND, 
                    HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
                if (err < 0)
                    std::cout << "Error: memory binding failed" << std::endl;

                offset += ept;
                
                // hwloc_bitmap_asprintf(&s, set);

                // std::cout << "Rank=" << ParalleltonTB::rank << " Tid=" << i
                //     << " on PU logical index=" << s << std::endl;
            }
            


            // // first touching
            // for(size_type i = 0; i < n; i++)
            // {
            //     T tmp;
            //     t[i] = tmp;
            // }

            // // checking where memory is allocated to
            // hwloc_membind_policy_t policy;
            // hwloc_bitmap_zero(set);
            // err = hwloc_get_area_membind(ParalleltonTB::topology, t, sizeof(T)*n, set, &policy, HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
            // if(err < 0)
            //     std::cout << "Error: getting memory binding failed" << std::endl;

            // // print out data
            // hwloc_bitmap_asprintf(&s, set);
            // std::cout << "R" << rank << " actually on " << s << std::endl;

            // auto touch = [t](size_t start, size_t stride, size_t id)
            // {
            //     for(auto i = start; i < start+stride; i++)
            //     {
            //         T tmp;
            //         t[i] = tmp;
            //     }
            // };

            // for(size_t i = 0; i < context->getnbthreads(); i++)
            // {
            //     auto current = i * ept;
            //     context->addtask(touch, i, current, ept, i);
            // }
            // context->waittasks();


            // // checking where memory is allocated to
            // for(size_t i = 0; i < context->getnbthreads(); i++)
            // {
            //     hwloc_membind_policy_t policy;
            //     hwloc_bitmap_zero(set);
            //     err = hwloc_get_area_membind(ParalleltonTB::topology, t+i*ept, sizeof(T)*ept, set, &policy, HWLOC_MEMBIND_STRICT | HWLOC_MEMBIND_THREAD);
            //     if(err < 0)
            //         std::cout << "Error: getting memory binding failed" << std::endl;

            //     // print out data
            //     hwloc_bitmap_asprintf(&s, set);
            //     std::cout << "T" << i << " actually on " << s << std::endl; 
            // }

            hwloc_bitmap_free(set);
            return t;      
        }

        void deallocate(T* p, size_type n) const
        {
            if(p)
            {
                // delete[] p;
                hwloc_free(Parallelton<ThreadPool>::topology, p, sizeof(T) * n);
            }
        }
    };
}



#endif