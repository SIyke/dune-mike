#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <functional>
#include <chrono>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/istl/io.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/istl/overlappingschwarz.hh>
#include <dune/istl/solvers.hh>
#include "laplacian.hh"
//#include "custompreconditioner.hh"
#include "customlaplacian.hh"
//#include "custombcrsmatrix.hh"
//#include "customgsetc.hh"
#include "threadconstruction.hh"
#include "parmatrixadapter.hh"
#include "threadpool.hh"
#include "parallocator.hh"
#include "mtblockingvectorallocator.hh"
#include "timeexperiment.hh"
#include "parallelton.hh"
#include "parcommunicators.hh"
#include "parcalculations.hh"
#include "blockinglaplacian.hh"
#include "blockinglaplacian3D.hh"
#include "mtblockingdecomp.hh"
#include "mtblockingdecomp3d.hh"

#include <hwloc.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>


namespace Dune
{
  // function for printing a sparse matrix
  template<class Mat>
  void print_sparse_matrix(const Mat& A)
  {
      auto N = A.N();
      auto M = A.M();
      // print out sparsity pattern
      for(auto x = 0; x < N; x++)
      {
          for(auto y = 0; y < M; y++)
          {
              if(A.exists(x, y))
                std::cout << std::fixed << std::setprecision(2) << A[x][y] << " ";
              else
                std::cout << std::fixed << std::setprecision(2) << 0.0 << " ";
          }
          std::cout << std::endl;
      }
      std::cout << std::endl;
  }

  // function for printing a vector
  template<class X>
  void print_vector(const X& x)
  {
      for(auto i = x.begin(); 
          i != x.end(); ++i)
      {
          std::cout << std::fixed << std::setprecision(1) << *i << " ";
      }
      std::cout << std::endl << std::endl;
  }

  template<class X>
  void print_vector_in_matrix_format(const X& x)
  {
    size_t stride = std::sqrt(x.size());

    for(auto i = x.end() - 1; 
        i != x.begin() - 1; --i)
    {
        if((i - x.end()+1) % stride == 0)
          std::cout << std::endl;

        std::cout << std::fixed << std::setprecision(1) << *i << " ";
    }
    std::cout << std::endl << std::endl;
  }
}



template <class ThreadBackend, class Matrix, class Vector>
class Par_2D_Experiment
{
public:

  typedef Parallelton<ThreadBackend> ParalleltonTP;

  Par_2D_Experiment(size_t N, size_t iter)
    : parcomm(N, pardata), iter(iter), fop(mat, pardata), x((pardata.getpbx()+2) * (pardata.getpby()+2)), parcalc(x, b, pardata)
  {    
    // parcomm init
    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    size = pardata.getsize();
    // auto overlap = 2*pbx + 2*pby;
    // matrix and vector setup
    x.resize((pbx+2) * (pby+2));
    b.resize((pbx+2) * (pby+2));
    b_0.resize((pbx+2) * (pby+2));

    // x.resize(pbx * pby);
    // b.resize(pbx * pby);
    // b_0.resize(pbx * pby);

    x = 1.0;
    b = 0.0;
    b_0 = 0.0;
    omega = 1.0;

    setupFragmentedLaplacian(mat, N, pardata);
    // setupLaplacian(mat, N);
  }

  // ~Par_2D_Experiment()
  // {
  //   // auto rank = pardata.getrank();

  //   // // std::cout << "Rank=" << rank << " Total time=" << totalwatch << std::endl;
  //   // if (rank == 0)
  //   // {
  //   //   std::cout << "Rank=" << rank << " Time for apply=" << applywatch << std::endl;
  //   //   std::cout << "Rank=" << rank << " Time for Richardson=" << richardsonwatch << std::endl;
  //   // }
  // }

  void run()
  {
    // auto totalstart = std::chrono::high_resolution_clock::now();    

    Vector *d;
    Vector *Ad;
    x = 1.0;
    b = 0.0;
    b_0 = 0.0;

    // initial d
    fop.apply(x, b);
    parcalc.subalt2D(b, b_0);
  
    if (size > 1)
      parcomm.communicate(b);

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
        d = &b;
        Ad = &b_0;
      }
      else
      {
        d = &b_0;
        Ad = &b;
      }

      // auto start = std::chrono::high_resolution_clock::now();
      fop.apply(*d, *Ad);
      // auto end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      // start = std::chrono::high_resolution_clock::now();
      parcalc.richardsonalt2D(*d, *Ad, omega);
      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      if (size > 1)
        parcomm.communicate(*Ad);
    } 

    // auto totalend = std::chrono::high_resolution_clock::now();
    // totalwatch += std::chrono::duration_cast<std::chrono::duration<double>> (totalend - totalstart).count();
    
    // global sync
    while(!ParalleltonTP::context->notasksleft())
    {}
  }

  double operations () const
  {
    return 0;
  }

protected:
  Dune::ParCommunicator5Star<Vector, 2> parcomm;
  size_t iter;
  Matrix mat;
  Vector x, b, b_0, result;
  double omega;
  Dune::ParData pardata;
  Dune::ParMatrixAdapter<Matrix, Vector, Vector, ThreadBackend> fop;
  Dune::ParCalculationAdapter<Vector> parcalc;

  int size;
  double totalwatch = 0.0;
  double applywatch = 0.0;
  double richardsonwatch = 0.0;
};




template <class ThreadBackend, class Matrix, class Vector>
class IPar_2D_Experiment : public Par_2D_Experiment<ThreadBackend, Matrix, Vector>
{
public:

  typedef Parallelton<ThreadBackend> ParalleltonTP;

  IPar_2D_Experiment(size_t N, size_t iter)
    : Par_2D_Experiment<ThreadBackend, Matrix, Vector>(N, iter)
  {
    parcalc.setupborderindices2D();
  }

  // ~IPar_2D_Experiment()
  // {
  //   // auto rank = pardata.getrank();
  //   // std::cout << "Rank=" << rank << " Wait time=" << waitwatch << std::endl;
  // }

  void run()
  {
    // auto totalstart = std::chrono::high_resolution_clock::now();    
    Vector *d;
    Vector *Ad;
    x = 1.0;
    b = 0.0;
    b_0 = 0.0;

    // d_0 = b_0 - Ax_0
    fop.apply(x, b);
    parcalc.subalt(b, b_0);

    parcomm.communicate(b);

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
          d = &b;
          Ad = &b_0;
      }
      else
      {
          d = &b_0;
          Ad = &b;
      }

      // auto start = std::chrono::high_resolution_clock::now();
      fop.applyouter(*d, *Ad);
      // auto end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      // start = std::chrono::high_resolution_clock::now();
      parcalc.richarddefectout2D(*d, *Ad, omega);
      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      parcomm.Icommunicatestart(*Ad);

      // start = std::chrono::high_resolution_clock::now();
      fop.applyinner(*d, *Ad);
      // end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      // start = std::chrono::high_resolution_clock::now();
      parcalc.richarddefectin2D(*d, *Ad, omega);
      // parcalc.iteration(*Ad, omega);
      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();


      // start = std::chrono::high_resolution_clock::now();
      parcomm.Icommunicateend(*Ad);
      // end = std::chrono::high_resolution_clock::now();
      // waitwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
    } 

    // auto totalend = std::chrono::high_resolution_clock::now();
    // totalwatch += std::chrono::duration_cast<std::chrono::duration<double>> (totalend - totalstart).count();

    // parcomm.gatherresult(b, result);
  }

private:
  typedef Par_2D_Experiment<ThreadBackend, Matrix, Vector> Base;
  
  using Base::iter;
  using Base::mat;
  using Base::x; using Base::b; using Base::b_0; using Base::result;
  using Base::omega;
  using Base::pardata;
  using Base::parcomm;
  using Base::parcalc;
  using Base::fop;
  using Base::totalwatch;
  using Base::applywatch;
  using Base::richardsonwatch;
  double waitwatch = 0.0;
};





template <class ThreadBackend, class Matrix, class Vector>
class Par_3D_Experiment
{
public:

  typedef Parallelton<ThreadBackend> ParalleltonTP;

  Par_3D_Experiment(size_t N, size_t iter)
    : parcomm(N, pardata), iter(iter), fop(mat, pardata), x((pardata.getpbx()+2) * (pardata.getpby()+2) * (pardata.getpbz()+2)), parcalc(x, b, pardata)
  {
    // parcomm init
    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    size = pardata.getsize();
    // matrix and vector setup
    x.resize((pbx+2) * (pby+2) * (pbz+2));
    b.resize((pbx+2) * (pby+2) * (pbz+2));
    b_0.resize((pbx+2) * (pby+2) * (pbz+2));
      
    x = 1.0;
    b_0 = 0.0;
    omega = 1.0;

    setupFragmented3DLaplacian(mat, N, pardata);
  }

  // ~Par_3D_Experiment()
  // {
  //   // auto rank = pardata.getrank();
  //   // std::cout << "Rank=" << rank << " Total time=" << totalwatch << std::endl;
  //   // std::cout << "Rank=" << rank << " Time for apply=" << applywatch << std::endl;
  //   // std::cout << "Rank=" << rank << " Time for Richardson=" << richardsonwatch << std::endl;
  // }

  void run()
  {
    // auto totalstart = std::chrono::high_resolution_clock::now();    
    Vector* d;
    Vector* Ad;
    // intial d
    fop.apply(x, b);
    parcalc.subalt3D(b, b_0);

    if (size > 1)
      parcomm.communicate(b);

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
          d = &b;
          Ad = &b_0;
      }
      else
      {
          d = &b_0;
          Ad = &b;
      }

      // auto start = std::chrono::high_resolution_clock::now();
      fop.apply(*d, *Ad);
      // auto end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      // start = std::chrono::high_resolution_clock::now();

      parcalc.richardsonalt3D(*d, *Ad, omega);
      // parcalc.iteration(*Ad, omega);

      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      if (size > 1)
        parcomm.communicate(b);
    }

    // auto totalend = std::chrono::high_resolution_clock::now();
    // totalwatch += std::chrono::duration_cast<std::chrono::duration<double>> (totalend - totalstart).count();

    // global sync
    while(!ParalleltonTP::context->notasksleft())
    {}
  }

  double operations () const
  {
    return 0;
  }

protected:
  Dune::ParCommunicator5Star<Vector, 3> parcomm;
  size_t iter;
  Matrix mat;
  Vector x, b, b_0, result;
  double omega;
  Dune::ParData pardata;
  Dune::ParMatrixAdapter3D<Matrix, Vector, Vector, ThreadBackend> fop;
  Dune::ParCalculationAdapter<Vector> parcalc;

  int size;
  double totalwatch = 0.0;
  double applywatch = 0.0;
  double richardsonwatch = 0.0;
};








template <class ThreadBackend, class Matrix, class Vector>
class Par_2D_Experiment_BlockedMT
{
public:

  typedef Parallelton<ThreadBackend> ParalleltonTP;

  Par_2D_Experiment_BlockedMT(size_t N, size_t iter)
    : parcomm(N, pardata), iter(iter), fop(mat, pardata), x((pardata.getpbx()+2) * (pardata.getpby()+2)), parcalc(x, b, pardata)
  {
    // parcomm init
    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    size = pardata.getsize();

    // setup start point of the thread blocks
    auto context = ParalleltonTP::context;
    auto nbthreads = context->getnbthreads(); 
      
    // auto overlap = 2*pbx + 2*pby;
    // matrix and vector setup
    x.resize((pbx+2) * (pby+2));
    b.resize((pbx+2) * (pby+2));
    b_0.resize((pbx+2) * (pby+2));

    // x.resize(pbx * pby);
    // b.resize(pbx * pby);
    // b_0.resize(pbx * pby);

    x = 1.0;
    b = 0.0;
    b_0 = 0.0;
    omega = 1.0;

    setupBlockedLaplacian2D(mat, N, pardata);

    // Dune::print_sparse_matrix(mat);
    // char c;
    // std::cin >> c;

    // setupLaplacian(mat, N);
  }

  // ~Par_2D_Experiment_BlockedMT()
  // {
  //   // auto rank = pardata.getrank();

  //   // // std::cout << "Rank=" << rank << " Total time=" << totalwatch << std::endl;
  //   // if (rank == 0)
  //   // {
  //   //   std::cout << "Rank=" << rank << " Time for apply=" << applywatch << std::endl;
  //   //   std::cout << "Rank=" << rank << " Time for Richardson=" << richardsonwatch << std::endl;
  //   // }
  // }

  void run()
  {
    // auto totalstart = std::chrono::high_resolution_clock::now();    

    Vector *d;
    Vector *Ad;
    x = 1.0;
    b = 0.0;
    b_0 = 0.0;
    // initial d
    fop.apply(x, b);
    parcalc.subalt2Dblocked(b, b_0);

    if(size > 1)
      parcomm.communicate(b);

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
        d = &b;
        Ad = &b_0;
      }
      else
      {
        d = &b_0;
        Ad = &b;
      }

      // auto start = std::chrono::high_resolution_clock::now();
      fop.apply(*d, *Ad);
      // auto end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();


      // start = std::chrono::high_resolution_clock::now();
      parcalc.richardsonalt2Dblocked(*d, *Ad, omega);
      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      if(size > 1)
        parcomm.communicate(b);
    } 

    // auto totalend = std::chrono::high_resolution_clock::now();
    // totalwatch += std::chrono::duration_cast<std::chrono::duration<double>> (totalend - totalstart).count();
    
    // global sync
    while(!ParalleltonTP::context->notasksleft())
    {}
  }

  double operations () const
  {
    return 0;
  }

protected:
  Dune::BlockedParCommunicator5Star<Vector, 2> parcomm;
  size_t iter;
  Matrix mat;
  Vector x, b, b_0, result;
  double omega;
  Dune::ParData pardata;
  Dune::BlockMatrixAdapter2D<Matrix, Vector, Vector, ThreadBackend> fop;
  Dune::ParCalculationAdapter<Vector> parcalc;

  int size;
  double totalwatch = 0.0;
  double applywatch = 0.0;
  double richardsonwatch = 0.0;
};






template <class ThreadBackend, class Matrix, class Vector>
class Par_3D_Experiment_BlockedMT
{
public:

  typedef Parallelton<ThreadBackend> ParalleltonTP;

  Par_3D_Experiment_BlockedMT(size_t N, size_t iter)
    : parcomm(N, pardata), iter(iter), fop(mat, pardata), x((pardata.getpbx()+2) * (pardata.getpby()+2) * (pardata.getpbz()+2)), parcalc(x, b, pardata)
  {
    // parcomm init
    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    size = pardata.getsize();

    // setup start point of the thread blocks
    auto context = ParalleltonTP::context;
    auto nbthreads = context->getnbthreads(); 
      
    // auto overlap = 2*pbx + 2*pby;
    // matrix and vector setup
    x.resize((pbx+2) * (pby+2) * (pbz+2));
    b.resize((pbx+2) * (pby+2) * (pbz+2));
    b_0.resize((pbx+2) * (pby+2) * (pbz+2));

    // x.resize(pbx * pby);
    // b.resize(pbx * pby);
    // b_0.resize(pbx * pby);

    x = 1.0;
    b = 0.0;
    b_0 = 0.0;
    omega = 1.0;

    setupBlockedLaplacian3D(mat, N, pardata);
  }

  // ~Par_3D_Experiment_BlockedMT()
  // {
  //   // auto rank = pardata.getrank();

  //   // // std::cout << "Rank=" << rank << " Total time=" << totalwatch << std::endl;
  //   // if (rank == 0)
  //   // {
  //   //   std::cout << "Rank=" << rank << " Time for apply=" << applywatch << std::endl;
  //   //   std::cout << "Rank=" << rank << " Time for Richardson=" << richardsonwatch << std::endl;
  //   // }
  // }

  void run()
  {
    // auto totalstart = std::chrono::high_resolution_clock::now();    

    Vector *d;
    Vector *Ad;
    x = 1.0;
    b = 0.0;
    b_0 = 0.0;

    // initial d
    fop.apply(x, b);

    parcalc.subalt3D(b, b_0);

    if(size > 1)
      parcomm.communicate(b);

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
        d = &b;
        Ad = &b_0;
      }
      else
      {
        d = &b_0;
        Ad = &b;
      }

      // auto start = std::chrono::high_resolution_clock::now();
      fop.apply(*d, *Ad);
      // auto end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();


      // start = std::chrono::high_resolution_clock::now();
      parcalc.richardsonalt3Dblocked(*d, *Ad, omega);
      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      if (size > 1)
        parcomm.communicate(*Ad);
    } 

    // auto totalend = std::chrono::high_resolution_clock::now();
    // totalwatch += std::chrono::duration_cast<std::chrono::duration<double>> (totalend - totalstart).count();
    // global sync
    while(!ParalleltonTP::context->notasksleft())
    {}
  }

  double operations () const
  {
    return 0;
  }

protected:
  Dune::BlockedParCommunicator5Star<Vector, 3> parcomm;
  size_t iter;
  Matrix mat;
  Vector x, b, b_0, result;
  double omega;
  Dune::ParData pardata;
  Dune::BlockMatrixAdapter3D<Matrix, Vector, Vector, ThreadBackend> fop;
  Dune::ParCalculationAdapter<Vector> parcalc;

  int size;
  double totalwatch = 0.0;
  double applywatch = 0.0;
  double richardsonwatch = 0.0;
};











template <class ThreadBackend, class Matrix, class Vector>
class IPar_3D_Experiment : public Par_3D_Experiment<ThreadBackend, Matrix, Vector>
{
public:

  typedef Parallelton<ThreadBackend> ParalleltonTP;

  IPar_3D_Experiment(size_t N, size_t iter)
    : Par_3D_Experiment<ThreadBackend, Matrix, Vector>(N, iter)
  {
    parcalc.setupborderindices3D();
  }

  ~IPar_3D_Experiment()
  {
    // auto rank = pardata.getrank();
    // std::cout << "Rank=" << rank << " Wait time=" << waitwatch << std::endl;
  }

  void run()
  {
    // auto totalstart = std::chrono::high_resolution_clock::now();    

    Vector* d;
    Vector* Ad;
    typedef Dune::ParCalculationAdapter<Vector> ParCalc;

    // d_0 = b_0 - Ax_0
    fop.apply(x, b);
    parcalc.subalt(b, b_0);

    parcomm.communicate(b);

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
          d = &b;
          Ad = &b_0;
      }
      else
      {
          d = &b_0;
          Ad = &b;
      }

      // auto start = std::chrono::high_resolution_clock::now();
      fop.applyouter(*d, *Ad);
      // auto end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      // start = std::chrono::high_resolution_clock::now();
      parcalc.richarddefectout3D(*d, *Ad, omega);
      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      parcomm.Icommunicatestart(*Ad);

      // start = std::chrono::high_resolution_clock::now();
      fop.applyinner(*d, *Ad);
      // end = std::chrono::high_resolution_clock::now();
      // applywatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      // start = std::chrono::high_resolution_clock::now();
      parcalc.richarddefectin3D(*d, *Ad, omega);
      // parcalc.iteration(*Ad, omega);
      // end = std::chrono::high_resolution_clock::now();
      // richardsonwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

      // start = std::chrono::high_resolution_clock::now();
      parcomm.Icommunicateend(*Ad);
      // end = std::chrono::high_resolution_clock::now();
      // waitwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
    } 

    // auto totalend = std::chrono::high_resolution_clock::now();
    // totalwatch += std::chrono::duration_cast<std::chrono::duration<double>> (totalend - totalstart).count();

    // parcomm.gatherresult(b, result);
  }

private:
  typedef Par_3D_Experiment<ThreadBackend, Matrix, Vector> Base;
  
  using Base::iter;
  using Base::mat;
  using Base::x; using Base::b; using Base::b_0; using Base::result;
  using Base::omega;
  using Base::pardata;
  using Base::parcomm;
  using Base::fop;
  using Base::parcalc;
  using Base::totalwatch;
  using Base::applywatch;
  using Base::richardsonwatch;
  double waitwatch = 0.0;
};




template<class Thread, class Matrix, class Vector>
class Experiment
{
public:

  // definitions
  typedef std::shared_ptr<Thread> ThreadBackendContext;

  Experiment(size_t N, size_t iter_, bool ca, ThreadBackendContext ctx) 
    : iter(iter_), x(N*N), b(N*N), custom_alloc(ca), fop(mat, ctx)
  {
    setupLaplacian(mat, N);
    x = 100;
    b = 0;
  }

  void run()
  {

    if(!custom_alloc)
    {
      for(size_t i = 0; i < iter; i++)
        fop.apply(x,b);
    }
    else
    {
      for(size_t i = 0; i < iter; i++)
      {  
        fop.apply_alloc(x,b);
        // std::cout << "Iteration: " << i << std::endl;
      }
    }

    // std::cout << "b: "; 
    // Dune::print_vector(b);
    // std::cout << std::endl;
  }

  double operations () const
  {
    return 0;
  }

private:
  size_t iter;
  Matrix mat;
  Vector x;
  Vector b;
  bool custom_alloc;
  Dune::ParMatrixAdapter<Matrix, Vector, Vector, ThreadBackendContext> fop;
};



template<class Matrix, class Vector>
class SeqExperiment
{
public:

  SeqExperiment(size_t N, size_t iter_) 
    : iter(iter_), x(N*N), b(N*N), b_0(N*N), fop(mat)
  {
    setupLaplacian(mat, N);
    x = 1.0;
    b_0 = 0.0;
    omega = 1.0;
  }

  void run()
  {
    // intial d
    // d_0 = b - Ax
    fop.apply(x, b);

    for(auto i = 0ul; i < b.size(); i++)
      b[i] = b_0[i] - b[i];

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
        d = &b;
        Ad = &b_0;
      }
      else
      {
        d = &b_0;
        Ad = &b;
      }
      fop.apply(*d, *Ad);
      // Richardson
      for(auto j = 0ul; j < b.size(); j++)
      {
        // d_k+1 = d_k - omega * Ad_k
        (*Ad)[j] = (*d)[j] - omega * (*Ad)[j];
        // x_k+1 = x_k + omega * d_k
        x[j] = x[j] + omega * (*Ad)[j];
      }
    }  
  }

private:
  size_t iter;
  Matrix mat;
  Vector x;
  Vector b;
  Vector b_0;
  Vector* d;
  Vector* Ad;
  double omega;
  Dune::MatrixAdapter<Matrix, Vector, Vector> fop;
};


template<class Matrix, class Vector>
class Seq3DExperiment
{
public:

  Seq3DExperiment(size_t N, size_t iter_) 
    : iter(iter_), x(N*N*N), b(N*N*N), b_0(N*N*N), fop(mat)
  {
    setup3DLaplacian(mat, N);
    x = 1.0;
    b_0 = 0.0;
    omega = 1.0;
  }


  void run()
  {
    // intial d
    // d_0 = b - Ax
    fop.apply(x, b);

    for(auto i = 0ul; i < b.size(); i++)
      b[i] = b_0[i] - b[i];

    for(size_t i = 0; i < iter; i++)
    {
      if(i % 2 == 0)
      {
        d = &b;
        Ad = &b_0;
      }
      else
      {
        d = &b_0;
        Ad = &b;
      }
      fop.apply(*d, *Ad);
      // Richardson
      for(auto j = 0ul; j < b.size(); j++)
      {
        // d_k+1 = d_k - omega * Ad_k
        (*Ad)[j] = (*d)[j] - omega * (*Ad)[j];
        // x_k+1 = x_k + omega * d_k
        x[j] = x[j] + omega * (*Ad)[j];
      }
    } 
  }

private:
  size_t iter;
  Matrix mat;
  Vector x;
  Vector b;
  Vector b_0;
  Vector* d;
  Vector* Ad;
  double omega;
  Dune::MatrixAdapter<Matrix, Vector, Vector> fop;
};




int main(int argc, char** argv)
{
  int thread_safety_level;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_safety_level);

  // std::cout << thread_safety_level << std::endl;
  if(MPI_THREAD_FUNNELED > thread_safety_level)
  {
    std::cout << "required MPI thread safety level not provided" << std::endl;
    MPI_Finalize();
    return 1;
  }
  // definitions
  const int BS=1;
  if(argc != 3)
  {
    std::cout << "Not enough Arguments! Arg1: nb threads; Arg2: N" << std::endl;
    MPI_Finalize();
    return 2;
  }

  auto threadcount = std::atoi(argv[1]);
  auto N = std::atoi(argv[2]);

  // auto iter = std::atoi(argv[3]);
  auto iter = 100;

  // dune-istl types  
  typedef Dune::FieldMatrix<double,BS,BS> MatrixBlock;
  typedef Dune::BCRSMatrix<MatrixBlock> BCRSMat;
  
  typedef Dune::FieldVector<double,BS> VectorBlock;
  typedef Dune::BlockVector<VectorBlock> Vector;

  typedef Dune::MatrixAdapter<BCRSMat,Vector,Vector> Operator;

  // thread pool
  typedef Dune::ThreadPool ThreadBackend;

  // custom allocator types
  typedef Dune::ParAllocator<MatrixBlock> MatAllocator;
  typedef Dune::ParAllocator<VectorBlock> VecAllocator;

  typedef Dune::BlockVector<VectorBlock, VecAllocator> CustomAllocVec;
  typedef Dune::BCRSMatrix<MatrixBlock, MatAllocator> CustomAllocMat;

  // typedef Dune::ParMatrixAdapter<AllocMat, Vector, Vector, ThreadBackend> AllocOperator;

  typedef Parallelton<ThreadBackend> ParalleltonTP;

  // hwloc setup
  int err;
  /* create a topology */
  err = hwloc_topology_init(&ParalleltonTP::topology);
  if (err < 0) {
    fprintf(stderr, "failed to initialize the topology\n");
  }
  // /* filter everything out */
  // hwloc_topology_set_all_types_filter(topology, HWLOC_TYPE_FILTER_KEEP_NONE);
  // /* filter Cores back in */
  // hwloc_topology_set_type_filter(topology, HWLOC_OBJ_CORE, HWLOC_TYPE_FILTER_KEEP_ALL);
  err = hwloc_topology_load(ParalleltonTP::topology);
  if (err < 0) {
    fprintf(stderr, "failed to load the topology\n");
    hwloc_topology_destroy(ParalleltonTP::topology);
  }


  // std::vector<size_t> logindices{0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40,42,44,46,48,50,52,54,56,58,60,62,
  //                                 64,66,68,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,
  //                                 128, 130, 132, 134, 136, 138, 140, 142, 144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 
  //                                 174, 176, 178, 180, 182, 184, 186, 188, 190, 192, 194, 196, 198, 200, 202, 204, 206, 208, 210, 212, 214, 216, 218, 220, 
  //                                 222, 224, 226, 228, 230, 232, 234, 236, 238, 240, 242, 244, 246, 248, 250, 252, 254};

  // std::vector<size_t> logindices{ 64,66,68,70,72,74,76,78,80,82,84,86,88,90,92,94,96,98,100,102,104,106,108,110,112,114,116,118,120,122,124,126,
  //                                 192, 194, 196, 198, 200, 202, 204, 206, 208, 210, 212, 214, 216, 218, 220, 
  //                                 222, 224, 226, 228, 230, 232, 234, 236, 238, 240, 242, 244, 246, 248, 250, 252, 254};

  std::vector<size_t> logindices{0,2,4,6,16,18,20,22,32,34,36,38,48,50,52,54,
                                 8,10,12,14,24,26,28,30,40,42,44,46,56,58,60,62,

                                 64,66,68,70,80,82,84,86,96,98,100,102,112,114,116,118,
                                 72,74,76,78,88,90,92,94,104,106,108,110,120,122,124,126,

                                  128, 130, 132, 134, 144, 146, 148, 150, 160, 162, 164, 166, 176, 178, 180, 182,
                                  136, 138, 140, 142, 152, 154, 156, 158, 168, 170, 172, 174, 184, 186, 188, 190,

                                  192, 194, 196, 198, 208, 210, 212, 214, 224, 226, 228, 230, 240, 242, 244, 246, 
                                  200, 202, 204, 206, 216, 218, 220, 222, 232, 234, 236, 238, 248, 250, 252, 254};


  auto binding = [](size_t threadid, size_t rank, size_t nbthreads)
  {
    // return (rank * 8ul + threadid) * 2ul;// + rank/2ul * 48ul * 2ul;
    // return rank*2ul + rank/32ul * 32ul * 2ul;
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // // 8 processes seperated on one cpu
    // if (nbthreads == 1)
    //   return rank * 8;// + rank/4 * 32;
    // else if (size == 1)
    //   return threadid * 8;// + threadid/4 * 32;

     if (nbthreads == 1)
       return rank;// + rank/32ul*64ul;
     else if (size == 1)
       return threadid;// + threadid/32ul*64ul;
     else
       return (rank * 8ul + threadid);// + rank/4ul*64ul;

    // // for separated procs
    if (nbthreads == 1)
    {
      auto procsperchiplet = size/16ul;
      auto whichchiplet = rank/procsperchiplet;
      auto idinchiplet = rank%procsperchiplet;

      return (idinchiplet + whichchiplet * 8ul);// + rank/4ul * 128ul;
    }
    else if (size == 1)
    {
      auto procsperchiplet = nbthreads/16ul;
      auto whichchiplet = threadid/procsperchiplet;
      auto idinchiplet = threadid%procsperchiplet;

      return (idinchiplet + whichchiplet * 8ul);// + threadid/4ul * 128ul;
    }
    else
    {
      return (rank * 8ul + threadid) + 0ul;
    }
  };
  


  int rank, size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // 2D
  std::vector<size_t> problemsizes {128, 256, 384, 504, 640, 768, 896, 1016, 1280, 1408, 1792, 2040, 2560, 3072, 4104, 5120, 8192, 15360/*, 30720, 51200*/};
  // 3D
  // std::vector<size_t> problemsizes {16, 32, 56, 64, 72, 96, 112, 128, 144, 160, 192, 224, 264, 320, 384, 448, 520/*, 640, 768, 896, 1032, 1152, 1280*/};

  
  // std::vector<size_t> ps2D{/*128,256,512,1024,1280,1536,*/2048/*,2560,3072/*,3584/*,4096/*,6144,8192,16384,32768*/};
  // std::vector<size_t> ps3D{32,64,96,128,256,512/*,768*/};

  // // 2D Seq
  // // for (auto i : problemsizes)
  // {
  //   auto i = N;
  //   SeqExperiment<BCRSMat, Vector> e(i, iter);
  //   // warm up
  //   auto d = time_experiment(e, 60 * 1e6);
  //   // actual measurement
  //   // d = time_experiment(e);

  //   if(rank == 0)
  //     std::cout << d.second/d.first;
  // }

  // // 3D Seq
  // {
  //   Seq3DExperiment<BCRSMat, Vector> e(N, iter);
  //   // warm up
  //   auto d = time_experiment(e, 10 * 1e6);
  //   // actual measurement
  //   // d = time_experiment(e);

  //   if(rank == 0)
  //     std::cout << d.second/d.first;
  // }

  // hwloc_topology_destroy(ParalleltonTP::topology);
  // MPI_Finalize();
  // return 0;  

  // // 2D
  // if(rank == 0)
  //   std::cout << "2D" << std::endl;

  // // for (auto i : problemsizes)
  // {
  //   // create threadpool
  //   auto context = std::make_shared<ThreadBackend>(threadcount);
  //   ParalleltonTP::context = context;
  //   context->setupneighbors<2>(N, true);
  //   ParalleltonTP::lambda = binding;

  //   // bind threads
  //   ParalleltonTP::context->bindthreads(binding);
  //   auto i = N;
  //   // give parallelton N
  //   ParalleltonTP::N = i;
  //   auto lowest = 100'000.0;
  //   auto rep = 1;
  //   // if (i > 2040)
  //   //   rep = 1;

  //   for (auto j = 0; j < rep; j++)
  //   { 
  //     Par_2D_Experiment<ThreadBackend, CustomAllocMat, CustomAllocVec> e(i, iter);
  //     // warm up 
  //     auto d = time_experiment(e, 10 * 1e6);
  //     // measurement
  //     d = time_experiment(e);

  //     if (d.second/d.first < lowest)
  //       lowest = d.second/d.first;
  //   }
    
  //   if(rank == 0)
  //     std::cout << lowest << ", " << std::endl;
  // }

  // if(rank == 0)
  //   std::cout << std::endl;

  // hwloc_topology_destroy(ParalleltonTP::topology);
  // MPI_Finalize();
  // return 0;

  // // 2D non-blocking
  // if(rank == 0)
  //   std::cout << "2D non blocking" << std::endl;

  // // for (auto i : problemsizes)
  // {
  //   auto i = N;
  //   // give parallelton N
  //   ParalleltonTP::N = i;
  //   auto lowest = 100'000.0;
  //   auto rep = 1;
  //   // if (i > 2040)
  //   //   rep = 1;

  //   for (auto j = 0; j < rep; j++)
  //   { 
  //     IPar_2D_Experiment<ThreadBackend, CustomAllocMat, CustomAllocVec> e(i, iter);
  //     // warm up 
  //     auto d = time_experiment(e);
  //     // measurement
  //     d = time_experiment(e);

  //     if (d.second/d.first < lowest)
  //       lowest = d.second/d.first;
  //   }
    
  //   if(rank == 0)
  //     std::cout << lowest << ", " << std::endl;
  // }

  // if(rank == 0)
  //   std::cout << std::endl;

  // hwloc_topology_destroy(ParalleltonTP::topology);
  // MPI_Finalize();
  // return 0;

  // 3D
  if(rank == 0)
    std::cout << "3D" << std::endl;

  // for (auto i : problemsizes)
  {
    // create threadpool
    auto context = std::make_shared<ThreadBackend>(threadcount);
    ParalleltonTP::context = context;
    context->setupneighbors<3>(N, true);
    ParalleltonTP::lambda = binding;

    // bind threads
    ParalleltonTP::context->bindthreads(binding);

    auto i = N;
    // give parallelton N
    ParalleltonTP::N = i;
    auto lowest = 100'000.0;
    auto rep = 1;
    // if (i > 160)
    //   rep = 1;

    for (auto j = 0; j < rep; j++)
    { 
      Par_3D_Experiment<ThreadBackend, CustomAllocMat, CustomAllocVec> e(i, iter);
      // warm up
      auto d = time_experiment(e, 10 * 1e6);
      // measurement
      d = time_experiment(e);

      if (d.second/d.first < lowest)
        lowest = d.second/d.first;
    }  
    
    if(rank == 0)
      std::cout << lowest << ", " << std::endl;
  }
  if(rank == 0)
    std::cout << std::endl;

  hwloc_topology_destroy(ParalleltonTP::topology);
  MPI_Finalize();
  return 0;

  // // 3D non-blocking
  // for (auto N : ps3D)
  // { 
  //   IPar_3D_Experiment<ThreadBackend, AllocMat, Vector> e(N, iter);
  //   auto d = time_experiment(e);

  //   if(rank == 0)
  //     // std::cout << "\nI3D Par_Exp N=" << N << std::endl;
  //     std::cout << "\n3D IPar_Exp N=" << N << " took " << d.second << "s with " << d.first << " iterations" << " = " << d.second/d.first << std::endl;
  // }


  // custom allocator types
  typedef Dune::MtBlockVectorAllocator<VectorBlock> BlockVecAllocator;
  typedef Dune::BlockVector<VectorBlock, BlockVecAllocator> BlockCustomAllocVec;

  // if(rank == 0)
  //   std::cout << "2D Blocked" << std::endl;

  // // for (auto i : problemsizes)
  // {
  //   // create threadpool
  //   auto context = std::make_shared<ThreadBackend>(threadcount);
  //   ParalleltonTP::context = context;
  //   context->setupneighbors<2>(N, false);
  //   ParalleltonTP::lambda = binding;

  //   // bind threads
  //   ParalleltonTP::context->bindthreads(binding);

  //   auto i = N;
  //   // give parallelton N
  //   ParalleltonTP::N = i;
  //   auto lowest = 100'000.0;
  //   auto rep = 1;
  //   // if (i > 2040)
  //   //   rep = 1;
    
  //   for (auto j = 0; j < rep; j++)
  //   {   
  //     Par_2D_Experiment_BlockedMT<ThreadBackend, CustomAllocMat, BlockCustomAllocVec> e(i, iter);
  //     // warm up 
  //     auto d = time_experiment(e, 10 * 1e6);
  //     // measurement
  //     d = time_experiment(e);

  //     if (d.second/d.first < lowest)
  //       lowest = d.second/d.first;
  //   }

  //   if(rank == 0)
  //     std::cout << lowest << ", " << std::endl;
  // }
  // if(rank == 0)
  //   std::cout << std::endl;

  // hwloc_topology_destroy(ParalleltonTP::topology);
  // MPI_Finalize();
  // return 0;

  if(rank == 0)
    std::cout << "3D Blocked" << std::endl;
  
  // for (auto i : problemsizes)
  { 
    // create threadpool
    auto context = std::make_shared<ThreadBackend>(threadcount);
    ParalleltonTP::context = context;
    context->setupneighbors<3>(N, false);
    ParalleltonTP::lambda = binding;

    // bind threads
    ParalleltonTP::context->bindthreads(binding);

    auto i = N;
    // give parallelton N
    ParalleltonTP::N = i;
    auto lowest = 100'000.0;
    auto rep = 2;
    if (i > 160)
      rep = 1;

    for (auto j = 0; j < rep; j++)
    { 
      Par_3D_Experiment_BlockedMT<ThreadBackend, CustomAllocMat, BlockCustomAllocVec> e(i, iter);
      // warm up
      auto d = time_experiment(e, 10 * 1e6);
      // measurement
      d = time_experiment(e);
      if (d.second/d.first < lowest)
        lowest = d.second/d.first;
    }  
    
    if(rank == 0)
      std::cout << lowest << ", " << std::endl;
  }
  if(rank == 0)
    std::cout << std::endl;

  hwloc_topology_destroy(ParalleltonTP::topology);
  MPI_Finalize();
  return 0;
}
