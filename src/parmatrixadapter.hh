#ifndef DUNE_PAR_MATRIX_ADAPTER_HH
#define DUNE_PAR_MATRIX_ADAPTER_HH

#include <dune/common/exceptions.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvercategory.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <thread>
#include "parallelton.hh"
#include "parcommunicator.hh"

namespace Dune {

    template<class M, class X, class Y, class ThreadBackend = ThreadPool>
    class ParMatrixAdapter : public AssembledLinearOperator<M,X,Y>
    {
    public:
        //! export types
        typedef M matrix_type;
        typedef X domain_type;
        typedef Y range_type;
        typedef typename X::field_type field_type; 

        //! constructor: just store a reference to a matrix
        explicit ParMatrixAdapter (const M& A, const ParData& p)
            : _A_(stackobject_to_shared_ptr(A)), pardata(stackobject_to_shared_ptr(p))
        {
            // setup variables used in every iteration
            context = Parallelton<ThreadBackend>::context;
            N = pardata->getN();
            size = pardata->getsize();
            nbrows = N*N / size;
            nbthreads = context->getnbthreads();
            stride = nbrows / nbthreads;
            pbx = pardata->getpbx();
            pby = pardata->getpby();
            // for non blocking comm
            setupborderindices();
            // auto nbthreads = Parallelton<ThreadBackend>::context->getnbthreads();
            // threadstarts.resize(nbthreads);
            // std::array<int, 2> dims;
            // dims.fill(0);
            // MPI_Dims_create(nbthreads, 2, &dims[0]);

            // // std::cout << "dim x: " << dims[0] << ", dim y: " << dims[1] << "\n";

            // Nx = pardata->getpbx();
            // auto Ny = pardata->getpby();
            
            // auto blockx = dims[0];
            // auto blocky = dims[1];
            // locpbx = Nx / blockx;
            // auto locpby = Ny / blocky;

            // for(auto i = 0ul; i < nbthreads; i++)
            // {
            //     auto x = i % dims[0];
            //     auto y = i / dims[0];

            //     // offset in y direction (y)
            //     auto start = y * Nx * locpby;
            //     // offset in x direction (x)
            //     start += x * locpbx;

            //     threadstarts[i] = start;
            // }

            // Parallelton<ThreadBackend>::Nx = Nx;
            // Parallelton<ThreadBackend>::locpbx = locpbx;
            // Parallelton<ThreadBackend>::threadstarts = threadstarts;

            // // std::cout << "threadstart:\n";
            // // for(auto i = 0ul; i < nbthreads; i++)
            // //     std::cout << i << ": " << threadstarts[i] << "\n";
        }

        //! constructor: store an std::shared_ptr to a matrix
        explicit ParMatrixAdapter (std::shared_ptr<const M> A, std::shared_ptr<const ParData> p)
            : _A_(A), pardata(p)
        {
            setupborderindices();
        }

        ~ParMatrixAdapter ()
        {
            // auto rank = pardata->getrank();
            // std::cout << "Rank=" << rank << " Time for applyouter=" << outerwatch << std::endl;
            // std::cout << "Rank=" << rank << " Time for applyinner=" << innerwatch << std::endl;

            // // print wait times of threads
            // for(auto i = 0; i < Parallelton<ThreadBackend>::context->getnbthreads(); i++)
            // {
            //     std::cout << "ParMatAdap Thread " << i << " waited " << waittimes[i] << "s\n";
            // }
        }

        void setupborderindices()
        {
            auto rank = pardata->getrank();
            auto nbrows = 2*pbx + 2*pby - 4;
            borderindices.reserve(nbrows);

            for(size_t i = 0; i < nbrows; i++)
            {
                // map i to borderindex
                bool lowrow = i / pbx;
                lowrow = 1 - lowrow;
                bool highrow = i / (pbx + 2*(pby-2));
                bool column = !lowrow && !highrow;

                auto lowrowcontrib = lowrow * i; // take first row directly
                auto highrowcontrib = highrow * (i + (pbx-2) * (pby-2)); // add all inner values to index
                auto columncontrib = column * (i + ((i - (pbx-1))/2 * (pbx-2))); // if we are at the right border add the inner values of that row
                
                auto borderindex = lowrowcontrib + highrowcontrib + columncontrib;
                borderindices.emplace_back(borderindex);
                // if(rank == 0) std::cout << borderindex << std::endl;

                // if (rank == 0) std::cout << "i=" << i << " borderindex=" << borderindex << "\n"
                //     << "1:" << lowrowcontrib << " 2:" << highrowcontrib << " 3:" << columncontrib << std::endl;
            }
        }


        void applyouter (const X& x, Y& y)
        {
            auto start = std::chrono::high_resolution_clock::now();

            auto pbx = pardata->getpbx();
            auto pby = pardata->getpby();
            auto context = Parallelton<ThreadBackend>::context;

            auto rank = pardata->getrank();

            // std::cout << "pbx=" << pbx << " pby=" << pby << " nbrows=" << nbrows << std::endl;
            auto f = [this,&x,&y,pbx,pby,rank](auto start, auto stride)
            {
                // // borderindices are unique and always increasing
                // auto iterstart = _A_->begin(); 
                // auto i = _A_->begin() + borderindices[start];
                // auto iend = _A_->begin() + borderindices[start + stride];

                // for(; i != iend; )
                // {

                //     auto currentind = i - iterstart;
                //     i += borderindices[currentind+1] - borderindices[currentind];
                // }

                for(size_t i = start; i < start + stride; i++)
                {
                    int borderindex = borderindices[i];
                    
                    auto iter = _A_->begin() + borderindex;

                    auto colindex = borderindex + borderindex/pbx*2 + (pbx+2)+1;
                    y[colindex] = 0;
                    // ConstColIterators
                    auto j = (*iter).begin();
                    auto endj = (*iter).end();
                    for(; j != endj; ++j)
                    {
                        auto&& xj = Impl::asVector(x[j.index()]);
                        // auto&& yi = Impl::asVector(y[i.index()]);
                        auto&& yi = Impl::asVector(y[colindex]);
                        Impl::asMatrix(*j).umv(xj, yi);                 
                    }
                }
            };
            auto N = pardata->getN();
            auto size = pardata->getsize();
            // one less thread because the last thread is the mpi progress thread
            auto nbthreads = context->getnbthreads();
            
            auto nbrows = 2*pbx + 2*pby - 4;
            auto stride = nbrows / nbthreads;
            auto rest = nbrows % nbthreads;
            // start threads
            for(size_t i = 0; i < nbthreads; i++)
            {
                auto current = i * stride;
                // for rest 
                if(i == nbthreads - 1)
                    stride += rest;
                context->addtask(f, i, current, stride);
            }
            // wait for threads
            context->waittasks();

            auto end = std::chrono::high_resolution_clock::now();
            outerwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }

        void applyinner (const X& x, Y& y)
        {
            auto start = std::chrono::high_resolution_clock::now();

            auto pbx = pardata->getpbx();
            auto pby = pardata->getpby();
            auto rank = pardata->getrank();
            auto context = Parallelton<ThreadBackend>::context;

            // lambda for every thread
            auto f = [this, &x, &y, pbx, rank](auto start, auto stride)
            {
                for(size_t i = start; i < start + stride; i++)
                {
                    auto innerindex = i + pbx+1 + i/(pbx-2) * 2;
                    // if (rank == 0) std::cout << "i=" << i << " innerindex=" << innerindex << std::endl;

                    auto iter = _A_->begin() + innerindex;

                    auto colindex = innerindex + innerindex/pbx*2 + (pbx+2)+1;
                    y[colindex] = 0;
                    // ConstColIterators
                    auto j = (*iter).begin();
                    auto endj = (*iter).end();
                    for(; j != endj; ++j)
                    {
                        auto&& xj = Impl::asVector(x[j.index()]);
                        // auto&& yi = Impl::asVector(y[i.index()]);
                        auto&& yi = Impl::asVector(y[colindex]);
                        Impl::asMatrix(*j).umv(xj, yi);                 
                    }
                }
            };

            auto N = pardata->getN();
            auto size = pardata->getsize();
            // one less thread because the last thread is the mpi progress thread
            auto threadnumber = context->getnbthreads();

            auto nbrows = N*N / size;
            auto nbinnerrows = nbrows - 2*pbx - 2*pby + 4;
            auto stride = nbinnerrows / threadnumber;
            auto rest = nbinnerrows % threadnumber;
            // start threads
            for(size_t i = 0; i < threadnumber; i++)
            {
                auto current = i * stride;
                // for rest 
                if(i == threadnumber - 1) 
                    stride += rest;
                context->addtask(f, i, current, stride);
            }
            // wait for threads
            context->waittasks();

            auto end = std::chrono::high_resolution_clock::now();
            innerwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }

        //! apply operator to x:  \f$ y = A(x) \f$
        void apply (const X& x, Y& y) const
        {
// #ifdef DUNE_ISTL_WITH_CHECKING
//       if (ready != built)
//         DUNE_THROW(BCRSMatrixError,"You can only call arithmetic operations on fully built BCRSMatrix instances");
//       if (x.N()!=M()) DUNE_THROW(BCRSMatrixError,"index out of range");
//       if (y.N()!=N()) DUNE_THROW(BCRSMatrixError,"index out of range");
// #endif
            // lambda for every thread
            auto f = [this, &x, &y](auto start, auto stride)
            {
                // ConstRowIterators
                auto i = _A_->begin() + start;
                auto iend = i + stride;
                for(; i != iend; ++i)
                {
                    // index for the vector
                    // index + between every row are 2 overlap variable + overlap in the first row
                    auto colindex = i.index() + i.index()/pbx*2 + (pbx+2)+1;
                    y[colindex] = 0;
                    // y[i.index()] = 0;
                    // ConstColIterators
                    auto j = (*i).begin();
                    auto endj = (*i).end();
                    for(; j != endj; ++j)
                    {
                        auto&& xj = Impl::asVector(x[j.index()]);
                        // auto&& yi = Impl::asVector(y[i.index()]);
                        auto&& yi = Impl::asVector(y[colindex]);
                        Impl::asMatrix(*j).umv(xj, yi);                 
                    }
                }
            };

            // auto rest = nbrows % nbthreads;
            // start threads
            for(size_t i = 0; i < nbthreads; i++)
            {
                auto current = i * stride;

                context->addtask(f, i, current, stride);
            }
            // wait for threads
            context->thread0task();
        }


        //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
        void applyscaleadd (field_type alpha, const X& x, Y& y) const override
        {
        }

        //! get matrix via *
        const M& getmat () const override
        {
            return *_A_;
        }

        //! Category of the solver (see SolverCategory::Category)
        SolverCategory::Category category() const override
        {
            return SolverCategory::sequential;
        }


    private:
        const std::shared_ptr<const M> _A_;
        const std::shared_ptr<const ParData> pardata;
        std::vector<size_t> borderindices;
        double innerwatch = 0;
        double outerwatch = 0;
        std::vector<size_t> threadstarts;
        // variables in each iteration
        size_t N;
        size_t size;
        size_t nbrows;
        size_t nbthreads;
        size_t stride;
        size_t pbx;
        size_t pby;
        std::shared_ptr<ThreadBackend> context;
    };




    template<class M, class X, class Y, class ThreadBackend = ThreadPool>
    class ParMatrixAdapter3D : public AssembledLinearOperator<M,X,Y>
    {
    public:
        //! export types
        typedef M matrix_type;
        typedef X domain_type;
        typedef Y range_type;
        typedef typename X::field_type field_type; 

        //! constructor: just store a reference to a matrix
        explicit ParMatrixAdapter3D (const M& A, const ParData& p)
            : _A_(stackobject_to_shared_ptr(A)), pardata(stackobject_to_shared_ptr(p))
        {
            setupborderindices();
            pbx = pardata->getpbx();
            pby = pardata->getpby();
            pbz = pardata->getpbz();
            context = Parallelton<ThreadBackend>::context;
            N = pardata->getN();
            size = pardata->getsize();
            nbrows = N*N*N / size;
            nbthreads = context->getnbthreads();
            stride = nbrows / nbthreads;
        }

        //! constructor: store an std::shared_ptr to a matrix
        explicit ParMatrixAdapter3D (std::shared_ptr<const M> A, std::shared_ptr<const ParData> p)
            : _A_(A), pardata(p)
        {
            setupborderindices();
            pbx = pardata->getpbx();
            pby = pardata->getpby();
            pbz = pardata->getpbz();
            context = Parallelton<ThreadBackend>::context;
            N = pardata->getN();
            size = pardata->getsize();
            nbrows = N*N*N / size;
            nbthreads = context->getnbthreads();
            stride = nbrows / nbthreads;
        }

        ~ParMatrixAdapter3D ()
        {
            // auto rank = pardata->getrank();
            // std::cout << "Rank=" << rank << " Time for applyouter=" << outerwatch << std::endl;
            // std::cout << "Rank=" << rank << " Time for applyinner=" << innerwatch << std::endl;
        }

        void setupborderindices()
        {
            auto pbx = pardata->getpbx();
            auto pby = pardata->getpby();
            auto pbz = pardata->getpbz();
            auto rank = pardata->getrank();
            size_t nbrows = 2*pbx*pby + (2*pby*pbz - 4*pby) + (2*pbx*pbz - 4*pbx - 4*(pbz-2));
            borderindices.reserve(nbrows);

            for(size_t i = 0; i < nbrows; i++)
            {
                auto layersize = pbx*pby;
                auto inbtwlsize = 2*pbx + 2*(pby-2);
                auto whichlayer = (i-layersize)/inbtwlsize; 
                
                bool firstlayer = i / (layersize);
                firstlayer = 1-firstlayer;
                bool lastlayer = i/(layersize + (pbz-2) * (inbtwlsize));
                bool betweenlayer = !firstlayer && !lastlayer;
                bool lowerrow = (1-firstlayer)*(1-lastlayer) * ((i-layersize)%inbtwlsize) / pbx;
                lowerrow = 1-lowerrow;
                bool highrow = (1-firstlayer)*(1-lastlayer) * ((i-layersize)%inbtwlsize) / (pbx + 2*(pby-2));
                bool rightcol = !lowerrow && !highrow;  

                auto firstlayercontrib = firstlayer * i; // first layer
                auto lastlayercontrib = lastlayer * (i + (pbz-2) * (pbx-2) * (pby-2)); // lastlayer 
                auto lowestrowcontrib = betweenlayer * lowerrow * (i + (whichlayer * (pbx-2) * (pby-2))); // lowest row of inbetween layer
                auto highestrowcontrib = betweenlayer * highrow * (i + ((1+whichlayer)  * (pbx-2) * (pby-2))); // highest row of inbetween layer
                auto columncontrib = betweenlayer * rightcol * ((i + ((i-layersize-(pbx-1) - (whichlayer*inbtwlsize))/2) * (pbx-2) + whichlayer*(pby-2)*(pbx-2)));   
                
                auto borderindex = firstlayercontrib + lastlayercontrib + lowestrowcontrib + highestrowcontrib + columncontrib; 

                // if (rank == 0) std::cout << "i=" << i << " bid=" << borderindex << " firstl=" << firstlayer
                // << " betwl=" << betweenlayer << " lastl=" << lastlayer << " which=" << whichlayer << "\n" <<
                // "1:" << firstlayercontrib << " 2:" << lastlayercontrib
                // << " 3:" << lowestrowcontrib << " 4:" << highestrowcontrib << " 5:" << columncontrib << std::endl;
                borderindices.emplace_back(borderindex);
            }
        }


        void applyouter (const X& x, Y& y)
        {
            auto start = std::chrono::high_resolution_clock::now();

            auto pbx = pardata->getpbx();
            auto pby = pardata->getpby();
            auto pbz = pardata->getpbz();
            auto rank = pardata->getrank();
            auto context = Parallelton<ThreadBackend>::context;

            auto f = [this,&x,&y,pbx,pby,pbz,rank](auto start, auto stride)
            {
                for(size_t i = start; i < start + stride; i++)
                {
                    auto borderindex = borderindices[i];
                    auto iter = _A_->begin() + borderindex;

                    size_t frontoverlap = (pbx+2) * (pby+2);
                    auto colindex = borderindex + borderindex/pbx * 2
                                    // if we clear one slide we go one layer into back direction (pos z)
                                    + borderindex/(pbx*pby) * (2*(pbx+2)) 
                                    // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                                    + frontoverlap+(pbx+2)+1;
                    y[colindex] = 0;
                    // ConstColIterators
                    auto j = (*iter).begin();
                    auto endj = (*iter).end();
                    for(; j != endj; ++j)
                    {
                        auto&& xj = Impl::asVector(x[j.index()]);
                        // auto&& yi = Impl::asVector(y[i.index()]);
                        auto&& yi = Impl::asVector(y[colindex]);
                        Impl::asMatrix(*j).umv(xj, yi);
                    }
                }
            };

            auto N = pardata->getN();
            auto size = pardata->getsize();
            auto threadnumber = context->getnbthreads();
            auto nbinnerrows = (pbx-2) * (pby-2) * (pbz-2);
            // size_t nbrows = 2*pbx*pby + (2*pby*pbz - 4*pby) + (2*pbx*pbz - 4*pbx - 4*(pbz-2));
            size_t nbrows = N*N*N/size - nbinnerrows;
            auto stride = nbrows / threadnumber;
            auto rest = nbrows % threadnumber;
            // start threads
            for(size_t i = 0; i < threadnumber; i++)
            {
                auto current = i * stride;
                // for rest 
                if(i == threadnumber - 1) 
                    stride += rest;
                context->addtask(f, i, current, stride);
            }
            // wait for threads
            context->waittasks();

            auto end = std::chrono::high_resolution_clock::now();
            outerwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();;
        }

        void applyinner (const X& x, Y& y)
        {
            auto start = std::chrono::high_resolution_clock::now();

            auto pbx = pardata->getpbx();
            auto pby = pardata->getpby();
            auto pbz = pardata->getpbz();
            auto rank = pardata->getrank();
            auto context = Parallelton<ThreadBackend>::context;

            // lambda for every thread
            auto f = [this, &x, &y, pbx, pby, pbz, rank](auto start, auto stride)
            {
                for(size_t i = start; i < start + stride; i++)
                {
                    //              skip front, lower border and one element of the left border
                    auto innerindex = i + pbx*pby + pbx+1 + 
                    //              once we complete one row we skip the left and right border to get to the next row
                                        i/(pbx-2) * 2 +
                    //              one we complete one layer we skip to the next (skip top layer, bottom layer)
                                        i/((pbx-2)*(pby-2)) * (2*pbx);
                    // if (rank == 0) std::cout << "i=" << i << " innerindex=" << innerindex << std::endl;
                    auto iter = _A_->begin() + innerindex;

                    size_t frontoverlap = (pbx+2) * (pby+2);
                    auto colindex = innerindex + innerindex/pbx * 2
                                    // if we clear one slide we go one layer into back direction (pos z)
                                    + innerindex/(pbx*pby) * (2*(pbx+2)) 
                                    // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                                    + frontoverlap+(pbx+2)+1;
                    y[colindex] = 0;
                    // ConstColIterators
                    auto j = (*iter).begin();
                    auto endj = (*iter).end();
                    for(; j != endj; ++j)
                    {
                        auto&& xj = Impl::asVector(x[j.index()]);
                        // auto&& yi = Impl::asVector(y[i.index()]);
                        auto&& yi = Impl::asVector(y[colindex]);
                        Impl::asMatrix(*j).umv(xj, yi);
                    }
                }
            };
            auto N = pardata->getN();
            auto size = pardata->getsize();
            auto threadnumber = context->getnbthreads();

            auto nbinnerrows = (pbx-2) * (pby-2) * (pbz-2);
            // if (rank == 0) std::cout << "nbrows=" << nbrows << " nbinnerrows=" << nbinnerrows << std::endl;
            auto stride = nbinnerrows / threadnumber;
            auto rest = nbinnerrows % threadnumber;
            // start threads
            for(size_t i = 0; i < threadnumber; i++)
            {
                auto current = i * stride;
                // for rest 
                if(i == threadnumber - 1) 
                    stride += rest;
                context->addtask(f, i, current, stride);
            }
            // wait for threads
            context->waittasks();

            auto end = std::chrono::high_resolution_clock::now();
            innerwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }

        //! apply operator to x:  \f$ y = A(x) \f$
        void apply (const X& x, Y& y) const
        {
// #ifdef DUNE_ISTL_WITH_CHECKING
//       if (ready != built)
//         DUNE_THROW(BCRSMatrixError,"You can only call arithmetic operations on fully built BCRSMatrix instances");
//       if (x.N()!=M()) DUNE_THROW(BCRSMatrixError,"index out of range");
//       if (y.N()!=N()) DUNE_THROW(BCRSMatrixError,"index out of range");
// #endif

            // lambda for every thread
            auto f = [this, &x, &y](auto start, auto stride)
            {
                // ConstRowIterators
                auto i = _A_->begin() + start;
                auto iend = i + stride;

                // size_t frontoverlap = (pbx+2) * (pby+2);
                // size_t iindex = i.index() + i.index()/pbx * 2
                //                     // if we clear one slide we go one layer into back direction (pos z)
                //                     + i.index()/(pbx*pby) * (2*(pbx+2)) 
                //                     // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                //                     + frontoverlap+(pbx+2)+1;

                // // in which row and column does this thread start
                // size_t startx = i.index() % pbx;

                // size_t elementsleft = stride;
                // size_t elementstoproc = pbx;

                // if (elementsleft < pbx - startx)
                //     elementstoproc = elementsleft + startx;

                // // clear first row (which might not be fully under out control)
                // for (auto l = startx; l < elementstoproc; l++)
                // {
                //     y[iindex] = 0;
                //     // ConstColIterators
                //     auto j = (*i).begin();
                //     auto endj = (*i).end();
                //     for(; j != endj; ++j)
                //     {
                //         auto&& xj = Impl::asVector(x[j.index()]);
                //         // auto&& yi = Impl::asVector(y[i.index()]);
                //         auto&& yi = Impl::asVector(y[iindex]);
                //         Impl::asMatrix(*j).umv(xj, yi);
                //     }   
                //     iindex++; ++i;
                // }
                // elementsleft -= elementstoproc;
                // // one row completed   
                // iindex += 2;

                // // one layer completed 
                // if (i.index() % (pbx*pby) == 0)
                //     iindex += 2 * (pbx+2);


                // // start calculation of all elements that are left over
                // for (; i != iend; )
                // {
                //     if (elementsleft < pbx)
                //         elementstoproc = elementsleft;

                //     for (auto l = 0ul; l < elementstoproc; l++)
                //     {
                //         y[iindex] = 0;
                //         // ConstColIterators
                //         auto j = (*i).begin();
                //         auto endj = (*i).end();
                //         for(; j != endj; ++j)
                //         {
                //             auto&& xj = Impl::asVector(x[j.index()]);
                //             // auto&& yi = Impl::asVector(y[i.index()]);
                //             auto&& yi = Impl::asVector(y[iindex]);
                //             Impl::asMatrix(*j).umv(xj, yi);
                //         }   
                //         iindex++; ++i;
                //     }
                //     elementsleft -= elementstoproc;

                //     // one row completed   
                //     iindex += 2;

                //     // one layer completed 
                //     if (i.index() % (pbx*pby) == 0)
                //         iindex += 2 * (pbx+2);   
                // }

                size_t frontoverlap = (pbx+2) * (pby+2);

                for(; i != iend; ++i)
                {
                    // index for the vector
                    auto colindex = i.index() + i.index()/pbx * 2
                                    // if we clear one slide we go one layer into back direction (pos z)
                                    + i.index()/(pbx*pby) * (2*(pbx+2)) 
                                    // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                                    + frontoverlap+(pbx+2)+1;
                    y[colindex] = 0;
                    // ConstColIterators
                    auto j = (*i).begin();
                    auto endj = (*i).end();
                    for(; j != endj; ++j)
                    {
                        auto&& xj = Impl::asVector(x[j.index()]);
                        // auto&& yi = Impl::asVector(y[i.index()]);
                        auto&& yi = Impl::asVector(y[colindex]);
                        Impl::asMatrix(*j).umv(xj, yi);
                    }
                    // std::cout << std::endl;
                }
            };

            // start threads
            for(size_t i = 0; i < nbthreads; i++)
            {
                auto current = i * stride;

                context->addtask(f, i, current, stride);
            }
            // wait for threads
            context->thread0task();
        }


        //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
        void applyscaleadd (field_type alpha, const X& x, Y& y) const override
        {
        }

        //! get matrix via *
        const M& getmat () const override
        {
            return *_A_;
        }

        //! Category of the solver (see SolverCategory::Category)
        SolverCategory::Category category() const override
        {
            return SolverCategory::sequential;
        }


    private:
        const std::shared_ptr<const M> _A_;
        const std::shared_ptr<const ParData> pardata;
        std::vector<size_t> borderindices;
        double innerwatch = 0;
        double outerwatch = 0;

        size_t pbx;
        size_t pby;
        size_t pbz;
        std::shared_ptr<ThreadBackend> context;
        size_t N;
        size_t size;
        size_t nbrows;
        size_t nbthreads;
        size_t stride;
    };
}



#endif
