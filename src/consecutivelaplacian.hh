#ifndef DUNE_CONSECUTIVE_LAPLACIAN
#define DUNE_CONSECUTIVE_LAPLACIAN
#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/scalarmatrixview.hh>
#include <algorithm>
#include "parcommunicator.hh"

template<class B, class Alloc>
void setupConsecutiveSparsity(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc> Matrix;

  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto blockx = pardata.getblockx();
  auto size = pardata.getsize();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();
  auto nbrows = N*N / size;
  auto nbcols = (pbx+2) * (pby+2);

  A.setSize(nbrows, nbcols, nbrows*5ul);
  A.setBuildMode(Matrix::row_wise);

  auto nbinner = (pbx-2) * (pby-2);
  auto nbouter = nbrows - nbinner;
  for (typename Dune::BCRSMatrix<B,Alloc>::CreateIterator i = A.createbegin(); i != A.createend(); ++i) {

    size_t x;
    size_t y;
    size_t new_index;
    size_t overlap_index;

    // first insert the outer rows
    if(i.index() < nbouter)
    {
        // map i to borderindex
        bool lowrow = i.index() / pbx;
        lowrow = 1 - lowrow;
        bool highrow = i.index() / (pbx + 2*(pby-2));
        bool column = !lowrow && !highrow;

        auto lowrowcontrib = lowrow * i.index(); // take first row directly
        auto highrowcontrib = highrow * (i.index() + (pbx-2) * (pby-2)); // add all inner values to index
        auto columncontrib = column * (i.index() + ((i.index() - (pbx-1))/2 * (pbx-2))); // if we are at the right border add the inner values of that row
        
        auto borderindex = lowrowcontrib + highrowcontrib + columncontrib;

        new_index = start + borderindex%pbx + borderindex/pbx * N;
        overlap_index = borderindex + borderindex/pbx*2 + (pbx+2)+1;
    }
    // then inner
    else
    {
        auto inneri = i.index() - nbouter;
        auto innerindex = inneri + pbx+1 + inneri/(pbx-2) * 2;

        new_index = start + innerindex%pbx + innerindex/pbx * N;
        overlap_index = innerindex + innerindex/pbx*2 + (pbx+2)+1;
    }

    x = new_index%N;
    y = new_index/N;


    if(y>0)
      // insert lower neighbour
      i.insert(overlap_index - (pbx+2));

    if(x>0)
      // insert left neighbour
      i.insert(overlap_index-1);

    // insert diagonal value
    i.insert(overlap_index);
    
    if(x<N-1)
      //insert right neighbour
      i.insert(overlap_index+1);
      
    if(y<N-1)
      // insert upper neighbour
      i.insert(overlap_index + pbx+2);
  }
}


template<class B, class Alloc>
void setupConsecutiveLaplacian(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc>::field_type FieldType;

  setupConsecutiveSparsity(A,N,pardata);

  B diagonal(static_cast<FieldType>(0)), bone(static_cast<FieldType>(0));

  auto setDiagonal = [](auto&& scalarOrMatrix, const auto& value) {
    auto&& matrix = Dune::Impl::asMatrix(scalarOrMatrix);
    for (auto rowIt = matrix.begin(); rowIt != matrix.end(); ++rowIt)
      (*rowIt)[rowIt.index()] = value;
  };

  setDiagonal(diagonal, 4.0);
  setDiagonal(bone, -1.0);

  auto nbrows = N*N / pardata.getsize();
  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto blockx = pardata.getblockx();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();

  auto nbinner = (pbx-2) * (pby-2);
  auto nbouter = nbrows - nbinner;
  for (typename Dune::BCRSMatrix<B,Alloc>::RowIterator i = A.begin(); i != A.end(); ++i) {
    size_t x;
    size_t y;
    size_t new_index;
    size_t overlap_index;

    // first insert the outer rows
    if(i.index() < nbouter)
    {
        // map i to borderindex
        bool lowrow = i.index() / pbx;
        lowrow = 1 - lowrow;
        bool highrow = i.index() / (pbx + 2*(pby-2));
        bool column = !lowrow && !highrow;

        auto lowrowcontrib = lowrow * i.index(); // take first row directly
        auto highrowcontrib = highrow * (i.index() + (pbx-2) * (pby-2)); // add all inner values to index
        auto columncontrib = column * (i.index() + ((i.index() - (pbx-1))/2 * (pbx-2))); // if we are at the right border add the inner values of that row
        
        auto borderindex = lowrowcontrib + highrowcontrib + columncontrib;

        new_index = start + borderindex%pbx + borderindex/pbx * N;
        overlap_index = borderindex + borderindex/pbx*2 + (pbx+2)+1;
    }
    // then inner
    else
    {
        auto inneri = i.index() - nbouter;
        auto innerindex = inneri + pbx+1 + inneri/(pbx-2) * 2;

        new_index = start + innerindex%pbx + innerindex/pbx * N;
        overlap_index = innerindex + innerindex/pbx*2 + (pbx+2)+1;
    }

    x = new_index%N;
    y = new_index/N;

    i->operator[](overlap_index)=diagonal;

    if(y>0)
        i->operator[](overlap_index - (pbx+2))=bone;
    
    if(y<N-1)
        i->operator[](overlap_index + pbx+2)=bone;
    
    if(x>0)
        i->operator[](overlap_index-1)=bone;

    if(x < N-1)
        i->operator[](overlap_index+1)=bone;
    
    }
}









template<class B, class Alloc>
void setupConsecutive3DSparsity(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc> Matrix;

  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto pbz = pardata.getpbz();
  auto blockx = pardata.getblockx();
  auto size = pardata.getsize();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();

  auto nbrows = N*N*N / size;
  auto nbcols = (pbx+2) * (pby+2) * (pbz+2);

  A.setSize(nbrows, nbcols, nbrows*7ul);
  A.setBuildMode(Matrix::row_wise);

  auto nbinner = (pbx-2) * (pby-2) * (pbz-2);
  auto nbouter = nbrows - nbinner;
  for (typename Dune::BCRSMatrix<B,Alloc>::CreateIterator i = A.createbegin(); i != A.createend(); ++i) {

    size_t new_index;
    size_t x, y, z;
    size_t overlap_index;

    if(i.index() < nbouter)
    {
        auto layersize = pbx*pby;
        auto inbtwlsize = 2*pbx + 2*(pby-2);
        auto whichlayer = (i.index()-layersize)/inbtwlsize; 
        
        bool firstlayer = i.index() / (layersize);
        firstlayer = 1-firstlayer;
        bool lastlayer = i.index()/(layersize + (pbz-2) * (inbtwlsize));
        bool betweenlayer = !firstlayer && !lastlayer;
        bool lowerrow = (1-firstlayer)*(1-lastlayer) * ((i.index()-layersize)%inbtwlsize) / pbx;
        lowerrow = 1-lowerrow;
        bool highrow = (1-firstlayer)*(1-lastlayer) * ((i.index()-layersize)%inbtwlsize) / (pbx + 2*(pby-2));
        bool rightcol = !lowerrow && !highrow;  

        auto firstlayercontrib = firstlayer * i.index(); // first layer
        auto lastlayercontrib = lastlayer * (i.index() + (pbz-2) * (pbx-2) * (pby-2)); // lastlayer 
        auto lowestrowcontrib = betweenlayer * lowerrow * (i.index() + (whichlayer * (pbx-2) * (pby-2))); // lowest row of inbetween layer
        auto highestrowcontrib = betweenlayer * highrow * (i.index() + ((1+whichlayer)  * (pbx-2) * (pby-2))); // highest row of inbetween layer
        auto columncontrib = betweenlayer * rightcol * ((i.index() + ((i.index()-layersize-(pbx-1) - (whichlayer*inbtwlsize))/2) * (pbx-2) + whichlayer*(pby-2)*(pbx-2)));
        
        auto borderindex = firstlayercontrib + lastlayercontrib + lowestrowcontrib + highestrowcontrib + columncontrib; 


        // index of where we would be currently if we were working with a complete matrix
                                // x contribution
        new_index = start + borderindex%pbx + 
                                // y contribution    subtract the y contribution if we start a new layer in z direction
                                borderindex/pbx * N - borderindex/(pbx*pby) * N*pby +
                                // z contribution
                                borderindex/(pbx*pby) * N*N; 

        // helping variables
        size_t frontoverlap = (pbx+2) * (pby+2);

        // with this index we only want to iterate over the "core" of our values stored on this rank
        // basically we get the index of the "core" within the vector with overlap
        //                               if we go one level higher skip overlap on the sides
        overlap_index = borderindex + borderindex/pbx * 2
                            // if we clear one slide we go one layer into back direction (pos z)
                            + borderindex/(pbx*pby) * (2*(pbx+2)) 
                            // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                            + frontoverlap+(pbx+2)+1;
    }
    else
    {
        auto inneri = i.index() - nbouter;
        //              skip front, lower border and one element of the left border
        auto innerindex = inneri + pbx*pby + pbx+1 + 
        //              once we complete one row we skip the left and right border to get to the next row
                            inneri/(pbx-2) * 2 +
        //              one we complete one layer we skip to the next (skip top layer, bottom layer)
                            inneri/((pbx-2)*(pby-2)) * (2*pbx);

        new_index = start + innerindex%pbx + 
                                // y contribution    subtract the y contribution if we start a new layer in z direction
                                innerindex/pbx * N - innerindex/(pbx*pby) * N*pby +
                                // z contribution
                                innerindex/(pbx*pby) * N*N; 

        // helping variables
        size_t frontoverlap = (pbx+2) * (pby+2);

        // with this index we only want to iterate over the "core" of our values stored on this rank
        // basically we get the index of the "core" within the vector with overlap
        //                               if we go one level higher skip overlap on the sides
        overlap_index = innerindex + innerindex/pbx * 2
                            // if we clear one slide we go one layer into back direction (pos z)
                            + innerindex/(pbx*pby) * (2*(pbx+2)) 
                            // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                            + frontoverlap+(pbx+2)+1;
    }

    // 3D Coordinates of where we are in the original grid
    x = new_index%N;
    y = new_index/N - new_index/(N*N) * N;
    z = new_index/(N*N);

    if(y>0)
      // insert lower neighbour
      i.insert(overlap_index - (pbx+2));

    if(x>0)
      // insert left neighbour
      i.insert(overlap_index-1);

    if(z>0)
      // insert front neighbour
      i.insert(overlap_index - ((pbx+2) * (pby+2)));

    // insert diagonal value
    i.insert(overlap_index);

    if(x<N-1)
      //insert right neighbour
      i.insert(overlap_index+1);
      
    if(y<N-1)
      // insert upper neighbour
      i.insert(overlap_index + pbx+2);

    if(z<N-1)
    // insert back neighbour
      i.insert(overlap_index + ((pbx+2) * (pby+2)));
  
  }
}


template<class B, class Alloc>
void setupConsecutive3DLaplacian(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc>::field_type FieldType;

  setupConsecutive3DSparsity(A,N,pardata);

  B diagonal(static_cast<FieldType>(0)), bone(static_cast<FieldType>(0));

  auto setDiagonal = [](auto&& scalarOrMatrix, const auto& value) {
    auto&& matrix = Dune::Impl::asMatrix(scalarOrMatrix);
    for (auto rowIt = matrix.begin(); rowIt != matrix.end(); ++rowIt)
      (*rowIt)[rowIt.index()] = value;
  };

  setDiagonal(diagonal, 4.0);
  setDiagonal(bone, -1.0);

  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto pbz = pardata.getpbz();
  auto blockx = pardata.getblockx();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();
  auto nbrows = N*N*N / pardata.getsize();

  auto nbinner = (pbx-2) * (pby-2) * (pbz-2);
  auto nbouter = nbrows - nbinner;
  for (typename Dune::BCRSMatrix<B,Alloc>::RowIterator i = A.begin(); i != A.end(); ++i) {
    size_t new_index;
    size_t x, y, z;
    size_t overlap_index;

    if(i.index() < nbouter)
    {
        auto layersize = pbx*pby;
        auto inbtwlsize = 2*pbx + 2*(pby-2);
        auto whichlayer = (i.index()-layersize)/inbtwlsize; 
        
        bool firstlayer = i.index() / (layersize);
        firstlayer = 1-firstlayer;
        bool lastlayer = i.index()/(layersize + (pbz-2) * (inbtwlsize));
        bool betweenlayer = !firstlayer && !lastlayer;
        bool lowerrow = (1-firstlayer)*(1-lastlayer) * ((i.index()-layersize)%inbtwlsize) / pbx;
        lowerrow = 1-lowerrow;
        bool highrow = (1-firstlayer)*(1-lastlayer) * ((i.index()-layersize)%inbtwlsize) / (pbx + 2*(pby-2));
        bool rightcol = !lowerrow && !highrow;  

        auto firstlayercontrib = firstlayer * i.index(); // first layer
        auto lastlayercontrib = lastlayer * (i.index() + (pbz-2) * (pbx-2) * (pby-2)); // lastlayer 
        auto lowestrowcontrib = betweenlayer * lowerrow * (i.index() + (whichlayer * (pbx-2) * (pby-2))); // lowest row of inbetween layer
        auto highestrowcontrib = betweenlayer * highrow * (i.index() + ((1+whichlayer)  * (pbx-2) * (pby-2))); // highest row of inbetween layer
        auto columncontrib = betweenlayer * rightcol * ((i.index() + ((i.index()-layersize-(pbx-1) - (whichlayer*inbtwlsize))/2) * (pbx-2) + whichlayer*(pby-2)*(pbx-2)));
        
        auto borderindex = firstlayercontrib + lastlayercontrib + lowestrowcontrib + highestrowcontrib + columncontrib; 


        // index of where we would be currently if we were working with a complete matrix
                                // x contribution
        new_index = start + borderindex%pbx + 
                                // y contribution    subtract the y contribution if we start a new layer in z direction
                                borderindex/pbx * N - borderindex/(pbx*pby) * N*pby +
                                // z contribution
                                borderindex/(pbx*pby) * N*N; 

        // helping variables
        size_t frontoverlap = (pbx+2) * (pby+2);

        // with this index we only want to iterate over the "core" of our values stored on this rank
        // basically we get the index of the "core" within the vector with overlap
        //                               if we go one level higher skip overlap on the sides
        overlap_index = borderindex + borderindex/pbx * 2
                            // if we clear one slide we go one layer into back direction (pos z)
                            + borderindex/(pbx*pby) * (2*(pbx+2)) 
                            // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                            + frontoverlap+(pbx+2)+1;
    }
    else
    {
        auto inneri = i.index() - nbouter;
        //              skip front, lower border and one element of the left border
        auto innerindex = inneri + pbx*pby + pbx+1 + 
        //              once we complete one row we skip the left and right border to get to the next row
                            inneri/(pbx-2) * 2 +
        //              one we complete one layer we skip to the next (skip top layer, bottom layer)
                            inneri/((pbx-2)*(pby-2)) * (2*pbx);

        new_index = start + innerindex%pbx + 
                                // y contribution    subtract the y contribution if we start a new layer in z direction
                                innerindex/pbx * N - innerindex/(pbx*pby) * N*pby +
                                // z contribution
                                innerindex/(pbx*pby) * N*N; 

        // helping variables
        size_t frontoverlap = (pbx+2) * (pby+2);

        // with this index we only want to iterate over the "core" of our values stored on this rank
        // basically we get the index of the "core" within the vector with overlap
        //                               if we go one level higher skip overlap on the sides
        overlap_index = innerindex + innerindex/pbx * 2
                            // if we clear one slide we go one layer into back direction (pos z)
                            + innerindex/(pbx*pby) * (2*(pbx+2)) 
                            // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                            + frontoverlap+(pbx+2)+1;
    }

    // 3D Coordinates of where we are in the original grid
    x = new_index%N;
    y = new_index/N - new_index/(N*N) * N;
    z = new_index/(N*N);
    
    {
      i->operator[](overlap_index)=diagonal;

      if(y>0)
        i->operator[](overlap_index - (pbx+2))=bone;
      
      if(y<N-1)
        i->operator[](overlap_index + pbx+2)=bone;

      if(x>0)
        i->operator[](overlap_index-1)=bone;

      if(x < N-1)
        i->operator[](overlap_index+1)=bone;

      if(z>0)
        i->operator[](overlap_index - ((pbx+2) * (pby+2)))=bone;
      
      if(z<N-1)
        i->operator[](overlap_index + ((pbx+2) * (pby+2)))=bone;
    }
  }
}









#endif