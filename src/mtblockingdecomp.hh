#ifndef DUNE_BLOCK_DECOMP_HH
#define DUNE_BLOCK_DECOMP_HH


#include <iostream>
#include <cstring>
#include <string.h>
#include <hwloc.h>
#include "parallelton.hh"
#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/scalarmatrixview.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/solvercategory.hh>
#include <algorithm>
#include "parcommunicator.hh"




namespace Dune{


    template<class M, class X, class Y, class ThreadBackend = ThreadPool>
    class BlockMatrixAdapter2D : public AssembledLinearOperator<M,X,Y>
    {
    public:
        //! export types
        typedef M matrix_type;
        typedef X domain_type;
        typedef Y range_type;
        typedef typename X::field_type field_type; 

        //! constructor: just store a reference to a matrix
        explicit BlockMatrixAdapter2D (const M& A, const ParData& p)
            : _A_(stackobject_to_shared_ptr(A)), pardata(stackobject_to_shared_ptr(p))
        {
            // setup variables used in every iteration
            context = Parallelton<ThreadBackend>::context;
            N = pardata->getN();
            size = pardata->getsize();
            nbrows = N*N / size;
            nbthreads = context->getnbthreads();
            stride = nbrows / nbthreads;
            pbx = pardata->getpbx();
            pby = pardata->getpby();

            std::array<int, 2> dims;
            dims.fill(0);
            MPI_Dims_create(nbthreads, 2, &dims[0]);
            // per thread in x and y direction
            ptx = pbx / dims[0];
            pty = pby / dims[1];

            threadblockx = dims[0];
            threadblocky = dims[1];
        }

        //! constructor: store an std::shared_ptr to a matrix
        explicit BlockMatrixAdapter2D (std::shared_ptr<const M> A, std::shared_ptr<const ParData> p)
            : _A_(A), pardata(p)
        {}

        ~BlockMatrixAdapter2D ()
        {}

        //! apply operator to x:  \f$ y = A(x) \f$
        void apply (const X& x, Y& y) const
        {
    // #ifdef DUNE_ISTL_WITH_CHECKING
    //       if (ready != built)
    //         DUNE_THROW(BCRSMatrixError,"You can only call arithmetic operations on fully built BCRSMatrix instances");
    //       if (x.N()!=M()) DUNE_THROW(BCRSMatrixError,"index out of range");
    //       if (y.N()!=N()) DUNE_THROW(BCRSMatrixError,"index out of range");
    // #endif
            // lambda for every thread
            auto f = [this, &x, &y](auto start, auto stride, auto id)
            {
                // ConstRowIterators
                auto i = _A_->begin() + start;
                auto iend = i + stride;

                auto tx = id%threadblockx;
                auto ty = (id/threadblockx) % threadblocky;

                auto iindex = Parallelton<ThreadBackend>::threadstarts[id]; // thread block start

                auto overlapx = 0ul;
                // overlap on the sides
                if (tx == 0 || tx == threadblockx-1)
                    overlapx++;
                if (tx == 0 && tx == threadblockx-1)
                    overlapx++;

                for (size_t k = 0; k < pty; k++)
                {
                    for (size_t l = 0; l < ptx; l++)
                    {
                        // index inside thread block
                        y[iindex] = 0;
                        // y[i.index()] = 0;
                        // ConstColIterators
                        auto j = (*i).begin();
                        auto endj = (*i).end();
                        for(; j != endj; ++j)
                        {
                            auto&& xj = Impl::asVector(x[j.index()]);
                            // auto&& yi = Impl::asVector(y[i.index()]);
                            auto&& yi = Impl::asVector(y[iindex]);
                            Impl::asMatrix(*j).umv(xj, yi);
                        }

                        // if (id == 0 && pardata->getrank() == 0)
                        // {
                        //     char c;
                        //     std::cin >> c;

                        //     std::cout << "OID=" << iindex << " tx=" << tx << " ty=" << ty << "\n";
                        // }

                        // one element completed
                        ++i; iindex++;
                    }
                    // one row completed   
                    iindex += overlapx;
                }
            };

            // auto rest = nbrows % nbthreads;
            // start threads
            for(size_t i = 0; i < nbthreads; i++)
            {
                auto current = i * stride;

                context->addtask(f, i, current, stride, i);
            }
            // wait for threads
            context->thread0task();
        }

        //! apply operator to x, scale and add:  \f$ y = y + \alpha A(x) \f$
        void applyscaleadd (field_type alpha, const X& x, Y& y) const override
        {}

        //! get matrix via *
        const M& getmat () const override
        {
            return *_A_;
        }

        //! Category of the solver (see SolverCategory::Category)
        SolverCategory::Category category() const override
        {
            return SolverCategory::sequential;
        }


    private:
        const std::shared_ptr<const M> _A_;
        const std::shared_ptr<const ParData> pardata;
        double innerwatch = 0;
        double outerwatch = 0;
        // variables in each iteration
        size_t N;
        size_t size;
        size_t nbrows;
        size_t nbthreads;
        size_t stride;
        size_t pbx, pby;
        size_t ptx, pty;
        size_t threadblockx, threadblocky;
        std::shared_ptr<ThreadBackend> context;
    };



    template <class Vector, int Dim>
    class BlockedParCommunicator5Star : public ParCommunicator5Star<Vector, Dim>
    {
    public:
        BlockedParCommunicator5Star(size_t N, ParData& pardata);
        ~BlockedParCommunicator5Star() = default;

        void setupsendbuffer (Vector& x) override;
        void setuprecvbuffer (Vector& x) override;
    private:
        typedef ParCommunicator<Vector,Dim> Base;
        typedef ParCommunicator5Star<Vector,Dim> Parent;

        using Base::GRID_COMM;
        using Base::request;

        using Base::size;
        using Base::sendbuf; using Base::recvbuf;
        using Base::root; using Base::rank;
        using Base::pbx; using Base::pby; using Base::pbz; using Base::start; using Base::N;
        using Base::blockx; using Base::blocky; using Base::blockz;
        // index of where the first element for a direction is located
        using Base::startcore;
        using Base::counts_send;
        using Base::displ_send;
        using Base::counts_recv;
        using Base::displ_recv;
        
        // size of communication in every direction
        using Parent::commx; using Parent::commy; using Parent::commz;
        // presence of neighbor
        using Parent::neighbxp; using Parent::neighbxn; using Parent::neighbyp; 
        using Parent::neighbyn; using Parent::neighbzp; using Parent::neighbzn; 
        
        using Parent::startx; using Parent::starty; using Parent::startz;
        std::array<int, Dim> coords;
        // sizes of the thread blocks
        size_t threadblockx, threadblocky, threadblockz, ptx, pty, ptz;
        // which indices to send and recv in which direction
        std::vector<size_t> send_neg_x, send_pos_x;
        std::vector<size_t> send_neg_y, send_pos_y;
        std::vector<size_t> send_neg_z, send_pos_z; 
        std::vector<size_t> recv_neg_x, recv_pos_x;
        std::vector<size_t> recv_neg_y, recv_pos_y;
        std::vector<size_t> recv_neg_z, recv_pos_z;

        double setupsendwatch = 0; 
        double setuprecvwatch = 0;
    };


    template <class Vector, int Dim>
    BlockedParCommunicator5Star<Vector,Dim>::BlockedParCommunicator5Star(size_t N, ParData& pardata)
        : ParCommunicator5Star<Vector, Dim>::ParCommunicator5Star(N, pardata)
    {
        // variables for the individual thread blocks
        auto nbthreads = Parallelton<Dune::ThreadPool>::context->getnbthreads();
        std::array<int, Dim> dims;
        dims.fill(0);
        MPI_Dims_create(nbthreads, Dim, &dims[0]);

        threadblockx = dims[0];
        threadblocky = dims[1];
        if(Dim == 3) threadblockz = dims[2];

        ptx = pbx / threadblockx;
        pty = pby / threadblocky;
        if (Dim == 3) ptz = pbz / threadblockz;

        auto rx = pardata.getxcoord();
        auto ry = pardata.getycoord();
        auto rz = pardata.getzcoord();

        // The following variables need to be overwritten
        // where to start looking for the values that need to be communicated (considering local overlap)
        // size_t zoverlap = Dim/3 * (pbx+2) * (pby+2);
        // startx = ptx+1 + zoverlap;
        // starty = 1 + zoverlap;
        // startz = ptx+2;
        // startcore = ptx+2 + zoverlap;
        
        // which variables need to be send
        std::vector<size_t> indices;
        if (Dim == 2)
            indices = setupoverlapindices2D(N, pardata);
        if (Dim == 3)
            indices = setupoverlapindices3D(N, pardata);
        
        auto index = 0ul;

        auto threadblocksize = 0ul;
        if (Dim == 2)
            threadblocksize = ptx*pty;
        if (Dim == 3)
            threadblocksize = ptx*pty*ptz;

        for (size_t j = 0; j < nbthreads; j++)
            for (size_t k = 0; k < threadblocksize; k++, index++)
            {
                // offset so each block is mapped correctly
                auto block_index = index - j * threadblocksize;

                // thread local x and y coords
                size_t x = block_index%ptx;
                size_t y = (block_index/ptx) % pty;
                size_t z = 0;
                if (Dim == 3) z = (block_index/(ptx*pty)) % ptz;
                // coordinates of the current thread block
                auto tx = j%dims[0];
                auto ty = (j/dims[0]) % threadblocky;
                auto tz = 0;
                if (Dim == 3) tz = (j/(dims[0] * dims[1])) % threadblockz;

                auto overlap_index = indices[index];

                // char c;
                // std::cin >> c;
                // std::cout << "x:" << x << " y:" << y << " tx:" << tx << " ty:" << ty << " rx:" << rx << " ry:" << ry << " OID: " << overlap_index << std::endl;
                
                // inter rank dependencies

                // rank block to the left exists
                if (x == 0 && tx == 0 && rx > 0)
                {
                    send_neg_x.emplace_back(overlap_index);
                    recv_neg_x.emplace_back(overlap_index - 1);
                    // if (rank == 0)
                    // {
                    //     std::cout << "send_neg_x: " << overlap_index << "\n";
                    //     std::cout << "recv_neg_x: " << overlap_index - 1 << "\n";
                    // }
                }
                // rank block to the right exists
                if (x == ptx-1 && tx == threadblockx-1 && rx < blockx-1)
                {
                    send_pos_x.emplace_back(overlap_index);
                    recv_pos_x.emplace_back(overlap_index + 1);
                    // if (rank == 0)
                    // {
                    //     std::cout << "send_pos_x: " << overlap_index << "\n";
                    //     std::cout << "recv_pos_x: " << overlap_index + 1 << "\n";
                    // }
                }
                // rank block to bottom exists
                if (y == 0 && ty == 0 && ry > 0)
                {    
                    auto elementsx = ptx;
                    if (tx == 0 || tx == threadblockx-1) // threadblock with overlap to the side
                        elementsx++;
                    if (tx == 0 && tx == threadblockx-1) // threadblock with overlap to both sides
                        elementsx++;

                    send_neg_y.emplace_back(overlap_index);
                    recv_neg_y.emplace_back(overlap_index - elementsx);
                    // if (rank == 0)
                    // {
                    //     std::cout << "send_neg_y: " << overlap_index << "\n";
                    //     std::cout << "recv_neg_y: " << overlap_index - elementsx << "\n";
                    // }
                }
                // rank block to the top exists
                if (y == pty-1 && ty == threadblocky-1 && ry < blocky-1)
                {   
                    auto elementsx = ptx;
                    if (tx == 0 || tx == threadblockx-1) // threadblock with overlap to the side
                        elementsx++;
                    if (tx == 0 && tx == threadblockx-1) // threadblock with overlap to both sides
                        elementsx++;

                    send_pos_y.emplace_back(overlap_index);
                    recv_pos_y.emplace_back(overlap_index + elementsx);

                    // if (rank == 0)
                    // {
                    //     std::cout << "send_pos_y: " << overlap_index << "\n";
                    //     std::cout << "recv_pos_y: " << overlap_index + elementsx << "\n";
                    // }
                }
                if (Dim == 3)
                {
                    // rank block to the front exists
                    if (z == 0 && tz == 0 && rz > 0)
                    {
                        send_neg_z.emplace_back(overlap_index);
                        recv_neg_z.emplace_back(overlap_index - (pbx+2) * (pby+2));

                        // if (rank == 0)
                        // {
                        //     std::cout << "send_neg_z: " << overlap_index << "\n";
                        //     std::cout << "recv_neg_z: " << overlap_index - (pbx+2) * (pby+2) << "\n";
                        // }
                    }
                    // rank block to the back exists
                    if (z == ptz-1 && tz == threadblockz-1 && rz < blockz-1)
                    {
                        send_pos_z.emplace_back(overlap_index);
                        recv_pos_z.emplace_back(overlap_index + (pbx+2) * (pby+2));

                        // if (rank == 0)
                        // {
                        //     std::cout << "send_pos_z: " << overlap_index << "\n";
                        //     std::cout << "recv_pos_z: " << overlap_index + (pbx+2) * (pby+2) << "\n";
                        // }
                    }
                }
            }
    }


    template<class Vector, int Dim>
    void BlockedParCommunicator5Star<Vector,Dim>::setupsendbuffer(Vector& x)
    {
        // the order in the sendbuffer is according to:
        // first send left, then right, then down, then up, then (if existing) front and then back
        // auto start = std::chrono::high_resolution_clock::now();
        
        auto iter = sendbuf.begin();

        if(neighbxn)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                *iter = x[send_neg_x[i]];
                // std::cout << "Send left: " << send_neg_x[i] << std::endl;
            }
        else
            iter += commx;

        if(neighbxp)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                *iter = x[send_pos_x[i]];
                // std::cout << "Send right: " << send_pos_x[i] << std::endl;
            }
        else
            iter += commx;

        if(neighbyn)
            // here we send down and up
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                *iter = x[send_neg_y[i]];
                // std::cout << "Send down: " << send_neg_y[i] << std::endl;
            }
        else
            iter += commy;

        if(neighbyp)
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                *iter = x[send_pos_y[i]];
                // std::cout << "Send up: " << send_pos_y[i] << std::endl;
            }
        else
            iter += commy;
        
        if(neighbzn)
            // here we send down and up
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                *iter = x[send_neg_z[i]];
                // std::cout << "Send down: " << send_neg_y[i] << std::endl;
            }
        else
            iter += commz;

        if(neighbzp)
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                *iter = x[send_pos_z[i]];
                // std::cout << "Send up: " << send_pos_y[i] << std::endl;
            }
        else
            iter += commz;
    };


    template<class Vector, int Dim>
    void BlockedParCommunicator5Star<Vector,Dim>::setuprecvbuffer(Vector& x)
    {
        // auto start = std::chrono::high_resolution_clock::now();

        auto iter = recvbuf.begin();
    
        if(neighbxn)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                x[recv_neg_x[i]] = *iter;
                // std::cout << "Recv left: " << startx + i*(pbx+2) + nextlayer << std::endl;
            }
        else
            iter += commx;

        if(neighbxp)
            for(size_t i = 0; i < commx; i++, ++iter)
            {
                x[recv_pos_x[i]] = *iter;
                // std::cout << "Recv right: " << startx+pbx+1 + i*(pbx+2) + nextlayer << std::endl;
            }
        else
            iter += commx;

        if(neighbyn)
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                x[recv_neg_y[i]] = *iter;
                // std::cout << "Recv down: " << starty + i + nextlayer << std::endl;
            }
        else
            iter += commy;

        if(neighbyp)
            for(size_t i = 0; i < commy; i++, ++iter)
            {
                x[recv_pos_y[i]] = *iter;
                // std::cout << "Recv up: " << starty+(pbx+2)*(pby+1) + i + nextlayer << std::endl;
            }
        else
            iter += commy;

        if(neighbzn)
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                x[recv_neg_z[i]] = *iter;
                // std::cout << "Recv down: " << starty + i + nextlayer << std::endl;
            }
        else
            iter += commz;

        if(neighbzp)
            for(size_t i = 0; i < commz; i++, ++iter)
            {
                x[recv_pos_z[i]] = *iter;
                // std::cout << "Recv up: " << starty+(pbx+2)*(pby+1) + i + nextlayer << std::endl;
            }
        else
            iter += commz;


        // if(rank == 0)
        // {
        //     std::cout << "Recvbuf: ";
        //     for(auto i : recvbuf)
        //         std::cout << i << " ";
        //     std::cout << std::endl;
        // }
        // auto end = std::chrono::high_resolution_clock::now();
        // setuprecvwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
    };



}


#endif