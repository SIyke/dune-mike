#ifndef CUSTOM_PATTERN_HH
#define CUSTOM_PATTERN_HH

#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/scalarmatrixview.hh>
#include <algorithm>
#include "parcommunicator.hh"

template<class B, class Alloc>
void setupFragmentedSparsity(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc> Matrix;

  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto blockx = pardata.getblockx();
  auto size = pardata.getsize();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();
  auto nbrows = N*N / size;
  auto nbcols = (pbx+2) * (pby+2);

  A.setSize(nbrows, nbcols, nbrows*5ul);
  A.setBuildMode(Matrix::row_wise);

  
  for (typename Dune::BCRSMatrix<B,Alloc>::CreateIterator i = A.createbegin(); i != A.createend(); ++i) {

    auto new_index = start + i.index()%pbx + i.index()/pbx * N;
    size_t x = new_index%N;
    size_t y = new_index/N;

    auto overlap_index = i.index() + i.index()/pbx*2 + (pbx+2)+1;
    // if(rank == 0)
    // {      
    //   std::cout << "old index=" << i.index() << " new index=" << new_index << " ovl index=" << overlap_index << std::endl;
    // //   std::cout << "start=" << start << std::endl;
    // //   std::cout << "index=" << new_index << ", x=" << x << ", y=" << y << std::endl;
      
    //   std::cout << std::endl;
    // }

    if(y>0)
      // insert lower neighbour
    {
      auto tmp = overlap_index - (pbx+2); 

      i.insert(tmp);
      
      // if(rank == 0) std::cout << "Inserted lower of x=" << x << ", y=" << y << " at " << tmp << std::endl;
    }
    if(x>0)
      // insert left neighbour
    {
      i.insert(overlap_index-1);
      // if(rank == 0) std::cout << "Inserted left of x=" << x << ", y=" << y << " at " << overlap_index-1 << std::endl;
    }
    // insert diagonal value
    i.insert(overlap_index);
    
    // if(rank == 0) std::cout << "Inserted center of x=" << x << ", y=" << y << " at " << overlap_index << std::endl;

    if(x<N-1)
      //insert right neighbour
    {
      i.insert(overlap_index+1);
      
      // if(rank == 0) std::cout << "Inserted right of x=" << x << ", y=" << y << " at " << overlap_index+1 << std::endl;
    }
      
    if(y<N-1)
      // insert upper neighbour
    {
      auto tmp = overlap_index + pbx+2; 

      i.insert(tmp);
      // if(rank == 0) std::cout << "Inserted upper of x=" << x << ", y=" << y << " at " << overlap_index + pbx+2 - (overlap_index-pbx)/((pbx+2)*(pby-1)) << std::endl;
    }
  }
}


template<class B, class Alloc>
void setupFragmentedLaplacian(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc>::field_type FieldType;

  setupFragmentedSparsity(A,N,pardata);

  B diagonal(static_cast<FieldType>(0)), bone(static_cast<FieldType>(0));

  auto setDiagonal = [](auto&& scalarOrMatrix, const auto& value) {
    auto&& matrix = Dune::Impl::asMatrix(scalarOrMatrix);
    for (auto rowIt = matrix.begin(); rowIt != matrix.end(); ++rowIt)
      (*rowIt)[rowIt.index()] = value;
  };

  setDiagonal(diagonal, 4.0);
  setDiagonal(bone, -1.0);

  auto nbrows = N*N / pardata.getsize();
  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto blockx = pardata.getblockx();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();

  for (typename Dune::BCRSMatrix<B,Alloc>::RowIterator i = A.begin(); i != A.end(); ++i) {
    auto new_index = start + i.index()%pbx + i.index()/pbx * N;
    size_t x = new_index%N;
    size_t y = new_index/N;
    auto overlap_index = i.index() + i.index()/pbx*2 + (pbx+2)+1;

    // auto new_index = start + i.index()%pbx + i.index()/pbx * N;
    // int x = i.index()%N; // x coordinate in the 2d field
    // int y = i.index()/N;  // y coordinate in the 2d field   

    /*    if(x==0 || x==N-1 || y==0||y==N-1){

       i->operator[](i.index())=1.0;

       if(y>0)
       i->operator[](i.index()-N)=0;

       if(y<N-1)
       i->operator[](i.index()+N)=0.0;

       if(x>0)
       i->operator[](i.index()-1)=0.0;

       if(x < N-1)
       i->operator[](i.index()+1)=0.0;

       }else*/
    {
      i->operator[](overlap_index)=diagonal;

      if(y>0)
      {
        auto tmp = overlap_index - (pbx+2); 
        // if in the lowermost row
        // if(overlap_index < pbx + 1 + pbx + 1)
        //   tmp++;

        i->operator[](tmp)=bone;
      }
      
      if(y<N-1)
      {
        auto tmp = overlap_index + pbx+2; 
        // if in the uppermost row
        // if(overlap_index > pbx + (pbx+2)*(pby-1))
        //   tmp--;

        i->operator[](tmp)=bone;
      }
      if(x>0)
        i->operator[](overlap_index-1)=bone;

      if(x < N-1)
        i->operator[](overlap_index+1)=bone;
    }
  }
}




template<class B, class Alloc>
void setupFragmented3DSparsity(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc> Matrix;

  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto pbz = pardata.getpbz();
  auto blockx = pardata.getblockx();
  auto size = pardata.getsize();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();

  auto nbrows = N*N*N / size;
  auto nbcols = (pbx+2) * (pby+2) * (pbz+2);

  A.setSize(nbrows, nbcols, nbrows*7ul);
  A.setBuildMode(Matrix::row_wise);

  
  for (typename Dune::BCRSMatrix<B,Alloc>::CreateIterator i = A.createbegin(); i != A.createend(); ++i) {
    // index of where we would be currently if we were working with a complete matrix
                              // x contribution
    auto new_index = start + i.index()%pbx + 
                              // y contribution    subtract the y contribution if we start a new layer in z direction
                              i.index()/pbx * N - i.index()/(pbx*pby) * N*pby +
                              // z contribution
                              i.index()/(pbx*pby) * N*N; 
    // 3D Coordinates of where we are in the original grid
    size_t x = new_index%N;
    size_t y = new_index/N - new_index/(N*N) * N;
    size_t z = new_index/(N*N);

    // helping variables
    size_t frontoverlap = (pbx+2) * (pby+2);
    // 3D Coordinates of where we are in our local grid
    size_t local_x = i.index() % pbx;
    size_t local_y = i.index() / pbx - i.index()/(pbx*pby)*pby;
    size_t local_z = i.index() / (pbx*pby);

    // with this index we only want to iterate over the "core" of our values stored on this rank
    // basically we get the index of the "core" within the vector with overlap
    //                               if we go one level higher skip overlap on the sides
    auto overlap_index = i.index() + i.index()/pbx * 2
                        // if we clear one slide we go one layer into back direction (pos z)
                        + i.index()/(pbx*pby) * (2*(pbx+2)) 
                        // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                        + frontoverlap+(pbx+2)+1;

    if(rank == 0)
    {
      // std::cout << "old index=" << i.index() << " new index=" << new_index << " overlap index=" << overlap_index << std::endl;
      // std::cout << "x=" << x << ", y=" << y << ", z=" << z << std::endl;
      // std::cout << std::endl;
    }

    if(y>0)
      // insert lower neighbour
    {
      int tmp = overlap_index - (pbx+2); 

      i.insert(tmp);
      // if(rank == 0) std::cout << "Inserted below of x=" << x << ", y=" << y << ", z=" << z << " at " << tmp << std::endl; 
    }
    if(x>0)
      // insert left neighbour
    {
      i.insert(overlap_index-1);
      // if(rank == 0) std::cout << "Inserted left of x=" << x << ", y=" << y << ", z=" << z << " at " << overlap_index-1 << std::endl; 
    }
    if(z>0)
      // insert front neighbour
    {
      int tmp = overlap_index - ((pbx+2) * (pby+2));

      i.insert(tmp);
      // if(rank == 0) std::cout << "Inserted front of x=" << x << ", y=" << y << ", z=" << z << " at " << tmp << std::endl; 
    }
    // insert diagonal value
    i.insert(overlap_index);
    // if(rank == 0) std::cout << "Inserted center of x=" << x << ", y=" << y << ", z=" << z << " at " << overlap_index << " with oid=" << overlap_index << std::endl;

    if(x<N-1)
      //insert right neighbour
    {
      i.insert(overlap_index+1);
      // if(rank == 0) std::cout << "Inserted right of x=" << x << ", y=" << y << ", z=" << z << " at " << overlap_index+1 << std::endl;
    }
      
    if(y<N-1)
      // insert upper neighbour
    {
      int tmp = overlap_index + pbx+2;

      i.insert(tmp);
      // if(rank == 0) std::cout << "Inserted above of x=" << x << ", y=" << y << ", z=" << z << " at " << tmp << std::endl; 
    }

    if(z<N-1)
    {
      int tmp = overlap_index + ((pbx+2) * (pby+2));

      i.insert(tmp);
      // if(rank == 0) std::cout << "Inserted back of x=" << x << ", y=" << y << ", z=" << z << " at " << tmp << std::endl; 
    }
    // std::cout << std::endl;
  }
}


template<class B, class Alloc>
void setupFragmented3DLaplacian(Dune::BCRSMatrix<B,Alloc>& A, size_t N, Dune::ParData& pardata)
{
  typedef typename Dune::BCRSMatrix<B,Alloc>::field_type FieldType;

  setupFragmented3DSparsity(A,N,pardata);

  B diagonal(static_cast<FieldType>(0)), bone(static_cast<FieldType>(0));

  auto setDiagonal = [](auto&& scalarOrMatrix, const auto& value) {
    auto&& matrix = Dune::Impl::asMatrix(scalarOrMatrix);
    for (auto rowIt = matrix.begin(); rowIt != matrix.end(); ++rowIt)
      (*rowIt)[rowIt.index()] = value;
  };

  setDiagonal(diagonal, 6.0);
  setDiagonal(bone, -1.0);

  auto pbx = pardata.getpbx();
  auto pby = pardata.getpby();
  auto pbz = pardata.getpbz();
  auto blockx = pardata.getblockx();
  auto rank = pardata.getrank();
  auto start = pardata.getstart();


  for (typename Dune::BCRSMatrix<B,Alloc>::RowIterator i = A.begin(); i != A.end(); ++i) {
    auto new_index = start + i.index()%pbx + 
                              // y contribution    subtract the y contribution if we start a new layer in z direction
                              i.index()/pbx * N - i.index()/(pbx*pby) * N*pby +
                              // z contribution
                              i.index()/(pbx*pby) * N*N; 
    // 3D Coordinates of where we are in the original grid
    size_t x = new_index%N;
    size_t y = new_index/N - new_index/(N*N) * N;
    size_t z = new_index/(N*N);

    // with this index we only want to iterate over the "core" of our values stored on this rank
    // basically we get the index of the "core" within the vector with overlap
    //                               if we go one level higher skip overlap on the sides
    // helping variables
    size_t frontoverlap = (pbx+2) * (pby+2);
    
    auto overlap_index = i.index() + i.index()/pbx * 2
                        // if we clear one slide we go one layer into back direction (pos z)
                        + i.index()/(pbx*pby) * (2*(pbx+2)) 
                        // offset due to overlap in the front direction + bottom direction + 1 one the left direction
                        + frontoverlap+(pbx+2)+1;
    // 3D Coordinates of where we are in our local grid
    size_t local_x = i.index() % pbx;
    size_t local_y = i.index() / pbx - i.index()/(pbx*pby)*pby;
    size_t local_z = i.index() / (pbx*pby);
    {
      i->operator[](overlap_index)=diagonal;

      if(y>0)
      {
        auto tmp = overlap_index - (pbx+2); 

        i->operator[](tmp)=bone;
      }
      
      if(y<N-1)
      {
        auto tmp = overlap_index + pbx+2; 

        i->operator[](tmp)=bone;
      }

      if(x>0)
        i->operator[](overlap_index-1)=bone;

      if(x < N-1)
        i->operator[](overlap_index+1)=bone;

      if(z>0)
      {
        int tmp = overlap_index - ((pbx+2) * (pby+2));
        
        i->operator[](tmp)=bone;
      }
      if(z<N-1)
      {
        int tmp = overlap_index + ((pbx+2) * (pby+2));        
        
        i->operator[](tmp)=bone;
      }
    }
  }
}


#endif