#ifndef DUNE_MPI_TYPE_TRAITS_HH
#define DUNE_MPI_TYPE_TRAITS_HH

#include <mpi.h>
// #include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <vector>
#include <utility>
#include <complex>
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include "threadpool.hh"


template<class T>
struct MPITypeTraits
{
    typedef T element_type;
    typedef T* element_addr_type;

    static inline MPI_Datatype get_type(T&& raw);
    static inline size_t get_size(T& raw) { return 1; }
    static inline element_addr_type get_addr(T& raw) { return &raw; }
};

/** 
 * Specialization of the mpi_type_traits for primitive types 
 */
#define PRIMITIVE(Type, MpiType) \
	template<> \
	inline MPI_Datatype MPITypeTraits<Type>::get_type(Type&&) { \
		return MpiType; \
	}

PRIMITIVE(char, 				MPI_CHAR);
PRIMITIVE(wchar_t,				MPI_WCHAR);
PRIMITIVE(short, 				MPI_SHORT);
PRIMITIVE(int, 					MPI_INT);
PRIMITIVE(long, 				MPI_LONG);
PRIMITIVE(signed char, 			MPI_SIGNED_CHAR);
PRIMITIVE(unsigned char, 		MPI_UNSIGNED_CHAR);
PRIMITIVE(unsigned short, 		MPI_UNSIGNED_SHORT);
PRIMITIVE(unsigned int,			MPI_UNSIGNED);
PRIMITIVE(unsigned long,		MPI_UNSIGNED_LONG);
PRIMITIVE(unsigned long long,	MPI_UNSIGNED_LONG_LONG);

PRIMITIVE(float, 				MPI_FLOAT);
PRIMITIVE(double, 				MPI_DOUBLE);
PRIMITIVE(long double,			MPI_LONG_DOUBLE);

// PRIMITIVE(bool,						MPI_BOOL);
PRIMITIVE(std::complex<float>,		MPI_COMPLEX);
PRIMITIVE(std::complex<double>,		MPI_DOUBLE_COMPLEX);
// PRIMITIVE(std::complex<long double>,	MPI_LONG_DOUBLE_COMPLEX);

#undef PRIMITIVE 

// ... add missing types here ...


    template<class T>
    struct MPITypeTraits<std::vector<T>>
    {
        typedef T element_type;
        typedef T* element_addr_type;

        static inline MPI_Datatype get_type(std::vector<T>&& v)
        {
            return MPITypeTraits<T>::get_type( T() );
        }
        static inline size_t get_size(std::vector<T>& v)
        {
            return v.size();
        }
        static inline element_addr_type get_addr(std::vector<T>& v)
        {
            return MPITypeTraits<T>::get_addr( v.front() );
        }
    };

    template<class T, int BS>
    struct MPITypeTraits<Dune::FieldVector<T,BS>>
    {
        typedef T element_type;
        typedef T* element_addr_type;

        static inline MPI_Datatype get_type(Dune::FieldVector<T,BS>&& v)
        {
            return MPITypeTraits<T>::get_type( T() );
        }
        static inline size_t get_size(Dune::FieldVector<T,BS>& v)
        {
            return v.size();
        }
        static inline element_addr_type get_addr(Dune::FieldVector<T,BS>& v)
        {
            return MPITypeTraits<T>::get_addr( v.front() );
        }
    };

    template<class T>
    struct MPITypeTraits<Dune::BlockVector<T>>
    {
        typedef T element_type;
        typedef T* element_addr_type;

        static inline MPI_Datatype get_type(Dune::BlockVector<T>&& v)
        {
            return MPITypeTraits<T>::get_type( T() );
        }
        static inline size_t get_size(Dune::BlockVector<T>& v)
        {
            return v.size();
        }
        static inline element_addr_type get_addr(Dune::BlockVector<T>& v)
        {
            return MPITypeTraits<T>::get_addr( *v.begin() );
        }
    };

#endif