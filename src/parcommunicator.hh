#ifndef DUNE_PAR_COMMUNICATOR_HH
#define DUNE_PAR_COMMUNICATOR_HH

#include <mpi.h>
// #include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <vector>
#include <utility>
#include <complex>
#include <array>
#include <cmath>
#include "mpitypetraits.hh"
#include "parallelton.hh"
#include "parallocator.hh"
#include "mtblockingvectorallocator.hh"
#include "mtblockingalloc.hh"

namespace Dune{

    template<typename T>
    struct Buffer
    {
        using type = T;
    };

    template<typename T, int BS>
    struct Buffer<FieldVector<T, BS>>
    {
        using type = typename Buffer<T>::type;
    };

    template<typename T>
    struct Buffer<BlockVector<T>>
    {
        using type = typename Buffer<T>::type;
    };

    template<typename T>
    struct Buffer<BlockVector<T, ParAllocator<T>>>
    {
        using type = typename Buffer<T>::type;
    };

    template<typename T>
    struct Buffer<BlockVector<T, BlockAllocator2D<T>>>
    {
        using type = typename Buffer<T>::type;
    };

    template<typename T>
    struct Buffer<BlockVector<T, MtBlockVectorAllocator<T>>>
    {
        using type = typename Buffer<T>::type;
    };

    template<typename T>
    struct Buffer<std::vector<T>>
    {
        using type = typename Buffer<T>::type;
    };

    template<typename T>
    using Buffer_t = typename Buffer<T>::type;


    struct ParData
    {
        void set(int rank_, int size_, size_t pbx_, size_t pby_, size_t pbz_, size_t blockx_, size_t blocky_, size_t blockz_, size_t start_, size_t N_, 
                    int xcoord_, int ycoord_, int zcoord_)
        {
            rank = rank_;
            size = size_;
            pbx = pbx_;
            pby = pby_;
            pbz = pbz_;
            blockx = blockx_;
            blocky = blocky_;
            blockz = blockz_;
            start = start_;
            N = N_;
            xcoord = xcoord_;
            ycoord = ycoord_;
            zcoord = zcoord_;
        }

        auto getrank() const { return rank; }
        auto getN() const { return N; }
        auto getpbx() const { return pbx; }
        auto getpby() const { return pby; }
        auto getpbz() const { return pbz; }
        auto getsize() const { return size; }
        auto getblockx() const { return blockx; }
        auto getblocky() const { return blocky; }
        auto getblockz() const { return blockz; }
        auto getstart() const { return start; }
        auto getxcoord() const { return xcoord; }
        auto getycoord() const { return ycoord; }
        auto getzcoord() const { return zcoord; }

    private:
        int rank, size, xcoord, ycoord, zcoord;
        size_t pbx, pby, pbz, blockx, blocky, blockz, start, N;
    };


    template<class Vector, int Dim>
    class ParCommunicator
    {
    public:
        ParCommunicator(size_t N);
        ~ParCommunicator();

        virtual void setupsendbuffer(Vector& x) = 0;
        virtual void setuprecvbuffer(Vector& x) = 0;

        void communicate(Vector& x);
        void Icommunicatestart(Vector& b);
        void Icommunicateend(Vector& x);

        void gatherresult(Vector& b, Vector& result);
        void broadcastvector(Vector&& v);

    protected:
        MPI_Comm GRID_COMM;
        MPI_Request request;

        std::vector<Buffer_t<Vector>> sendbuf;
        std::vector<Buffer_t<Vector>> recvbuf;

        int size, root, rank;
        size_t pbx, pby, pbz, start, N;
        size_t blockx, blocky, blockz;

        // index of where the first element for a direction is located
        size_t startcore;
        std::vector<int> counts_send;
        std::vector<int> displ_send;
        std::vector<int> counts_recv;
        std::vector<int> displ_recv;

        double commwatch = 0.0;
    };


    template<class Vector, int Dim>
    ParCommunicator<Vector,Dim>::ParCommunicator(size_t N)
        : N(N), root(0)
    {}


    template<class Vector, int Dim>
    ParCommunicator<Vector,Dim>::~ParCommunicator()
    {
        // std::array<int, Dim> coords;
        // MPI_Cart_coords(GRID_COMM, rank, Dim, &coords[0]);
        // // how many variables are communicated in each direction
        // // 2D
        // auto commx = pby;
        // auto commy = pbx;
        // auto commz = 0;
        // // 3D
        // if constexpr (Dim == 3)
        // {
        //     commx = pby*pbz;
        //     commy = pbx*pbz;
        //     commz = pbx*pby;
        // }


        // auto bytessend = 0.0;

        // // check for neighbors in x direction
        // if(coords[0] > 0)
        //     bytessend += commx;
        // if(coords[0] < blockx-1)
        //     bytessend += commx;

        // // check for neighbors in y direction
        // if(coords[1] > 0)
        //     bytessend += commy;
        // if(coords[1] < blocky-1)
        //     bytessend += commy;

        // if constexpr (Dim == 3)
        // {
        //     // check for neighbors in z direction
        //     if(coords[2] > 0)
        //         bytessend += commz;
        //     if(coords[2] < blockz-1)
        //         bytessend += commz;
        // }

        // // send data are doubles
        // bytessend *= 8.0;
        // // in megabytes
        // bytessend /= (1024.0 * 1024.0);

        // double sendbuf[] = {bytessend, commwatch};

        // if(rank == 0)
        // {
        //     std::vector<double> buffer(size * 2);
        //     MPI_Gather(sendbuf, 2, MPI_DOUBLE, &buffer[0], 2, MPI_DOUBLE, 0, GRID_COMM);
            
        //     bytessend = 0;
        //     commwatch = 0;

        //     for(auto i = 0; i < buffer.size(); i++)
        //     {
        //         if(i%2 == 0)
        //             bytessend += buffer[i];
        //         else
        //             commwatch += buffer[i];
        //     }

        //     std::cout << "Accumulated MB send=" << bytessend << "; Time taken=" << commwatch << "; Comm Bandwidth=" << bytessend/commwatch << " MB/s" << std::endl;
        //     std::cout << "Averaged MB send=" << bytessend/size << "; Time taken=" << commwatch/size << std::endl;
        // }
        // else
        // {
        //     MPI_Gather(sendbuf, 2, MPI_DOUBLE, NULL, 0, MPI_DOUBLE, 0, GRID_COMM);
        // }

        MPI_Comm_free(&GRID_COMM);
    }


    template<class Vector, int Dim>
    void ParCommunicator<Vector,Dim>::broadcastvector(Vector&& vec)
    {
        MPI_Bcast( // MPITypeTraits<Vec>::get_addr(vec),
                  &vec[0],
                  MPITypeTraits<Vector>::get_size(vec),
                  MPITypeTraits<Vector>::get_type(std::move(vec)), root, GRID_COMM);
    }


    template<class Vector, int Dim>
    void ParCommunicator<Vector,Dim>::communicate(Vector& x)
    {
        setupsendbuffer(x);

        // auto start = std::chrono::high_resolution_clock::now();
        MPI_Neighbor_alltoallv(&sendbuf[0], &counts_send[0], &displ_send[0], MPITypeTraits<std::vector<Buffer_t<Vector>>>::get_type(std::move(sendbuf)), 
                               &recvbuf[0], &counts_recv[0], &displ_recv[0], MPITypeTraits<std::vector<Buffer_t<Vector>>>::get_type(std::move(recvbuf)), GRID_COMM);
        // auto end = std::chrono::high_resolution_clock::now();
        // commwatch += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();

        setuprecvbuffer(x);
    }

    template<class Vector, int Dim>
    void ParCommunicator<Vector,Dim>::Icommunicatestart(Vector& b)
    {
        setupsendbuffer(b);

        MPI_Ineighbor_alltoallv(&sendbuf[0], &counts_send[0], &displ_send[0], MPITypeTraits<std::vector<Buffer_t<Vector>>>::get_type(std::move(sendbuf)),
                                &recvbuf[0], &counts_send[0], &displ_send[0], MPITypeTraits<std::vector<Buffer_t<Vector>>>::get_type(std::move(recvbuf)), GRID_COMM, &request);
    
        // // start the progress thread
        // Parallelton<ThreadPool>::context->startprogressthread(request);
    }

    template<class Vector, int Dim>
    void ParCommunicator<Vector,Dim>::Icommunicateend(Vector& x)
    {
        MPI_Wait(&request, MPI_STATUS_IGNORE);
        // Parallelton<ThreadPool>::context->endprogressthread();

        setuprecvbuffer(x);
    }

    template<class Vector, int Dim>
    void ParCommunicator<Vector,Dim>::gatherresult(Vector& b, Vector& result)
    {
        // on the root process copy the data directly from b to result
        if(rank == root)
        {
            result.resize(std::pow(N, Dim));
                
            // send data in chuncks of pbx, resulting in pby sends
            size_t z = 0;
            do
            {
                for(size_t y = 0; y < pby; y++)
                {
                    for(size_t x = 0; x < pbx; x++)
                    {
                        auto current_src = startcore + x + y*(pbx+2) + z*((pbx+2) * (pby+2));
                        auto current_dest = start + x + y*N + z*(N*N);
                        result[current_dest] = b[current_src];
                        // std::cout << "csrc=" << current_src << " cdest=" << current_dest << std::endl;
                    }
                }
                z++;
            }
            while(z < pbz);
        }
        else
        {
            // send chunks of data, one horizontal line of results per block
            size_t z = 0;
            do
            {      
                for(size_t y = 0; y < pby; y++)
                {
                    auto current = startcore + y*(pbx+2) + z*((pbx+2) * (pby+2));
                    MPI_Request req;
                    MPI_Isend(&b[current], pbx, MPITypeTraits<Vector>::get_type(std::move(b)), root, y + z*pby, GRID_COMM, &req);
                    // std::cout << "send current=" << current << " with tag=" << y + z*pby << std::endl;
                    // for(auto i = 0; i < pbx; i++)
                    //     std::cout << "Sending " << b[current+i] << std::endl;                


                    // not sure why I need this?
                    MPI_Wait(&req, MPI_STATUS_IGNORE);
                }
                z++;
            }
            while (z < pbz);    
        }
    

        // receive the chunks of data
        if(rank == root)
        {
            for(size_t currentrank = 1; currentrank < size; currentrank++)
            {
                std::array<int, Dim> currentcoords;
                MPI_Cart_coords(GRID_COMM, currentrank, Dim, &currentcoords[0]);
                
                size_t currentstart = currentcoords[2] * pbz * (N*N);
                // offset in y direction (y)
                currentstart += currentcoords[1] * N * pby;
                // offset in x direction (x)
                currentstart += currentcoords[0] * pbx;

                size_t z = 0;
                do
                {
                    for(size_t y = 0; y < pby; y++)
                    {
                        auto current = currentstart + y*N + z*(N*N);
                        MPI_Recv(&result[current], pbx, MPITypeTraits<Vector>::get_type(std::move(result)), currentrank, y + z*pby, GRID_COMM, MPI_STATUS_IGNORE);
                        // std::cout << "recv current=" << current << " with tag=" << y + z*pby << " from rank=" << currentrank << std::endl;

                        // for(auto i = 0; i < pbx; i++)
                        //     std::cout << "Receiving " << result[current+i] << std::endl;
                    }
                    z++;
                }
                while (z < pbz);
            }
        }
    }
}


#endif