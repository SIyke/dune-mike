#!/bin/bash
echo "Results" > results.txt

mpirun -np 16 ./build-cmake/src/dune-mike 8 >> build-cmake/output.txt
mpirun -np 128 ./build-cmake/src/dune-mike 1 >> build-cmake/output.txt


#likwid-mpirun -np 8 -pin S0:0-7_S0:8-15_S0:16-23_S0:24-31_S0:32-39_S0:40-47_S0:48-55_S0:56-63 ./build-cmake/src/dune-mike 128 8 1000
#likwid-mpirun -np 8 -pin S0:0-7_S0:8-15_S0:16-23_S0:24-31_S0:32-39_S0:40-47_S0:48-55_S0:56-63 ./build-cmake/src/dune-mike 256 8 1000
#likwid-mpirun -np 8 -pin S0:0-7_S0:8-15_S0:16-23_S0:24-31_S0:32-39_S0:40-47_S0:48-55_S0:56-63 ./build-cmake/src/dune-mike 1024 8 1000
#likwid-mpirun -np 8 -pin S0:0-7_S0:8-15_S0:16-23_S0:24-31_S0:32-39_S0:40-47_S0:48-55_S0:56-63 ./build-cmake/src/dune-mike 4096 8 1000
#likwid-mpirun -np 8 -pin S0:0-7_S0:8-15_S0:16-23_S0:24-31_S0:32-39_S0:40-47_S0:48-55_S0:56-63 ./build-cmake/src/dune-mike 8192 8 1000
#likwid-mpirun -np 8 -pin S0:0-7_S0:8-15_S0:16-23_S0:24-31_S0:32-39_S0:40-47_S0:48-55_S0:56-63 ./build-cmake/src/dune-mike 16384 8 1000
