#include "config.h"
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/operators.hh>
#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/communicator.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <vector>
#include <thread>
#include <utility>
#include <iostream>
#include <cstdlib>
#include <chrono>
#include <hwloc.h>
#include "mpi.h"


int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // check for number of ranks and problem size
    if(size != 2)
    {
        std::cout << "Need two ranks" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
        return 1;
    }
    if(argc != 2)
    {
        std::cout << "Need Problem size" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
        return 1;
    }


    // setup cpu binding
    hwloc_topology_t topology;
    hwloc_bitmap_t set;
    int err;

    // setup hwloc topology
    err = hwloc_topology_init(&topology);
    if (err < 0) 
        fprintf(stderr, "failed to initialize the topology\n");
    
    err = hwloc_topology_load(topology);
    if (err < 0) {
        fprintf(stderr, "failed to load the topology\n");
        hwloc_topology_destroy(topology);
    }

    // setup hwloc bitmap
    set = hwloc_bitmap_alloc();
    if (!set) {
        fprintf(stderr, "failed to allocate a bitmap\n");
        hwloc_topology_destroy(topology);
    }

    int logid;
    if (rank == 0)
        logid = 0;
    else
        logid = 128;

    auto logobj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_PU, logid);
    hwloc_bitmap_only(set, logobj->os_index);

    err = hwloc_set_cpubind(topology, set, HWLOC_CPUBIND_THREAD);
    if (err < 0) {
        fprintf(stderr, "failed to set thread binding\n");
        hwloc_bitmap_free(set);
        hwloc_topology_destroy(topology);
    }

    hwloc_bitmap_free(set);



    std::vector<int> data;
    auto N = std::atoi(argv[1]);
    auto iter = 10;
    auto blockingtime = 0.0;
    auto nonblockingtime = 0.0;


    data.resize(N, 0.0);

    // blocking measurements
    if(rank == 0)
    {   
        for(auto i = 0; i < iter; i++)
        {
            // auto start = std::chrono::high_resolution_clock::now();
            MPI_Send(&data[0], N, MPI_INT, 1, 0, MPI_COMM_WORLD);
            // auto end = std::chrono::high_resolution_clock::now();
            // blockingtime += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }
        // std::cout << "Send takes " << blockingtime << "s" << std::endl;
    }
    else
    {
        for(auto i = 0; i < iter; i++)
        {            
            auto start = std::chrono::high_resolution_clock::now();
            MPI_Recv(&data[0], N, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            auto end = std::chrono::high_resolution_clock::now();
            blockingtime += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }
        std::cout << "Recv takes " << blockingtime << "s" << std::endl;
    }


    MPI_Finalize();
    return 0;
}