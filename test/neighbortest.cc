#include "config.h"
#include "parcommunicator.hh"


int testmpineighbor(int N)
{
    typedef std::vector<int> Vector;

    Dune::ParCommunicator<Vector, 3> parcomm(N);
    // down:0 up:1 left:2 right:3 front:4 back:5 
    Vector b{-1,-1,
             -1,
             -1, 1, -1,
             -1, 2, -1,
             -1,
             -1,
             -1, 1, -1,
             -1, 2, -1,
             -1,
             -1,-1
            };
            
    Vector x(b.size(), -2);

    parcomm.communicate(x,b);
    
    if(Parallelton<Dune::ThreadPool>::rank == 0)
    {
        std::cout << "Rank=" << Parallelton<Dune::ThreadPool>::rank << " x: ";
        for(auto i : x)
            std::cout << i << " ";
        std::cout << std::endl;

    }

    return 0;
}




int main(int argc, char** argv)
{
    int thread_safety_level;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_safety_level);

    if(MPI_THREAD_FUNNELED != thread_safety_level)
        std::cout << "required MPI thread safety level not provided" << std::endl;

    auto N = 4;
    auto ret = testmpineighbor(N);

    MPI_Finalize();
    return ret;
}