#include <dune/common/test/testsuite.hh>
#include "threadpool.hh"
#include "threadpool_old.hh"
#include "parmatrixadapter.hh"
#include <memory>
#include "laplacian.hh"

using namespace Dune;


int simpletest(int threadcount)
{
    auto threadpool = std::make_shared<ThreadPool>(threadcount);

    auto N = 1024;
    std::vector<int> results(N, 0);
    bool equal = true;

    auto f = [&results](int i)
    {
        results[i] = i;
    };

    for(auto i = 0; i < N; i++)
    {
        size_t tid = i % threadcount;
        threadpool->addtask(f, tid, i);
    }

    threadpool->waittasks();

    for(auto i = 0; i < N; i++)
    {
        equal = equal && (results[i] == i);
    }

    if(equal)
        return 0;
    else
    {
        std::cerr << "error: simpletest not correct" << std::endl;
        return 1;
    }
}


int main (int argc, char** argv)
{
    auto ret = simpletest(2);
    ret += simpletest(4);
    ret += simpletest(6);
    ret += simpletest(8);
}