#include "config.h"
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/operators.hh>
#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/communicator.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <vector>
#include <utility>
#include <cstdlib>
#include <iostream>
#include <chrono>
#include "mpi.h"
#include "parcommunicators.hh"
#include "parallelton.hh"
#include "threadpool.hh"
#include "laplacian.hh"
#include "customlaplacian.hh"
#include "consecutivelaplacian.hh"
#include "parmatrixadapter.hh"
#include "parcalculations.hh"
#include "timeexperiment.hh"

using namespace Dune;

constexpr int BS = 1;

// dune-istl types  
typedef Dune::FieldMatrix<double,BS,BS> MatrixBlock;
typedef Dune::BCRSMatrix<MatrixBlock> BCRSMat;

typedef Dune::FieldVector<double,BS> VectorBlock;
typedef Dune::BlockVector<VectorBlock> Vector;

typedef Dune::MatrixAdapter<BCRSMat,Vector,Vector> Operator;

// thread pool
typedef Dune::ThreadPool ThreadBackend;

// custom allocator types
// typedef Dune::ParAllocator<MatrixBlock> MatAllocator;
// typedef Dune::ParAllocator<VectorBlock> VecAllocator;

// typedef Dune::BlockVector<VectorBlock, VecAllocator> CustomAllocVec;
// typedef Dune::BCRSMatrix<MatrixBlock, MatAllocator> CustomAllocMat;

// typedef Dune::ParMatrixAdapter<AllocMat, Vector, Vector, ThreadBackend> AllocOperator;

typedef Parallelton<ThreadBackend> ParalleltonTP;


int testcomm(size_t N)
{
    ParData pardata;
    ParCommunicator5Star<Vector,2> parcomm(N, pardata);

    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();

    auto context = ParalleltonTP::context;
    auto nbthreads = context->getnbthreads();
    std::array<int, 2> dims;
    dims.fill(0);
    MPI_Dims_create(nbthreads, 2, &dims[0]);

    auto localpbx = pbx / dims[0];
    auto localpby = pby / dims[1];

    std::vector<size_t> threadstarts(nbthreads); 
    for(auto i = 0ul; i < nbthreads; i++)
    {
        auto x = i % dims[0];
        auto y = i / dims[0];

        // offset in y direction (y)
        auto start = y * pbx * localpby;
        // offset in x direction (x)
        start += x * localpbx;

        threadstarts[i] = start;
    }

    // TODO: pad BCRSMat to one full cache line (same with Vector)
    // struct paddedBCRSMat
    // {
    //     BCRSMat mat;
    //     // char padding[64 - sizeof(BCRSMat)];

    //     auto operator[](size_t i)
    //     {
    //         mat[i];
    //     }
    // };

    std::vector<BCRSMat> mats;
    std::vector<Vector> xs;
    std::vector<Vector> bs;
    std::vector<Vector> b_0s;

    mats.resize(nbthreads);
    xs.resize(nbthreads);
    bs.resize(nbthreads);
    b_0s.resize(nbthreads);


    auto setupsparsity = [&mats, &threadstarts, localpbx, localpby, nbthreads](size_t id, size_t N)
    {
        auto nbrows = localpbx * localpby;
        auto nbcols = (localpbx+2) * (localpby+2);

        BCRSMat mat;
        mats[id] = mat;
        mats[id].setSize(nbrows, nbcols, nbrows*5ul);
        mats[id].setBuildMode(BCRSMat::row_wise);

        for (auto i = mats[id].createbegin(); i != mats[id].createend(); ++i) {

            auto new_index = threadstarts[id] + i.index()%localpbx + i.index()/localpbx * N;
            size_t x = new_index%N;
            size_t y = new_index/N;

            auto overlap_index = i.index() + i.index()/localpbx*2 + (localpbx+2)+1;

            if(y>0)
            // insert lower neighbour
            {
                auto tmp = overlap_index - (localpbx+2); 
                i.insert(tmp);          
            }
            if(x>0)
            // insert left neighbour
            {
                i.insert(overlap_index-1);
            }
            // insert diagonal value
            i.insert(overlap_index);
            
            if(x<N-1)
            //insert right neighbour
            {
                i.insert(overlap_index+1); 
            }
            
            if(y<N-1)
            // insert upper neighbour
            {
                auto tmp = overlap_index + localpbx+2; 
                i.insert(tmp);
            }
        }
    };
    std::mutex mtx;
    auto setupmats = [&mats, N, setupsparsity, nbthreads, localpbx, localpby, threadstarts, &mtx](size_t id)
    {
        typedef typename BCRSMat::field_type FieldType;

        setupsparsity(id, N);

        MatrixBlock diagonal(static_cast<FieldType>(0)), bone(static_cast<FieldType>(0));

        auto setDiagonal = [](auto&& scalarOrMatrix, const auto& value) {
            auto&& matrix = Dune::Impl::asMatrix(scalarOrMatrix);
            for (auto rowIt = matrix.begin(); rowIt != matrix.end(); ++rowIt)
            (*rowIt)[rowIt.index()] = value;
        };

        setDiagonal(diagonal, 4.0);
        setDiagonal(bone, -1.0);

        for (auto i = mats[id].begin(); i != mats[id].end(); ++i) {
            auto new_index = threadstarts[id] + i.index()%localpbx + i.index()/localpbx * N;
            size_t x = new_index%N;
            size_t y = new_index/N;
            auto overlap_index = i.index() + i.index()/localpbx*2 + (localpbx+2)+1;

            {
                i->operator[](overlap_index)=diagonal;

                if(y>0)
                {
                    auto tmp = overlap_index - (localpbx+2);
                    i->operator[](tmp)=bone;
                }
                
                if(y<N-1)
                {
                    auto tmp = overlap_index + localpbx+2; 
                    i->operator[](tmp)=bone;
                }
                if(x>0)
                    i->operator[](overlap_index-1)=bone;

                if(x < N-1)
                    i->operator[](overlap_index+1)=bone;
            }
        }


        // for (auto i = 0; i < nbthreads; i++)
            // if(i == id)
            // {
            //     std::lock_guard<std::mutex> lock(mtx);
            //     std::cout << "T" << id << "\n";
            //     auto N = mats[id].N();
            //     auto M = mats[id].M();
            //     // print out sparsity pattern
            //     for(auto x = 0; x < N; x++)
            //     {
            //         for(auto y = 0; y < M; y++)
            //         {
            //             if(mats[id].exists(x, y))
            //                 std::cout << std::fixed << std::setprecision(2) << mats[id][x][y] << " ";
            //             else
            //                 std::cout << std::fixed << std::setprecision(2) << 0.0 << " ";
            //         }
            //         std::cout << std::endl;
            //     }
            //     std::cout << std::endl;
            // }
    };

    auto setupvecs = [&xs,&bs,&b_0s,localpbx,localpby,&mtx,nbthreads](size_t id)
    {
        Vector vec0, vec1, vec2;
        xs[id] = vec0;
        bs[id] = vec1;
        b_0s[id] = vec2;

        xs[id].resize((localpbx+2) * (localpby+2));
        bs[id].resize((localpbx+2) * (localpby+2));
        b_0s[id].resize((localpbx+2) * (localpby+2));

        xs[id] = 1.0;
        bs[id] = 0.0;
        b_0s[id] = 0.0;

        // for (auto i = 0; i < nbthreads; i++)
            // if(id == i)
            // {
            //     std::lock_guard<std::mutex> lock(mtx);
            //     std::cout << "T" << id << "\n";
            //     // print out sparsity pattern
            //     for(auto const& x : xs[id])
            //         std::cout << std::fixed << std::setprecision(2) << x << " ";
            //     std::cout << std::endl;

            //     for(auto const& x : bs[id])
            //         std::cout << std::fixed << std::setprecision(2) << x << " ";
            //     std::cout << std::endl;

            //     for(auto const& x : b_0s[id])
            //         std::cout << std::fixed << std::setprecision(2) << x << " ";
            //     std::cout << std::endl;
            //     std::cout << std::endl;
            // }
    };

    for(auto i = 0ul; i < nbthreads; i++)
    {
        context->addtask(setupmats, i, i);
        context->addtask(setupvecs, i, i);
        context->waittasks();
    }

    // std::cout << "T0 result" << std::endl;
    // auto j = 0;
    // for(auto y_ = 0; y_ < localpby; y_++)
    // {
    //     for(auto x_ = 0; x_ < localpbx; x_++)
    //     {
    //         std::cout << mat[0][j++] << " "; 
    //     }
    //     std::cout << std::endl;
    // }
    // std::cout << std::endl;

    double omega = 1.0;
    auto stride = localpbx * localpby;
    auto fullstride = (localpbx+2) * (localpby+2);

    // we dont use parmatadapter or parcalc we use their lambdas directly in the main loop
    // here we define their lambdas
    Vector* d;
    Vector* Ad;

    auto apply = [&mats, &bs, &b_0s, localpbx, &d, &Ad](size_t id, size_t iter) 
    {
        // if(iter % 2 == 0)
        // {
            d = &bs[id];
            Ad = &b_0s[id];
        // }
        // else
        // {
        //     d = &b_0s[id];
        //     Ad = &bs[id];
        // }

        // ConstRowIterators
        auto i = mats[id].begin();
        auto iend = mats[id].end();
        for(; i != iend; ++i)
        {
            // index for the vector
            auto colindex = i.index() + i.index()/localpbx*2 + (localpbx+2)+1;
            (*Ad)[colindex] = 0;
            // ConstColIterators
            auto j = (*i).begin();
            auto endj = (*i).end();
            for(; j != endj; ++j)
            {
                auto&& xj = Impl::asVector((*d)[j.index()]);
                auto&& yi = Impl::asVector((*Ad)[colindex]);
                Impl::asMatrix(*j).umv(xj, yi);
            }
        }
    };

    auto richardson = [&bs, &b_0s, &xs, localpbx, &d, &Ad](size_t stride, size_t id, double omega, size_t iter)
    {
        // if(iter % 2 == 0)
        // {
            d = &bs[id];
            Ad = &b_0s[id];
        // }
        // else
        // {
        //     d = &b_0s[id];
        //     Ad = &bs[id];
        // }

        for(auto i = 0; i < stride; i++)
        {
            auto overlapid = i + i/localpbx*2 + (localpbx+2)+1;
            // update d
            (*Ad)[overlapid] = (*d)[overlapid] - omega * (*Ad)[overlapid];
            // update x
            xs[id][overlapid] = xs[id][overlapid] + omega * (*Ad)[overlapid];
        }
    };

    auto sub = [&bs, &b_0s](size_t stride, size_t id)
    {     
        for(auto i = 0; i < stride; i++)
            bs[id][i] = b_0s[id][i] - bs[id][i];
    };

    auto iter = 1000;
    auto init = 0ul;

    for (auto outer = 0; outer < 8; outer++)
    {
        auto start = std::chrono::high_resolution_clock::now();

        // intial d
        // apply
        for(size_t j = 0; j < nbthreads; j++)
            context->addtask(apply, j, j, init);

        context->waittasks();

        // sub
        for(size_t j = 0; j < nbthreads; j++)
            context->addtask(sub, j, fullstride, j);

        context->waittasks();

        for(auto i = 0; i < iter; i++)
        {
            // apply
            for(size_t j = 0; j < nbthreads; j++)
                context->addtask(apply, j, j, i);

            context->waittasks();

            // richardson
            for(size_t j = 0; j < nbthreads; j++)
                context->addtask(richardson, j, stride, j, omega, i);

            context->waittasks();
        }
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "Time taken: " << std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count() << std::endl;
    }

    return 0;
}



int main(int argc, char** argv)
{
    int thread_safety_level;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_safety_level);

    // std::cout << thread_safety_level << std::endl;
    if(MPI_THREAD_FUNNELED > thread_safety_level)
    {
        std::cout << "required MPI thread safety level not provided" << std::endl;
        MPI_Finalize();
        return 1;
    }
    
    if(argc != 3)
    {
        std::cout << "Not enough Arguments! Arg1: nb threads; Arg2: N" << std::endl;
        MPI_Finalize();
        return 2;
    }

    auto threadcount = std::atoi(argv[1]);
    auto N = std::atoi(argv[2]);

    // hwloc setup
    int err;
    /* create a topology */
    err = hwloc_topology_init(&ParalleltonTP::topology);
    if (err < 0) {
        fprintf(stderr, "failed to initialize the topology\n");
    }
    // /* filter everything out */
    // hwloc_topology_set_all_types_filter(topology, HWLOC_TYPE_FILTER_KEEP_NONE);
    // /* filter Cores back in */
    // hwloc_topology_set_type_filter(topology, HWLOC_OBJ_CORE, HWLOC_TYPE_FILTER_KEEP_ALL);
    err = hwloc_topology_load(ParalleltonTP::topology);
    if (err < 0) {
        fprintf(stderr, "failed to load the topology\n");
        hwloc_topology_destroy(ParalleltonTP::topology);
    }

    auto binding = [](size_t threadid, size_t rank, size_t nbthreads)
    {
        // return threadid * 2;

        auto procsperchiplet = nbthreads/16ul;
        auto whichchiplet = threadid/procsperchiplet;
        auto idinchiplet = threadid%procsperchiplet;

        return (idinchiplet + whichchiplet * 8ul) * 2ul + 4ul;
    };

    auto context = std::make_shared<ThreadPool>(threadcount);
    ParalleltonTP::context = context;
    // bind threads
    ParalleltonTP::context->bindthreads(binding);

    auto ret = testcomm(N);
    
    hwloc_topology_destroy(ParalleltonTP::topology);
    MPI_Finalize();
    return ret;
}