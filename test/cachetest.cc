#include "config.h"
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/operators.hh>
#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/communicator.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <vector>
#include <thread>
#include <utility>
#include <iostream>
#include <cstdlib>
#include <chrono>
#include <hwloc.h>
#include "mpi.h"




int main(int argc, char** argv)
{
    // setup mpi
    MPI_Init(&argc, &argv);
    
    if(argc != 2)
    {
        std::cout << "Need Problem size" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
        return 1;
    }

    auto sizeinmb = std::atoi(argv[1]);

    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // setup cpu binding
    hwloc_topology_t topology;
    hwloc_bitmap_t set;
    int err;

    // setup hwloc topology
    err = hwloc_topology_init(&topology);
    if (err < 0) 
        fprintf(stderr, "failed to initialize the topology\n");
    
    err = hwloc_topology_load(topology);
    if (err < 0) {
        fprintf(stderr, "failed to load the topology\n");
        hwloc_topology_destroy(topology);
    }

    // setup hwloc bitmap
    set = hwloc_bitmap_alloc();
    if (!set) {
        fprintf(stderr, "failed to allocate a bitmap\n");
        hwloc_topology_destroy(topology);
    }
    
    std::vector<int> procs{0, 16, 32, 48, 64, 80, 96, 112, 128, 144, 160, 176, 192, 208, 224, 240};
    // std::vector<int> procs{0, 2, 16, 18, 32, 34, 48, 50, 64, 66, 80, 82, 96, 98, 112, 114};
    int logid = procs[rank];

    auto logobj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_PU, logid);
    hwloc_bitmap_only(set, logobj->os_index);

    err = hwloc_set_cpubind(topology, set, HWLOC_CPUBIND_THREAD);
    if (err < 0) {
        fprintf(stderr, "failed to set thread binding\n");
        hwloc_bitmap_free(set);
        hwloc_topology_destroy(topology);
    }

    hwloc_bitmap_free(set);


    size_t problemsize = (sizeinmb * 1024 * 1024) / (8 * 16);
    if(rank == 0) 
        std::cout << "Problemsize: " << (problemsize * 8) / (1024 * 1024) << " MB for each proc\n" << "In total " << (problemsize * 8 * 16) / (1024 * 1024) << " MB\n";

    std::vector<double> buffer(problemsize, 0.0);

    auto start = std::chrono::high_resolution_clock::now();

    for (auto iter = 0; iter < 10'000; iter++)
    {
        for (auto& i : buffer)
        {
            i += iter;
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "R" << rank << ": " << std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count() << "\n";


    
    hwloc_topology_destroy(topology);
    MPI_Finalize();
    return 0;
}