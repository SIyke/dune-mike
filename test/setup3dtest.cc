#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/istl/io.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>

#include "laplacian.hh"
#include "customlaplacian.hh"
#include "parallocator.hh"


typedef Dune::FieldMatrix<double,1,1> MatrixBlock;
typedef Dune::BCRSMatrix<MatrixBlock> BCRSMat;

typedef Dune::ParAllocator<MatrixBlock, Dune::ThreadPool> Allocator;
typedef Dune::BCRSMatrix<MatrixBlock, Allocator> Mat;

typedef Parallelton<Dune::ThreadPool> ParalleltonTP;

int testsetup(int n)
{
    BCRSMat mat;
    setupFragmented3DLaplacian(mat, n);

    Dune::FieldMatrix<double, 8, 32> comp{
        {0, 0, 0, 0, 0, 0, 0, 4, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, -1, 4, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 4, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 4, -1, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, -1, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 4, -1, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 4, -1, 0, -1, 0, 0, 0, -1, 0},
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, -1, 4, -1, 0, -1, 0, 0, 0, -1}
    };

    auto N = mat.N();
    auto M = mat.M();

    for(auto x = 0; x < N; x++)
    {        
        for(auto y = 0; y < M; y++)
        {
            auto tmp = 0.0;
            if(mat.exists(x,y))
                tmp = mat[x][y];

            // std::cout << tmp << " ";
            if(std::abs(tmp - comp[x][y]) > 0.01)
            {
                std::cerr << "Error at position x=" << x << ", y=" << y << ", test=" << tmp << ", comp=" << comp[x][y] << std::endl;
            }
        }
        // std::cout << std::endl;
    }

    return 0;
}


int main()
{
    ParalleltonTP::rank = 2;
    ParalleltonTP::size = 8;
    ParalleltonTP::pbx = 2;
    ParalleltonTP::pby = 2;
    ParalleltonTP::pbz = 2;
    ParalleltonTP::blockx = 2;
    ParalleltonTP::blocky = 2;
    ParalleltonTP::blockz = 2;

    auto rank = ParalleltonTP::rank;
    auto pbx = ParalleltonTP::pbx;
    auto pby = ParalleltonTP::pby;
    auto pbz = ParalleltonTP::pbz;
    auto blockx = ParalleltonTP::blockx;
    auto blocky = ParalleltonTP::blocky;
    auto N = 4;

    ParalleltonTP::start = (rank%blockx) * pbx
        // y offset                
        + (rank/blockx) * N * pby - (rank/(blockx*blocky) * blockx*pbx * blocky*pby)
        // z offset
        + rank/(blockx*blocky) * blockx*pbx*blocky*pby*pbz;
    
    
    
    auto ret = testsetup(N);


    return ret;
}