#include "config.h"
#include <dune/common/fvector.hh>
#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/communicator.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <vector>
#include <utility>
#include <iostream>
#include "mpi.h"
#include "parcommunicators.hh"
#include "threadpool.hh"

using namespace Dune;


int teststdvector()
{
    typedef std::vector<double> Vector;

    ParData pardata;
    ParCommunicator5Star<Vector,2> parcomm(64, pardata);
    auto rank = pardata.getrank();

    Vector vec(64);

    if(rank == 0)
        for(size_t i = 0; i < vec.size(); i++)
            vec[i] = i;
    
    parcomm.broadcastvector(std::move(vec));

    if(rank == 1)
    {
        for(size_t i = 0; i < vec.size(); i++)
            if(std::abs(i - vec[i]) > 0.01)
                return 1;
    }

    return 0;
}


int testdunefieldvector()
{
    typedef FieldVector<double, 64> Vector;

    ParData pardata;
    ParCommunicator5Star<Vector,2> parcomm(64, pardata);
    auto rank = pardata.getrank();

    Vector vec;

    if(rank == 0)
        for(size_t i = 0; i < vec.size(); i++)
            vec[i] = i;
    
    parcomm.broadcastvector(std::move(vec));

    if(rank == 1)
    {
        for(auto iter = vec.begin(); iter != vec.end(); ++iter)
            if(std::abs((iter - vec.begin()) - *iter) > 0.01)
                return 1;
    }

    return 0;
}


int testduneblockvector()
{
    typedef FieldVector<double, 1> VecBlock;
    typedef BlockVector<VecBlock> Vector;

    ParData pardata;
    ParCommunicator5Star<Vector,2> parcomm(64, pardata);
    auto rank = pardata.getrank();

    Vector vec(64);
    
    if(rank == 0)
        for(size_t i = 0; i < vec.size(); i++)
            vec[i] = i;
    
    parcomm.broadcastvector(std::move(vec));
    
    if(rank == 1)
    {
        for(auto iter = vec.begin(); iter != vec.end(); ++iter)
            if(std::abs((iter - vec.begin()) - *iter) > 0.01)
                return 1;
    }

    return 0;
}



int main(int argc, char** argv)
{
    int thread_safety_level;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &thread_safety_level);

    // std::cout << thread_safety_level << std::endl;
    if(MPI_THREAD_FUNNELED != thread_safety_level)
        std::cout << "required MPI thread safety level not provided" << std::endl;

    auto ret = teststdvector();
    ret += testdunefieldvector();
    ret += testduneblockvector();

    MPI_Finalize();
    return ret;
}