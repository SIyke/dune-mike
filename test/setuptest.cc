#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/istl/io.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>

#include "laplacian.hh"
#include "customlaplacian.hh"
#include "parallocator.hh"


typedef Dune::FieldMatrix<double,1,1> MatrixBlock;
typedef Dune::BCRSMatrix<MatrixBlock> BCRSMat;

typedef Dune::ParAllocator<MatrixBlock, Dune::ThreadPool> Allocator;
typedef Dune::BCRSMatrix<MatrixBlock, Allocator> Mat;

typedef Parallelton<Dune::ThreadPool> ParalleltonTP;

int testsetup()
{
    BCRSMat mat;
    setupFragmentedLaplacian(mat, 4);

    Dune::FieldMatrix<double, 4, 12> comp{
        {0, 0, 0, 4, -1, 0, 0, -1, 0, 0, 0, 0},
        {0, 0, 0, -1, 4, -1, 0, 0, -1, 0, 0, 0},
        {0, 0, 0, -1, 0, 0, 0, 4, -1, 0, -1, 0},
        {0, 0, 0, 0, -1, 0, 0, -1, 4, -1, 0, -1}
    };

      auto N = mat.N();
      auto M = mat.M();

      for(auto x = 0; x < N; x++)
        for(auto y = 0; y < M; y++)
        {
            auto tmp = 0.0;
            if(mat.exists(x,y))
                tmp = mat[x][y];

            if(std::abs(tmp - comp[x][y]) > 0.01)
            {
                std::cerr << "Error at position x=" << x << ", y=" << y << std::endl;
                return 1;
            }
        }

    return 0;
}


int main()
{
    ParalleltonTP::rank = 0;
    ParalleltonTP::size = 4;
    ParalleltonTP::pbx = 2;
    ParalleltonTP::pby = 2;
    ParalleltonTP::blockx = 2;
    ParalleltonTP::start = (ParalleltonTP::rank%ParalleltonTP::blockx) * ParalleltonTP::pbx + (ParalleltonTP::rank/ParalleltonTP::blockx) * 4 * ParalleltonTP::pby;

    auto ret = testsetup();
    return ret;
}