#include "config.h"
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/operators.hh>
#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/communicator.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <vector>
#include <thread>
#include <utility>
#include <iostream>
#include <cstdlib>
#include <chrono>
#include "mpi.h"


int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(size != 2)
    {
        std::cout << "Need two ranks" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
        return 1;
    }
    if(argc != 2)
    {
        std::cout << "Need Problem size" << std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
        return 1;
    }

    std::vector<int> data;
    auto N = std::atoi(argv[1]);
    auto iter = 10;
    auto blockingtime = 0.0;
    auto nonblockingtime = 0.0;


    data.resize(N, 0.0);

    // blocking measurements
    if(rank == 0)
    {   
        for(auto i = 0; i < iter; i++)
        {
            // auto start = std::chrono::high_resolution_clock::now();
            MPI_Send(&data[0], N, MPI_INT, 1, 0, MPI_COMM_WORLD);
            // auto end = std::chrono::high_resolution_clock::now();
            // blockingtime += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }
        // std::cout << "Send takes " << blockingtime << "s" << std::endl;
    }
    else
    {
        for(auto i = 0; i < iter; i++)
        {            
            auto start = std::chrono::high_resolution_clock::now();
            MPI_Recv(&data[0], N, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            auto end = std::chrono::high_resolution_clock::now();
            blockingtime += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }
        std::cout << "Recv takes " << blockingtime << "s" << std::endl;
    }

    data.resize(N, 0.0);

    // non blocking measurements
    if(rank == 0)
    {   
        std::vector<MPI_Request> requests(iter);
        for(auto i = 0; i < iter; i++)
        {
            MPI_Isend(&data[0], N, MPI_INT, 1, i, MPI_COMM_WORLD, &requests[i]);
            // auto start = std::chrono::high_resolution_clock::now();
            
            // auto end = std::chrono::high_resolution_clock::now();
            // nonblockingtime += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }
        
        for(auto i = 0; i < iter; i++)
            MPI_Wait(&requests[i], MPI_STATUS_IGNORE);

        // std::cout << "ISend takes " << nonblockingtime << "s" << std::endl;
    }
    else
    {   
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(5s);

        for(auto i = 0; i < iter; i++)
        {
            auto start = std::chrono::high_resolution_clock::now();
            MPI_Recv(&data[0], N, MPI_INT, 0, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            auto end = std::chrono::high_resolution_clock::now();
            nonblockingtime += std::chrono::duration_cast<std::chrono::duration<double>> (end - start).count();
        }
        std::cout << "(I)Recv takes " << nonblockingtime << "s" << std::endl;
    }


    MPI_Finalize();
    return 0;
}