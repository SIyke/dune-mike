#include <dune/common/test/testsuite.hh>
#include "threadpool.hh"
#include "threadpool_old.hh"
#include "parmatrixadapter.hh"
#include <memory>
#include "laplacian.hh"

using namespace Dune;


int testThreadPoolID(int threadcount, int N)
{
    auto threadpool = std::make_shared<ThreadPool>(threadcount);
    const int BS = 1;
    typedef Dune::FieldMatrix<int,BS,BS> MatrixBlock;
    typedef Dune::BCRSMatrix<MatrixBlock> BCRSMat;
    typedef Dune::FieldVector<int,BS> VectorBlock;
    typedef Dune::BlockVector<VectorBlock> Vector;
    typedef Dune::MatrixAdapter<BCRSMat,Vector,Vector> Operator;

    BCRSMat mat;
    Operator fop(mat);
    Vector b(N*N), x(N*N);
    Vector seq_b(N*N), seq_x(N*N);

    setupLaplacian(mat, N);
    b=0.0;
    x=1.0;
    seq_b = 0.0;
    seq_x = 1.0;

    int iter = 1;

    auto f = [&mat,&x,&b](auto start)
    {
        // ConstRowIterators
        auto i = mat.begin() + start;
        b[i.index()] = 0;
        // ConstColIterators
        auto j = (*i).begin();
        auto endj = (*i).end();
        for(; j != endj; ++j)
        {
            auto&& xj = Impl::asVector(x[j.index()]);
            auto&& bi = Impl::asVector(b[i.index()]);
            Impl::asMatrix(*j).umv(xj, bi);                 
        }
    };

    for(auto i = 0; i < iter; i++)
    {
        for(auto j = 0; j < N*N; j++)
        {
            size_t thread_id = j % threadcount;
            threadpool->addtask(f, thread_id, j);
        }
        threadpool->waittasks();
    }

    // correctness check
    for(auto i = 0; i < iter; i++)
        fop.apply(seq_x,seq_b);

    // for(auto i = 0; i < N*N; i++) 
    //     std::cout << b[i] << " ";
    // std::cout << std::endl;

    // for(auto i = 0; i < N*N; i++) 
    //     std::cout << seq_b[i] << " ";
    // std::cout << std::endl;


    for(auto i = 0; i < N*N; i++)
    {
        if(b[i] != seq_b[i])
            return 1;
    }

    return 0;
}

int main (int argc, char** argv)
{
    auto N = 512;

    auto ret = testThreadPoolID(4, N);
    ret += testThreadPoolID(8, N);

    return ret;
}