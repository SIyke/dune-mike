#include "config.h"
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/operators.hh>
#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/communicator.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <vector>
#include <utility>
#include <iostream>
#include <cstdlib>
#include "mpi.h"
#include "parcommunicators.hh"
#include "parallelton.hh"
#include "threadpool.hh"
#include "laplacian.hh"
#include "customlaplacian.hh"
#include "consecutivelaplacian.hh"
#include "parmatrixadapter.hh"
#include "parcalculations.hh"

using namespace Dune;

constexpr int BS = 1;
typedef FieldMatrix<double,BS,BS> MatrixBlock;
typedef BCRSMatrix<MatrixBlock> BCRSMat;
typedef FieldVector<double,BS> VectorBlock;
typedef BlockVector<VectorBlock> Vector;
typedef MatrixAdapter<BCRSMat,Vector,Vector> Operator;

typedef Parallelton<Dune::ThreadPool> ParalleltonTP;


Vector seqresult(size_t N, size_t iter)
{
    Vector b(N*N*N);
    Vector b_0(N*N*N);
    Vector x(N*N*N);
    BCRSMat mat;
    Operator fop(mat);
    Vector* d;
    Vector* Ad;
    
    b_0 = 0.0;
    x = 1.0;
    double omega = 1.0;
    setup3DLaplacian(mat, N);
    
    // intial d
    fop.apply(x, b);
    for(auto i = 0ul; i < b.size(); i++)
        b[i] = b_0[i] - b[i];

    for(auto i = 0; i < iter; i++)
    {
        if(i % 2 == 0)
        {
            d = &b;
            Ad = &b_0;
        }
        else
        {
            d = &b_0;
            Ad = &b;
        }
        fop.apply(*d, *Ad);
        // Richardson
        for(auto j = 0ul; j < b.size(); j++)
        {
            // d_k+1 = d_k - omega * Ad_k
            (*Ad)[j] = (*d)[j] - omega * (*Ad)[j];
            // x_k+1 = x_k + omega * d_k
            x[j] = x[j] + omega * (*Ad)[j];
        }
    }

    return x;
}



int testcomm(size_t N)
{    
    ParData pardata;
    ParCommunicator5Star<Vector,3> parcomm(N, pardata);

    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();

    auto size = pardata.getsize();
    auto rank = pardata.getrank();

    if(rank == 0)
    {
        std::cout << "pbx=" << pbx << " pby=" << pby << " pbz=" << pbz << std::endl;
        std::cout << "blockx=" << pardata.getblockx() << " blocky=" << pardata.getblocky() << " blockz=" << pardata.getblockz() << std::endl;
    }

    Vector x((pbx+2) * (pby+2) * (pbz+2));
    Vector b((pbx+2) * (pby+2) * (pbz+2));
    Vector b_0((pbx+2) * (pby+2) * (pbz+2));
    double omega;
    BCRSMat mat;
    ParMatrixAdapter3D<BCRSMat, Vector, Vector> par_fop(mat, pardata);
    ParCalculationAdapter<Vector> par_calc(x, b, pardata);

    b_0 = 0.0;
    x = 1.0;
    omega = 1.0;
    
    setupFragmented3DLaplacian(mat, N, pardata);


    auto iter =  3;
    Vector* d;
    Vector* Ad;

    // intial d
    par_fop.apply(x, b);
    par_calc.subalt3D(b, b_0);

    if (size > 1)
        parcomm.communicate(b);

    for(auto i = 0; i < iter; i++)
    {
        if(i % 2 == 0)
        {
            d = &b;
            Ad = &b_0;
        }
        else
        {
            d = &b_0;
            Ad = &b;
        }

        par_fop.apply(*d, *Ad);

        par_calc.richardsonalt3D(*d, *Ad, omega);

        if (size > 1)
            parcomm.communicate(*Ad);
    }

    while(!ParalleltonTP::context->notasksleft())
    {}
    Vector result;
    parcomm.gatherresult(x, result);

    // test correctness of results
    if(rank == 0)
    {   
        auto seq_x = seqresult(N, iter);

        // // print b
        // auto i = 0;
        // for(auto z_ = 0; z_ < N; z_++)
        // {
        //     std::cout << "Layer " << z_ << std::endl;
        //     for(auto y_ = 0; y_ < N; y_++)
        //     {
        //         for(auto x_ = 0; x_ < N; x_++)
        //         {
        //            std::cout << result[i++] << "|"; 
        //         }
        //         std::cout << std::endl;
        //     }
        //     std::cout << std::endl;
        // }

        // Error checking
        for(size_t i = 0; i < N*N*N; i++)
        {
            if(std::abs(result[i] - seq_x[i]) > 0.001)
            {
                // std::cout << "Error in Calculation with " << "N=" << N << " TC=" << threadcount << " iter=" << iter << std::endl;
                std::cout << "Error in i=" << i << " seq=" << seq_x[i] << " par=" << result[i] << std::endl;
                // hwloc_topology_destroy(ParalleltonTP::topology);
                // MPI_Abort();
                return 1;
            }
        }
    }
    if(rank == 0) std::cout << "Calculation correct" << std::endl;
    return 0;
}





int testIcomm(size_t N)
{    
    ParData pardata;
    ParCommunicator5Star<Vector,3> parcomm(N, pardata);

    auto pbx = pardata.getpbx();
    auto pby = pardata.getpby();
    auto pbz = pardata.getpbz();
    auto size = pardata.getsize();
    auto rank = pardata.getrank();

    Vector x((pbx+2) * (pby+2) * (pbz+2));
    Vector b((pbx+2) * (pby+2) * (pbz+2));
    Vector b_0((pbx+2) * (pby+2) * (pbz+2));
    double omega = 1.0;
    BCRSMat mat;
    ParMatrixAdapter3D<BCRSMat, Vector, Vector> par_fop(mat, pardata);
    ParCalculationAdapter<Vector> par_calc(x, b, pardata);
    par_calc.setupborderindices3D();

    b_0 = 0.0;
    x = 1.0;

    setupFragmented3DLaplacian(mat, N, pardata);

    auto iter = 3;
    Vector* d;
    Vector* Ad;
    typedef ParCalculationAdapter<Vector> ParCalc;

    // d_0 = b_0 - Ax_0
    par_fop.apply(x, b);
    par_calc.subalt3D(b, b_0);

    parcomm.communicate(b);

    for(auto i = 0; i < iter; i++)
    {
        if(i % 2 == 0)
        {
            d = &b;
            Ad = &b_0;
        }
        else
        {
            d = &b_0;
            Ad = &b;
        }

        par_fop.applyouter(*d, *Ad);
        par_calc.richarddefectout3D(*d, *Ad, omega);

        parcomm.Icommunicatestart(*Ad);

        par_fop.applyinner(*d, *Ad);
        par_calc.richarddefectin3D(*d, *Ad, omega);

        // // print b
        // auto j = 0;
        // for(auto z_ = 0; z_ < pbz+2; z_++)
        // {
        //     std::cout << "b Layer " << z_ << std::endl;
        //     for(auto y_ = 0; y_ < pby+2; y_++)
        //     {
        //         for(auto x_ = 0; x_ < pbx+2; x_++)
        //             std::cout << (*Ad)[j++] << " "; 
        //         std::cout << std::endl;
        //     }
        //     std::cout << std::endl;
        // }
        // std::cout << std::endl;


        // copy updated b values to x
        // par_calc.iteration(*Ad, omega);

        parcomm.Icommunicateend(*Ad);
    }

    Vector result;
    parcomm.gatherresult(x, result);

    // test correctness of results
    if(rank == 0)
    {   
        auto seq_x = seqresult(N, iter);

        // // print b
        // auto i = 0;
        // for(auto z_ = 0; z_ < N; z_++)
        // {
        //     std::cout << "Layer " << z_ << std::endl;
        //     for(auto y_ = 0; y_ < N; y_++)
        //     {
        //         for(auto x_ = 0; x_ < N; x_++)
        //         {
        //            std::cout << seq_b[i++] << " "; 
        //         }
        //         std::cout << std::endl;
        //     }
        //     std::cout << std::endl;
        // }  



        // Error checking
        for(size_t i = 0; i < N*N*N; i++)
        {
            if(std::abs(result[i] - seq_x[i]) > 0.001)
            {
                // std::cout << "Error in Calculation with " << "N=" << N << " TC=" << threadcount << " iter=" << iter << std::endl;
                std::cout << "Error in i=" << i << " seq=" << seq_x[i] << " par=" << result[i] << std::endl;
                // hwloc_topology_destroy(ParalleltonTP::topology);
                // MPI_Abort();
                return 1;
            }
        }
    }
    if(rank == 0) std::cout << "Calculation correct" << std::endl;
    return 0;
}


int main(int argc, char** argv)
{
    int thread_safety_level;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &thread_safety_level);

    if(MPI_THREAD_MULTIPLE != thread_safety_level)
        std::cout << "required MPI thread safety level not provided" << std::endl;

    // hwloc setup
    int err;
    /* create a topology */
    err = hwloc_topology_init(&ParalleltonTP::topology);
    if (err < 0) {
        fprintf(stderr, "failed to initialize the topology\n");
    }
    // /* filter everything out */
    // hwloc_topology_set_all_types_filter(topology, HWLOC_TYPE_FILTER_KEEP_NONE);
    // /* filter Cores back in */
    // hwloc_topology_set_type_filter(topology, HWLOC_OBJ_CORE, HWLOC_TYPE_FILTER_KEEP_ALL);
    err = hwloc_topology_load(ParalleltonTP::topology);
    if (err < 0) {
        fprintf(stderr, "failed to load the topology\n");
        hwloc_topology_destroy(ParalleltonTP::topology);
    }

    int N = 144;
    int threadcount = 128;
 
    auto context = std::make_shared<ThreadPool>(threadcount);
    ParalleltonTP::context = context;
    context->setupneighbors<3>(N, true);
   
    auto binding = [](size_t threadid, size_t rank, size_t nbthreads)
    {
        int size;
        MPI_Comm_size(MPI_COMM_WORLD, &size);

        if (nbthreads == 1)
            return rank;// + rank/32ul*64ul;
        else if (size == 1)
            return threadid;// + threadid/32ul*64ul;
        else
            return (rank * 8ul + threadid);// + rank/4ul*64ul;
    };
    ParalleltonTP::lambda = binding;
    // bind threads
    ParalleltonTP::context->bindthreads(binding);


    auto ret = testcomm(N);
    // ret += testIcomm(N);

    hwloc_topology_destroy(ParalleltonTP::topology);
    MPI_Finalize();
    return ret;
}