#include <dune/common/test/testsuite.hh>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include "threadpool.hh"
#include "parmatrixadapter.hh"
#include <memory>
#include "laplacian.hh"
#include "customlaplacian.hh"

const int BS = 1;
typedef Dune::FieldMatrix<double,BS,BS> MatrixBlock;
typedef Dune::BCRSMatrix<MatrixBlock> BCRSMat;
typedef Dune::FieldVector<double,BS> VectorBlock;
typedef Dune::BlockVector<VectorBlock> Vector;
typedef Dune::MatrixAdapter<BCRSMat,Vector,Vector> Operator;

typedef Parallelton<Dune::ThreadPool> ParalleltonTP;

using namespace Dune;


int testapply(size_t N, int iter)
{

    auto overlap = 2*ParalleltonTP::pbx + 2*ParalleltonTP::pby;
    Vector x(N*N/ParalleltonTP::size + overlap);
    Vector b(N*N/ParalleltonTP::size + overlap);

    Vector seq_b(N*N), seq_x(N*N);
    BCRSMat mat;
    Operator fop(mat);

    x = 1.0;
    b = 0.0;
    seq_x = 1.0;
    seq_b = 0.0;

    setupFragmentedLaplacian(mat,N);
    ParMatrixAdapter<BCRSMat,Vector,Vector> par_fop(mat);

    for(auto i = 0; i < iter; i++)
    {
        par_fop.apply(x,b);
        x = b;


    }

    for(auto i = 0; i < iter; i++)
    {
        fop.apply(seq_x, seq_b);
        seq_x = seq_b;
    }

    for(auto i = 0; i < N*N; i++)
    {
        if(std::abs(b[i] - seq_b[i]) > 0.001)
            return 1;
    }

    return 0;
}


int main()
{
    auto context = std::make_shared<ThreadPool>(2);
    auto N = 1024;

    // case for no mpi parallelism
    ParalleltonTP::context = context;

    ParalleltonTP::rank = 0;
    ParalleltonTP::size = 1;
    ParalleltonTP::N = N;
    ParalleltonTP::blockx = 1;
    ParalleltonTP::blocky = 1;
    ParalleltonTP::pbx = N;
    ParalleltonTP::pby = N;
    ParalleltonTP::start = 0;



    auto ret = testapply(N, 5);

    return ret;
}